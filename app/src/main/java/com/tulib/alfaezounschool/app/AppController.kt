package com.tulib.alfaezounschool.app

import android.app.Application
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.tulib.alfaezounschool.activity.ui.accounts.AccountsViewModelFactory
import com.tulib.alfaezounschool.activity.ui.announcements.event.EventViewModelFactory
import com.tulib.alfaezounschool.activity.ui.announcements.holiday.HolidayViewModelFactory
import com.tulib.alfaezounschool.activity.ui.announcements.notice.NoticeViewModelFactory
import com.tulib.alfaezounschool.activity.ui.attendance.AttendanceViewModelFactory
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivityViewModelFactory
import com.tulib.alfaezounschool.activity.ui.complain.ComplainViewModelFactory
import com.tulib.alfaezounschool.activity.ui.dashboard.DashboardViewModelFactory
import com.tulib.alfaezounschool.activity.ui.exam.ExamViewModelFactory
import com.tulib.alfaezounschool.activity.ui.forgot_password.forgot_password_email.ForgotPasswordViewModelFactory
import com.tulib.alfaezounschool.activity.ui.forgot_password.verification_code.VerifyCodeViewModelFactory
import com.tulib.alfaezounschool.activity.ui.homework.HomeworkViewModelFactory
import com.tulib.alfaezounschool.activity.ui.library.LibraryViewModelFactory
import com.tulib.alfaezounschool.activity.ui.login.LoginViewModelFactory
import com.tulib.alfaezounschool.activity.ui.marks.MarksViewModelFactory
import com.tulib.alfaezounschool.activity.ui.media.MediaViewModelFactory
import com.tulib.alfaezounschool.activity.ui.message.conversation_view.ConversationViewModelFactory
import com.tulib.alfaezounschool.activity.ui.message.message_view.MessageViewModelFactory
import com.tulib.alfaezounschool.activity.ui.notification.NotificationViewModelFactory
import com.tulib.alfaezounschool.activity.ui.parents.ParentsViewModelFactory
import com.tulib.alfaezounschool.activity.ui.profile.ProfileViewModelFactory
import com.tulib.alfaezounschool.activity.ui.settings.change_password.ChangePasswordViewModelFactory
import com.tulib.alfaezounschool.activity.ui.studentactivity.StudentActivityViewModelFactory
import com.tulib.alfaezounschool.activity.ui.students.StudentsViewModelFactory
import com.tulib.alfaezounschool.activity.ui.teachers.TeachersViewModelFactory
import com.tulib.alfaezounschool.activity.ui.timetable.TimeTableViewModelFactory
import com.tulib.alfaezounschool.data.db.database.ActualRoomDatabase
import com.tulib.alfaezounschool.data.db.database.AppDatabase
import com.tulib.alfaezounschool.data.db.database.Database
import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.api_manger.LogoutApiManager
import com.tulib.alfaezounschool.data.network.interceptor.NetworkConnectionInterceptor
import com.tulib.alfaezounschool.data.repository.*
import com.tulib.alfaezounschool.dialog.LoaderDialog
import nye.health.data.network.api_call.middle_ware.ResponseMiddleware
import nye.health.data.network.api_call.network_handler.NetworkAvailable
import nye.health.data.network.api_call.session.SessionManager
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import com.tulib.alfaezounschool.utils.UserType
class AppController : Application() {
    init {
        instance = this
        userType = null
        studentid=""
    }


    companion object {
        var userType:UserType?=null
        var studentid:String?=null

        var fetchCommonData = false
        private lateinit var instance: AppController

        val kodein = Kodein.lazy {
            Firebase.messaging.isAutoInitEnabled = true



            //bind() from singleton { Preference(instance) }
            bind() from singleton { AppDatabase(ActualRoomDatabase(instance)) }
            bind() from singleton { Database(instance()) }
            bind() from singleton { LoaderDialog() }
            bind() from singleton { SessionManager(instance()) }
            bind() from singleton { ApiManager(NetworkConnectionInterceptor(instance())) }
            bind() from singleton { NetworkAvailable() }
            bind() from singleton { ResponseMiddleware(instance()) }
            bind() from singleton { LogoutApiManager(instance(), instance()) }

//
//            // Repository
            bind() from singleton { LoginRepository(instance(), instance()) }
            bind() from singleton { ChangePasswordRepository(instance(), instance()) }
            bind() from singleton { LogoutRepository(instance(), instance()) }
            bind() from singleton { ExamRepository(instance(), instance()) }
            bind() from singleton { NotificationRepository(instance(), instance()) }
            bind() from singleton { ConversationRepository(instance(), instance()) }
            bind() from singleton { ForgotPasswordRepository(instance(), instance()) }
            bind() from singleton { HomeWorkRepository(instance(), instance()) }
            bind() from singleton { TimeTableRepository(instance(), instance()) }
            bind() from singleton { NoticeRepository(instance(), instance()) }
            bind() from singleton { EventRepository(instance(), instance()) }
            bind() from singleton { HolidayRepository(instance(), instance()) }
            bind() from singleton { StudentActivityRepository(instance(), instance()) }
            bind() from singleton { AccountsRepository(instance(), instance()) }
            bind() from singleton { ComplainRepository(instance(), instance()) }
            bind() from singleton { LibraryRepository(instance(), instance()) }
            bind() from singleton { MarksRepository(instance(), instance()) }
            bind() from singleton { ProfileRepository(instance(), instance()) }
            bind() from singleton { AttendanceRepository(instance(), instance()) }
            bind() from singleton { CommonRepository(instance(), instance()) }
            bind() from singleton { StudentsRepository(instance(), instance()) }
            bind() from singleton { ParentsRepository(instance(), instance()) }
            bind() from singleton { TeachersRepository(instance(), instance()) }
            bind() from singleton { MediaRepository(instance(), instance()) }



//            // ViewModelFactory
            bind() from singleton { LoginViewModelFactory(instance(), instance()) }
            bind() from singleton { ChangePasswordViewModelFactory(instance()) }
            bind() from singleton { ExamViewModelFactory(instance()) }
            bind() from singleton { NotificationViewModelFactory(instance()) }
            bind() from singleton { ForgotPasswordViewModelFactory(instance()) }
            bind() from singleton { VerifyCodeViewModelFactory(instance()) }
            bind() from singleton { HomeworkViewModelFactory(instance()) }
            bind() from singleton { TimeTableViewModelFactory(instance()) }
            bind() from singleton { NoticeViewModelFactory(instance()) }
            bind() from singleton { EventViewModelFactory(instance()) }
            bind() from singleton { HolidayViewModelFactory(instance()) }
            bind() from singleton { StudentActivityViewModelFactory(instance()) }
            bind() from singleton { AccountsViewModelFactory(instance()) }
            bind() from singleton { ComplainViewModelFactory(instance()) }
            bind() from singleton { LibraryViewModelFactory(instance()) }
            bind() from singleton { MarksViewModelFactory(instance(),instance()) }
            bind() from singleton { ConversationViewModelFactory(instance()) }
            bind() from singleton { DashboardViewModelFactory(instance(),instance(),instance(),instance()) }
            bind() from singleton { MessageViewModelFactory(instance()) }
            bind() from singleton { ProfileViewModelFactory(instance()) }
            bind() from singleton { BaseActivityViewModelFactory(instance()) }
            bind() from singleton { AttendanceViewModelFactory(instance()) }
            bind() from singleton { StudentsViewModelFactory(instance()) }
            bind() from singleton { ParentsViewModelFactory(instance()) }
            bind() from singleton { TeachersViewModelFactory(instance()) }
            bind() from singleton { MediaViewModelFactory(instance()) }


        }


        fun kodein() = kodein
        fun applicationContext(): AppController = instance
    }



}
