package com.tulib.alfaezounschool.utils

object AppConstants {
    const val DATABASE_NAME = "tulib_school"
    const val USER_TABLE = "user"
    const val DATABASE_VERSION = 1
    const val BASE_URL = "https://nextiotechnologies.com/alfaezoun_school/api/v10/"
    const val URL_ATTACH_FILES = "https://nextiotechnologies.com/alfaezoun_school/uploads/"
//    const val BASE_URL = "https://portal.tulib.k12.tr/api/v10/"
//    const val URL_ATTACH_FILES = "https://portal.tulib.k12.tr/uploads/"
    const val INTERNET_ERROR = "Please check your internet connect"






}