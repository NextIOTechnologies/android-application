package com.tulib.alfaezounschool.utils

enum class UserType { Teacher,Student,Parent,Admin }