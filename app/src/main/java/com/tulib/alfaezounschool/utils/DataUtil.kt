package com.tulib.alfaezounschool.utils


import android.util.Log
import com.google.gson.JsonObject


object DataUtil {

    // Login Request
    fun getLoginJson(username:String,password:String,device_token:String) =
        JsonObject().apply {
            addProperty("username", username)
            addProperty("password",password)
            addProperty("device_token",device_token)
        }

    // Forgot Password Email Request
    fun getForgotPasswordEmailJson(email:String) =
            JsonObject().apply {
                addProperty("email", email)
            }

    // Verify Sms Code Request
    fun getVerifySmsCodeJson(email:String,otpCode:String) =
            JsonObject().apply {
                addProperty("email", email)
                addProperty("otp_code", otpCode)
            }

    // Forgot Password  Request
    fun getForgotPasswordJson(email:String,otpCode:String,password:String) =
            JsonObject().apply {
                addProperty("email", email)
                addProperty("otp_code", otpCode)
                addProperty("new_password", password)
            }

    // Change Password Request
    fun getChangePasswordJson(oldPassword:String,newPassword:String) =
            JsonObject().apply {
                Log.d("xcfgdf", "getChangePasswordJsonold: "+oldPassword)
                Log.d("xcfgdf", "getChangePasswordJsonnew: "+newPassword)
                addProperty("old_password", oldPassword)
                addProperty("new_password",newPassword)
            }

    // New Message Send Request
    fun getNewMessageJson(subject:String,message:String,usertypeID:String,userID:String) =
            JsonObject().apply {
                addProperty("subject", subject)
                addProperty("message",message)
                addProperty("usertypeID",usertypeID)
                addProperty("userID",userID)
            }

    // Message(Conversation) Send  Request
    fun getMessageJson(conversationID:String,message:String) =
            JsonObject().apply {
                addProperty("conversationID", conversationID)
                addProperty("msg",message)

            }


    // New Message Send Request
    fun getComplainJson(description:String,title:String,usertypeID:String,userID:String) =
            JsonObject().apply {
                addProperty("description", description)
                addProperty("title",title)
                addProperty("usertypeID",usertypeID)
                addProperty("userID",userID)
            }
}