@file:Suppress("Annotator")

package com.tulib.alfaezounschool.utils

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar



object AppUtils {


    fun showSnackBar(activity:AppCompatActivity,msg: String?, success: Boolean=false)
    {
        if(activity!=null)
        {
            hideKeyboard(activity)
                val snackbar = Snackbar.make(activity.window.decorView.rootView, msg ?: "", Snackbar.LENGTH_LONG)
                val view: View = snackbar.view
                view.setBackgroundColor(if (success) Color.parseColor("#32CD32") else Color.RED)
                snackbar.show()
        }
    }



    fun hideKeyboard(activity: AppCompatActivity?) {
        if(activity!=null)
        {
            val imm: InputMethodManager =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(activity.window.decorView.rootView.windowToken, 0)
        }
    }


}