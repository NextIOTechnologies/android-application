package com.tulib.alfaezounschool.utils

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.tulib.alfaezounschool.R
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.view.WindowManager
import android.widget.TextView
import android.graphics.drawable.ColorDrawable
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.Fragment

object Common {
    var count = 0
    fun startActivity(context: Context, secondActivity: Class<*>?) {
        val intent = Intent(context, secondActivity)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        context.startActivity(intent)
    }

    fun AddFragment(fragment: Fragment?, activity: AppCompatActivity) {
        val manager = activity.supportFragmentManager
        val ft = manager.beginTransaction()
        ft.replace(R.id.content_frame, fragment!!).commit()
    }

    fun FullScreen(activity: Activity) {
        val window = activity.window
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    fun CustomDialog(context: Context, openActivity: Class<*>?, dialogText: String?, btnText: String?) {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.dialog_email_confirm)
        val txt = dialog.findViewById<TextView>(R.id.dialogCustomText)
        val btnOk = dialog.findViewById<Button>(R.id.btnDialogOk)
        val imgClose = dialog.findViewById<ImageView>(R.id.imgDialogClose)
        txt.text = dialogText
        btnOk.text = btnText
        btnOk.setOnClickListener {
            dialog.dismiss()
            startActivity(context, openActivity)
        }
        imgClose.setOnClickListener { dialog.dismiss() }
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }
}