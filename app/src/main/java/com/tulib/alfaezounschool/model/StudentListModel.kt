package com.tulib.alfaezounschool.model

class StudentListModel {
    var studentname: String
    var studentlist_rollno: String
    var studentlist_class: String
    var studentlist_image: Int

    constructor(studentname: String, studentlist_rollno: String, studentlist_class: String, studentlist_image: Int) {
        this.studentname = studentname
        this.studentlist_rollno = studentlist_rollno
        this.studentlist_class = studentlist_class
        this.studentlist_image = studentlist_image
    }


}