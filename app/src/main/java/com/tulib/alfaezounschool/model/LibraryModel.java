package com.tulib.alfaezounschool.model;

public class LibraryModel {
    String library_bookname;
    String library_bookauthor;
    int library_bookcoverimage;

    public LibraryModel(String library_bookname, String library_bookauthor, int library_bookcoverimage) {
        this.library_bookname = library_bookname;
        this.library_bookauthor = library_bookauthor;
        this.library_bookcoverimage = library_bookcoverimage;
    }

    public String getLibrary_bookname() {
        return library_bookname;
    }

    public void setLibrary_bookname(String library_bookname) {
        this.library_bookname = library_bookname;
    }

    public String getLibrary_bookauthor() {
        return library_bookauthor;
    }

    public void setLibrary_bookauthor(String library_bookauthor) {
        this.library_bookauthor = library_bookauthor;
    }

    public int getLibrary_bookcoverimage() {
        return library_bookcoverimage;
    }

    public void setLibrary_bookcoverimage(int library_bookcoverimage) {
        this.library_bookcoverimage = library_bookcoverimage;
    }
}
