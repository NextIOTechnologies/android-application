package com.tulib.alfaezounschool.model

class ForgotPasswordSingletonModel private constructor(){
   public var email :String ?=null
   public var smsCode :String ?=null

    companion object
    {
        private var model : ForgotPasswordSingletonModel?=null
        fun getModel():ForgotPasswordSingletonModel
        { if(model==null) model = ForgotPasswordSingletonModel()
        return model!!}
        fun clearModel(){ model=null }
    }


}