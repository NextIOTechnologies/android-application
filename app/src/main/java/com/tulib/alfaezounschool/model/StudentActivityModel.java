package com.tulib.alfaezounschool.model;

public class StudentActivityModel {

    String activity_studentname;
    String activity_date;
    String activity_subjectname;
    String activity_teachername;

    public StudentActivityModel(String activity_studentname, String activity_date, String activity_subjectname, String activity_teachername) {
        this.activity_studentname = activity_studentname;
        this.activity_date = activity_date;
        this.activity_subjectname = activity_subjectname;
        this.activity_teachername = activity_teachername;
    }

    public String getActivity_studentname() {
        return activity_studentname;
    }

    public void setActivity_studentname(String activity_studentname) {
        this.activity_studentname = activity_studentname;
    }

    public String getActivity_date() {
        return activity_date;
    }

    public void setActivity_date(String activity_date) {
        this.activity_date = activity_date;
    }

    public String getActivity_subjectname() {
        return activity_subjectname;
    }

    public void setActivity_subjectname(String activity_subjectname) {
        this.activity_subjectname = activity_subjectname;
    }

    public String getActivity_teachername() {
        return activity_teachername;
    }

    public void setActivity_teachername(String activity_teachername) {
        this.activity_teachername = activity_teachername;
    }
}
