package com.tulib.alfaezounschool.model;

public class MarkModel {
    String mark_subject;
    String mark_total;
    String mark_obtained;


    public MarkModel(String mark_subject, String mark_total, String mark_obtained) {
        this.mark_subject = mark_subject;
        this.mark_total = mark_total;
        this.mark_obtained = mark_obtained;
    }

    public String getMark_subject() {
        return mark_subject;
    }

    public void setMark_subject(String mark_subject) {
        this.mark_subject = mark_subject;
    }

    public String getMark_total() {
        return mark_total;
    }

    public void setMark_total(String mark_total) {
        this.mark_total = mark_total;
    }

    public String getMark_obtained() {
        return mark_obtained;
    }

    public void setMark_obtained(String mark_obtained) {
        this.mark_obtained = mark_obtained;
    }
}
