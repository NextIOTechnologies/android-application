package com.tulib.alfaezounschool.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.tulib.alfaezounschool.utils.AppConstants.USER_TABLE
import com.google.gson.annotations.SerializedName


const val CURRENT_USER_ID = 0
@Entity(tableName = USER_TABLE)
data class User(
        var email: String,
    var loginuserID: String,
    var name: String,
    var photo: String,
    var token: String,
    var username: String,
    var usertypeID: String,
   @SerializedName("class")
    var userClass: String?
)
{
    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name = "primaryId")
    private var primaryId = CURRENT_USER_ID
    fun getPrimaryId(): Int {
        return primaryId
    }
    fun setPrimaryId(primaryId: Int) {
        this.primaryId = primaryId
    }
}