package com.tulib.alfaezounschool.activity.ui.forgot_password.verification_code

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IForgotPasswordRepository


@Suppress("UNCHECKED_CAST")
class VerifyCodeViewModelFactory(var forgotPasswordRepository: IForgotPasswordRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return VerificationCodeViewModel(forgotPasswordRepository) as T
    }
}