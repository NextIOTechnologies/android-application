package com.tulib.alfaezounschool.activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;

import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity;
import com.tulib.alfaezounschool.adapter.StudentActivityAdapter;
import com.tulib.alfaezounschool.data.network.response.response_models.StudentActivityModel;

import com.tulib.alfaezounschool.R;

import java.util.ArrayList;

public class StudentActivity extends BaseActivity {
    RecyclerView recyclerViewStudentActivity;
    StudentActivityAdapter studentActivityAdapter;
    ArrayList<StudentActivityModel> studentActivityModelArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_student,frameLayout);
        initViews();
    }


    private void initViews() {
        recyclerViewStudentActivity = findViewById(R.id.recyclerStudentActivity);
        studentActivityModelArrayList = new ArrayList<>();
        studentActivityAdapter = new StudentActivityAdapter(StudentActivity.this,studentActivityModelArrayList);
        recyclerViewStudentActivity.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewStudentActivity.setAdapter(studentActivityAdapter);

    }


}