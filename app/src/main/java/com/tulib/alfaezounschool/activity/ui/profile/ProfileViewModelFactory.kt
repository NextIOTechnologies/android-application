package com.tulib.alfaezounschool.activity.ui.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IProfileRepository


@Suppress("UNCHECKED_CAST")
class ProfileViewModelFactory(var profileRepository: IProfileRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return ProfileViewModel(profileRepository) as T
    }
}