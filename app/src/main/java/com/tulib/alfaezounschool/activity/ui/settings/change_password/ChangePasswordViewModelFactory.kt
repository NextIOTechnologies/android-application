package com.tulib.alfaezounschool.activity.ui.settings.change_password

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IChangePasswordRepository


@Suppress("UNCHECKED_CAST")
class ChangePasswordViewModelFactory(var changePasswordRepository: IChangePasswordRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return ChangePasswordViewModel(changePasswordRepository) as T
    }
}