package com.tulib.alfaezounschool.activity.ui.attendance

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IAttendanceRepository

import kotlinx.coroutines.launch

class AttendanceViewModel(private var attendanceRepository: IAttendanceRepository) : ViewModel()
{
    private lateinit var liveDataForAttendanceResponse : MutableLiveData<AttendanceList>

    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

    var liveDataTotalPresent: MutableLiveData<String> = MutableLiveData()
    var liveDataTotalWeekEnd: MutableLiveData<String> = MutableLiveData()
    var liveDataTotalAbsent: MutableLiveData<String> = MutableLiveData()



    fun initializeLiveDataForAttendanceResponse(): LiveData<AttendanceList> {
        liveDataForAttendanceResponse = MutableLiveData()
        return liveDataForAttendanceResponse
    }
    fun emptyLiveDataForAttendanceResponse(){liveDataForAttendanceResponse.value= null}



    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getAttendanceList(id:String) {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = attendanceRepository.attendanceList(id)
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<AttendanceList>

                    if(data.data.totalcount!=null)
                    {
                        val total = data.data.totalcount
//                        liveDataTotalPresent.value = total.totalpresent.toString()
//                        liveDataTotalAbsent.value = total.totalabsent.toString()
//                        liveDataTotalWeekEnd.value = total.totalweekend.toString()
                    }
                     liveDataForAttendanceResponse.value = data.data

                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }


}
