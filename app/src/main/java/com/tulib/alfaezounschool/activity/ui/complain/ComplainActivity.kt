package com.tulib.alfaezounschool.activity.ui.complain

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.adapter.ComplainAdapter
import com.tulib.alfaezounschool.adapter.CustomDropDownAdapter
import com.tulib.alfaezounschool.adapter.UserDropDownAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.ComplainModel
import com.tulib.alfaezounschool.data.network.response.response_models.User
import com.tulib.alfaezounschool.data.network.response.response_models.UserTypeModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import com.tulib.alfaezounschool.utils.AppUtils.showSnackBar
import kotlinx.android.synthetic.main.activity_message_converstion.*
import kotlinx.android.synthetic.main.row_homework.*
import org.kodein.di.generic.instance
import java.util.*
import kotlin.collections.ArrayList


class ComplainActivity : BaseActivity() {
    private var spinnerDialogComplainUser: Spinner?=null
    private var edtDialogMessageSubject: EditText?=null
    private var edtDialogMessageWrite: EditText?=null
    private var spinnerDialogGroup: Spinner?=null

    private var btnDialogMessageSend: Button?=null
    var recyclerViewComplain: RecyclerView? = null
    var complainAdapter: ComplainAdapter? = null
    lateinit var complainModelArrayList: ArrayList<ComplainModel>
    var imgComplain: ImageView? = null
    var dialog: Dialog? = null
     var usertypeList : ArrayList<UserTypeModel>?=null
    private val complainViewModelFactory : ComplainViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: ComplainViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_complain, frameLayout)
        viewModel = ViewModelProvider(this, complainViewModelFactory).get(ComplainViewModel::class.java)


        initViews()
        initListeners()


        viewModel.initializeLiveDataForComplainSentResponse().observe(this,
                { result ->
                    if (result != null) {
                        dialog?.dismiss()
                        viewModel.getComplains()
                        viewModel.emptyLiveDataForComplainSentResponse()
                    }
                })



        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if (result != null) {
                        if (result) loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })




        // Redirecting to next destination
        viewModel.initializeLiveDataForComplainResponse().observe(this,
                { result ->
                    if (result != null) {
                        complainAdapter?.setData(result.complains)
                        usertypeList = result.usersList
                        viewModel.emptyLiveDataForComplainResponse()
                    }
                })


        viewModel.initializeLiveDataForComplainUserResponse().observe(this,
                { result ->
                    if (result != null) {
                        if (result.usersList.size > 0) {
                            val userDropDownAdapter = UserDropDownAdapter(this, result.usersList)
                            spinnerDialogComplainUser?.adapter = userDropDownAdapter
                            edtDialogMessageSubject?.isEnabled = true
                            edtDialogMessageWrite?.isEnabled = true
                            btnDialogMessageSend?.isEnabled = true
                            spinnerDialogComplainUser?.isEnabled = true
                        } else {
                            val adapter = ArrayAdapter.createFromResource(this, R.array.user, android.R.layout.simple_spinner_item);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerDialogComplainUser?.setAdapter(adapter);


                            edtDialogMessageSubject?.isEnabled = false
                            edtDialogMessageWrite?.isEnabled = false
                            btnDialogMessageSend?.isEnabled = false
                            spinnerDialogComplainUser?.isEnabled = false
                        }

                        viewModel.emptyLiveDataForComplainUserResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this, error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this as AppCompatActivity, error)
                viewModel.emptyLiveDataError()
            }
        })

        viewModel.getComplains()



    }

    private fun initViews() {
        dialog = Dialog(this)
        imgComplain = findViewById(R.id.imgComplain)
        recyclerViewComplain = findViewById(R.id.recyclerComplain)
        complainModelArrayList = ArrayList()

        complainAdapter = ComplainAdapter(this@ComplainActivity, complainModelArrayList)
        recyclerViewComplain?.setLayoutManager(LinearLayoutManager(this))
        recyclerViewComplain?.setAdapter(complainAdapter)
    }

    private fun initListeners() {
        imgMenu!!.setOnClickListener { drawerLayout!!.openDrawer(GravityCompat.START) }
        imgComplain!!.setOnClickListener { ComplainRegisterDialog() }
    }


    private fun ComplainRegisterDialog()
    {
        if(usertypeList!=null)
        {
            dialog!!.setContentView(R.layout.dialog_complain_register)
            val imgClose: ImageView

            //var linearDownload: LinearLayout
            imgClose = dialog!!.findViewById(R.id.imgDialogComplainRegisterClose)
            edtDialogMessageSubject = dialog!!.findViewById(R.id.edtDialogComplainSubject)
            edtDialogMessageWrite = dialog!!.findViewById(R.id.edtDialogComplainWrite)
            btnDialogMessageSend = dialog!!.findViewById(R.id.btnDialogComplainSend)
            spinnerDialogGroup = dialog!!.findViewById(R.id.spinnerDialogComplainGroup)
            spinnerDialogComplainUser = dialog!!.findViewById(R.id.spinnerDialogComplainUser)
            usertypeList?.add(UserTypeModel("Select Group", "0"))
            val customDropDownAdapter = CustomDropDownAdapter(this, usertypeList!!)
            spinnerDialogGroup?.adapter = customDropDownAdapter
            spinnerDialogGroup?.setSelection(customDropDownAdapter.getCount());
            imgClose.setOnClickListener { dialog!!.dismiss() }
            btnDialogMessageSend?.setOnClickListener {
                var condition_result=true;
                if(edtDialogMessageWrite?.text.toString().equals("")){
                    showSnackBar(this@ComplainActivity, "Message must be required");
                    condition_result=false;
                }
                if(edtDialogMessageSubject?.text.toString().equals("")){
                    showSnackBar(this@ComplainActivity, "Subject must be required");
                    condition_result=false;
                }


                viewModel.description = edtDialogMessageWrite?.text.toString()
                viewModel.title = edtDialogMessageSubject?.text.toString()
                viewModel.userId = (spinnerDialogComplainUser?.selectedItem as User).userID

                if(condition_result){viewModel.sendComplain()}

            }

            spinnerDialogGroup?.setOnItemSelectedListener(object : OnItemSelectedListener {
                override fun onItemSelected(arg0: AdapterView<*>?, arg1: View?, position: Int, id: Long) {
                    if (position != (usertypeList?.size ?: 0) - 1) {
                        viewModel.userTypeId = usertypeList?.get(position)?.usertypeID ?: ""
                        viewModel.getComplainUsers()
                    }
                }

                override fun onNothingSelected(arg0: AdapterView<*>?) {}
            })
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.show()
        }
    }




}