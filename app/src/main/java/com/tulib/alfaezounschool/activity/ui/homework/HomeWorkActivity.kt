package com.tulib.alfaezounschool.activity.ui.homework

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.adapter.HomeWorkAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.HomeWorkModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance
import java.util.*

class HomeWorkActivity : BaseActivity() {
    private val homeworkViewModelFactory : HomeworkViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: HomeWorkViewModel


    var recyclerViewHomeWork: RecyclerView? = null
    var homeWorkAdapter: HomeWorkAdapter? = null
    var homeWorkModelArrayList: ArrayList<HomeWorkModel>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_home_work, frameLayout)
        initViews()
       // initListeners()
        viewModel = ViewModelProvider(this, homeworkViewModelFactory).get(HomeWorkViewModel::class.java)

        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForHoemWorkResponse().observe(this,
                { result ->
                    if(result!=null)
                    {
                        homeWorkAdapter?.setData(result)
                        viewModel.emptyLiveDataForNotificationResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error)
                viewModel.emptyLiveDataError()
            }
        })
        AppController.studentid?.let {
            viewModel.getHomeworkList(it
            )
        }
    }

    private fun initViews() {
        recyclerViewHomeWork = findViewById(R.id.recyclerHomeWork)
        homeWorkModelArrayList = ArrayList()

        homeWorkAdapter = HomeWorkAdapter(this@HomeWorkActivity, homeWorkModelArrayList)
        recyclerViewHomeWork?.setLayoutManager(LinearLayoutManager(this))
        recyclerViewHomeWork?.setAdapter(homeWorkAdapter)
    }




}