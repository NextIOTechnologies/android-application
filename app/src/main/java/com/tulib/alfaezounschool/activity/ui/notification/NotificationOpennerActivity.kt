package com.tulib.alfaezounschool.activity.ui.notification

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity

class NotificationOpennerActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_notification_openner,frameLayout,true)

        val manager: FragmentManager = getSupportFragmentManager()
        val ft = manager.beginTransaction()
        ft.replace(R.id.content, NotificationFragment()).commit()
    }
}