package com.tulib.alfaezounschool.activity.ui.forgot_password.verification_code

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.utils.Common
import com.tulib.alfaezounschool.activity.ui.settings.change_password.ChangePasswordActivity
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.ResponseMessages
import com.tulib.alfaezounschool.databinding.ActivityVerificationCodeBinding
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import kotlinx.android.synthetic.main.activity_verification_code.*
import org.kodein.di.generic.instance

class VerificationCodeActivity : AppCompatActivity() {
    private var btnVerificationCode: Button? = null
    private val verifyCodeViewModelFactory : VerifyCodeViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: VerificationCodeViewModel

    var imgBack: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding : ActivityVerificationCodeBinding = DataBindingUtil.setContentView(this, R.layout.activity_verification_code);
        viewModel = ViewModelProvider(this, verifyCodeViewModelFactory).get(VerificationCodeViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this
        initViews()
        initListeners()

        // Redirecting to next destination
        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForForgotPasswordResponse().observe(this,
                { result ->
                    if(result!=null)
                    { AppUtils.showSnackBar(this,result,success = true)
                        viewModel.emptyLiveDataForLoginResponse()
                    }
                })

        viewModel.initializeLiveDataForVerifyCode().observe(this,
                { result ->
                    if(result!=null)
                    {  Common.CustomDialog(this@VerificationCodeActivity, ChangePasswordActivity::class.java, "Email Address Verified Successfully", "Change Password")
                        viewModel.emptyLiveDataForVerifyCode()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error)
                viewModel.emptyLiveDataError()
            }
        })
    }

    private fun initViews() {
        btnVerificationCode = findViewById(R.id.btnVerificationSubmit)
        imgBack = findViewById(R.id.imgVerificationBack)
    }

    private fun initListeners() {

        tvResendCode.setOnClickListener {
            viewModel.resendSmsCode()
        }
        btnVerificationCode!!.setOnClickListener {
            if(viewModel.validateData())
                viewModel.verifySmsCode()
            else
                AppUtils.showSnackBar(this, ResponseMessages.SOME_THING_WENT_WRONG.message)
        }
        imgBack!!.setOnClickListener { onBackPressed() }
    }
}