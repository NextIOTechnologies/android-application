package com.tulib.alfaezounschool.activity.ui.homework

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IHomeWorkRepository
import kotlinx.coroutines.launch

class HomeWorkViewModel(private var homeWorkRepository: IHomeWorkRepository) : ViewModel()
{
    private lateinit var liveDataForHomeWorkResponse : MutableLiveData<ArrayList<HomeWorkModel>>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

    fun initializeLiveDataForHoemWorkResponse(): LiveData<ArrayList<HomeWorkModel>> {
        liveDataForHomeWorkResponse = MutableLiveData()
        return liveDataForHomeWorkResponse
    }
    fun emptyLiveDataForNotificationResponse(){liveDataForHomeWorkResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getHomeworkList(id:String) {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = homeWorkRepository.homeWork(id)
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<HomeWorkList>
                    if(data.data.homeWorkList.size==0)
                    {
                        liveDataError.value = "No Homework Available"
                    }
                    else liveDataForHomeWorkResponse.value = data.data.homeWorkList
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
