package com.tulib.alfaezounschool.activity.ui.settings.change_password

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.BaseResponse
import com.tulib.alfaezounschool.data.network.response.response_models.MainResponse
import com.tulib.alfaezounschool.data.network.response.response_models.MessageResponse
import com.tulib.alfaezounschool.data.repository.IChangePasswordRepository
import com.tulib.alfaezounschool.model.ForgotPasswordSingletonModel
import com.tulib.alfaezounschool.utils.DataUtil
import kotlinx.coroutines.launch

class ChangePasswordViewModel(private var changePasswordRepository: IChangePasswordRepository) : ViewModel()
{
    private lateinit var liveDataForPasswordResponse : MutableLiveData<String>
    private lateinit var liveDataForForgotPasswordResponse : MutableLiveData<String>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>


    var liveDataOldPassword: MutableLiveData<String> = MutableLiveData()
    var liveDataNewPassword: MutableLiveData<String> = MutableLiveData()


    fun initializeLiveDataForPasswordResponse(): LiveData<String> {
        liveDataForPasswordResponse = MutableLiveData()
        return liveDataForPasswordResponse
    }
    fun emptyLiveDataForPasswordResponse(){liveDataForPasswordResponse.value= null}

    fun initializeLiveDataForForgotPasswordResponse (): LiveData<String> {
        liveDataForForgotPasswordResponse = MutableLiveData()
        return liveDataForForgotPasswordResponse
    }
    fun emptyLiveDataForForgotPasswordResponse (){liveDataForForgotPasswordResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun validateData()  = (liveDataOldPassword.value?:"").length>0 && (liveDataNewPassword.value?:"").length>0


    fun changePassword() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = changePasswordRepository.changePassword(
                    DataUtil.getChangePasswordJson(liveDataOldPassword.value?.trim() ?: "",
                            liveDataNewPassword.value?.trim() ?: ""))
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<MessageResponse>
                    liveDataForPasswordResponse.value = data.message
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }

    fun forgotPassword() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = changePasswordRepository.forgotPassword(
                    DataUtil.getForgotPasswordJson(ForgotPasswordSingletonModel.getModel().email?:"",
                            ForgotPasswordSingletonModel.getModel().smsCode?:"",liveDataOldPassword.value?.trim() ?: ""))
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<MessageResponse>
                    liveDataForForgotPasswordResponse.value = data.message
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
