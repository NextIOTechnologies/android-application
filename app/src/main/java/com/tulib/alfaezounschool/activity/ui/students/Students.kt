package com.tulib.alfaezounschool.activity.ui.students

import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.adapter.StudentsAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.StudentModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance
import java.util.*

class Students : BaseActivity() {
    private val studentsViewModelFactory : StudentsViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: StudentsViewModel


    var recyclerViewStudents: RecyclerView? = null
    var studentsAdapter: StudentsAdapter? = null
    var studentsModelArrayList: ArrayList<StudentModel>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_students, frameLayout)
        initViews()
        initListeners()
        viewModel = ViewModelProvider(this, studentsViewModelFactory).get(StudentsViewModel::class.java)

        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeliveDataForStudentResponse().observe(this,
                { result ->
                    if(result!=null)
                    {
                        studentsAdapter?.setData(result.studentsList)
                        viewModel.emptyliveDataForStudentResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error)
                viewModel.emptyLiveDataError()
            }
        })
            viewModel.getStudents()
        }

    private fun initViews() {
        recyclerViewStudents = findViewById(R.id.recyclerStudents)
        studentsModelArrayList = ArrayList()

        studentsAdapter = StudentsAdapter(this@Students, studentsModelArrayList)
        recyclerViewStudents?.setLayoutManager(LinearLayoutManager(this))
        recyclerViewStudents?.setAdapter(studentsAdapter)
    }

    private fun initListeners() {
        imgMenu!!.setOnClickListener { drawerLayout!!.openDrawer(GravityCompat.START) }
    }


}