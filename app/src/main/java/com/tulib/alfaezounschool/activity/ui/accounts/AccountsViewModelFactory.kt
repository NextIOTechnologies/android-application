package com.tulib.alfaezounschool.activity.ui.accounts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IAccountsRepository


@Suppress("UNCHECKED_CAST")
class AccountsViewModelFactory(private var accountsRepository: IAccountsRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return AccountsViewModel(accountsRepository) as T
    }
}