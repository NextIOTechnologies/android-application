package com.tulib.alfaezounschool.activity.ui.library

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.ILibraryRepository
import kotlinx.coroutines.launch

class LibraryViewModel(private var libraryRepository: ILibraryRepository) : ViewModel()
{
    private lateinit var liveDataForLibraryResponse : MutableLiveData<ArrayList<LibraryModel>>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

    fun initializeLiveDataForLibraryResponse(): LiveData<ArrayList<LibraryModel>> {
        liveDataForLibraryResponse = MutableLiveData()
        return liveDataForLibraryResponse
    }
    fun emptyLiveDataForLibraryResponse(){liveDataForLibraryResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getLibraryList(id:String) {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = libraryRepository.libraryList(id)
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<LibraryList>
                    liveDataForLibraryResponse.value = data.data.libraryList
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
