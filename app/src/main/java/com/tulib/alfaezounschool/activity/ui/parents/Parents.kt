package com.tulib.alfaezounschool.activity.ui.parents

import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.adapter.ParentsAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.ParentsModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance
import java.util.*

class Parents : BaseActivity() {
    private val parentsViewModelFactory : ParentsViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: ParentsViewModel


    var recyclerViewParents: RecyclerView? = null
    var parentsAdapter: ParentsAdapter? = null
    var parentsModelArrayList: ArrayList<ParentsModel>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_parents, frameLayout)
        initViews()
        initListeners()
        viewModel = ViewModelProvider(this, parentsViewModelFactory).get(ParentsViewModel::class.java)

        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeliveDataForParentResponse().observe(this,
                { result ->
                    if(result!=null)
                    {
                        parentsAdapter?.setData(result.parentsList)
                        viewModel.emptyliveDataForParentResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error)
                viewModel.emptyLiveDataError()
            }
        })
            viewModel.getParents()
        }

    private fun initViews() {
        recyclerViewParents = findViewById(R.id.recyclerParents)
        parentsModelArrayList = ArrayList()

        parentsAdapter = ParentsAdapter(this@Parents, parentsModelArrayList)
        recyclerViewParents?.setLayoutManager(LinearLayoutManager(this))
        recyclerViewParents?.setAdapter(parentsAdapter)
    }

    private fun initListeners() {
        imgMenu!!.setOnClickListener { drawerLayout!!.openDrawer(GravityCompat.START) }
    }


}