package com.tulib.alfaezounschool.activity.ui.announcements.holiday

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.adapter.HolidayAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.HolidayModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance

class HolidayFragment : Fragment()
{

    private val holidayViewModelFactory : HolidayViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: HolidayViewModel

    var recyclerHoliday: RecyclerView? = null
    var holidayAdapter: HolidayAdapter? = null
    lateinit  var   holidayArrayList : ArrayList<HolidayModel> ;


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_holiday, container, false)
        viewModel = ViewModelProvider(this, holidayViewModelFactory).get(HolidayViewModel::class.java)
        initViews(view)
        return view
    }

    private fun initViews(v: View) {
        recyclerHoliday = v.findViewById(R.id.recyclerHoliday)
        holidayArrayList =  ArrayList();
        holidayAdapter =  HolidayAdapter(requireActivity(),holidayArrayList);
        recyclerHoliday?.setLayoutManager(LinearLayoutManager(activity))
        recyclerHoliday?.setAdapter(holidayAdapter)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.initializeLiveDataForLoader().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        //   if(result)loaderDialog.showLoader(requireActivity() as AppCompatActivity) else loaderDialog.hideLoader(requireActivity() as AppCompatActivity)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForHolidayResponse().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        holidayAdapter?.setData(result)
                        viewModel.emptyLiveDataForHolidayResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error)
                viewModel.emptyLiveDataError()
            }
        })

        viewModel.getHolidayList()
    }
}