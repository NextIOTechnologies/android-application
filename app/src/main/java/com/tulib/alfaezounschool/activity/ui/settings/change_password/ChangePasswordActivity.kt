package com.tulib.alfaezounschool.activity.ui.settings.change_password

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.login.LoginActivity
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.ResponseMessages
import com.tulib.alfaezounschool.databinding.ActivityChangePasswordBinding
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.model.ForgotPasswordSingletonModel
import com.tulib.alfaezounschool.utils.AppUtils
import com.tulib.alfaezounschool.utils.Common
import kotlinx.android.synthetic.main.activity_change_password.*
import org.kodein.di.generic.instance

class ChangePasswordActivity : AppCompatActivity() {
    private val changePasswordViewModelFactory : ChangePasswordViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: ChangePasswordViewModel
    var btnSubmit: Button? = null
    var imgBack: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityChangePasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        viewModel = ViewModelProvider(this, changePasswordViewModelFactory).get(ChangePasswordViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this
        initViews()
        initListeners()

        ForgotPasswordSingletonModel.getModel().email?.let {
            edtNewPassword.hint =  "New Password"
            edtNewPassword.hint =  "Confirm Password"
        }
        // Redirecting to next destination
        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForPasswordResponse().observe(this,
                { result ->
                    if(result!=null)
                    {  AppUtils.showSnackBar(this,result,success = true)
                        viewModel.emptyLiveDataForPasswordResponse()
                        onBackPressed()
                    }
                })

        // Redirecting to next destination
        viewModel.initializeLiveDataForForgotPasswordResponse().observe(this,
                { result ->
                    if(result!=null)
                    {   Common.CustomDialog(this@ChangePasswordActivity, LoginActivity::class.java,"Password Changed Successfully","Login");
                       ForgotPasswordSingletonModel.clearModel()
                        viewModel.emptyLiveDataForForgotPasswordResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error)
                viewModel.emptyLiveDataError()
            }
        })
    }

    private fun initViews() {
        btnSubmit = findViewById(R.id.btnChangePasswordSubmit)
        imgBack = findViewById(R.id.imgChangePasswordBack)
    }

    private fun initListeners() {
        btnSubmit!!.setOnClickListener {
            if(viewModel.validateData())
            {
                if(ForgotPasswordSingletonModel.getModel().email!=null)
                {
                    viewModel.forgotPassword()
                }
                else
                    viewModel.changePassword()
            }
            else  AppUtils.showSnackBar(this, ResponseMessages.SOME_THING_WENT_WRONG.message)
        }
        imgBack!!.setOnClickListener { onBackPressed() }
    }
}
