package com.tulib.alfaezounschool.activity.ui.complain

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IComplainRepository


@Suppress("UNCHECKED_CAST")
class ComplainViewModelFactory(private var complainRepository: IComplainRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return ComplainViewModel(complainRepository) as T
    }
}