package com.tulib.alfaezounschool.activity.ui.accounts

import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.adapter.AccountsAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.AccountsModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance
import java.util.*

class Accounts : BaseActivity() {
    private val accountsViewModelFactory : AccountsViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: AccountsViewModel


    var recyclerViewAccounts: RecyclerView? = null
    var accountsAdapter: AccountsAdapter? = null
    var accountsModelArrayList: ArrayList<AccountsModel>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_account, frameLayout)
        initViews()
        initListeners()
        viewModel = ViewModelProvider(this, accountsViewModelFactory).get(AccountsViewModel::class.java)

        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForAccountsResponse().observe(this,
                { result ->
                    if(result!=null)
                    {
                        accountsAdapter?.setData(result)
                        viewModel.emptyLiveDataForNotificationResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error)
                viewModel.emptyLiveDataError()
            }
        })

        viewModel.getAccountsList()
    }

    private fun initViews() {
        recyclerViewAccounts = findViewById(R.id.recyclerAccount)
        accountsModelArrayList = ArrayList()

        accountsAdapter = AccountsAdapter(this@Accounts, accountsModelArrayList)
        recyclerViewAccounts?.setLayoutManager(LinearLayoutManager(this))
        recyclerViewAccounts?.setAdapter(accountsAdapter)
    }

    private fun initListeners() {
        imgMenu!!.setOnClickListener { drawerLayout!!.openDrawer(GravityCompat.START) }
    }


}