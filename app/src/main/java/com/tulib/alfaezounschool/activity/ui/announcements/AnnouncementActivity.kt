package com.tulib.alfaezounschool.activity.ui.announcements

import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.viewpager.widget.ViewPager
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.activity.ui.announcements.event.EventFragment
import com.tulib.alfaezounschool.activity.ui.announcements.notice.NoticeFragment
import com.tulib.alfaezounschool.adapter.ViewPagerAdapter
import com.tulib.alfaezounschool.activity.ui.announcements.holiday.HolidayFragment
import com.google.android.material.tabs.TabLayout
import com.tulib.alfaezounschool.utils.LocaleHelper
import java.util.*

class AnnouncementActivity : BaseActivity() {
    private var tabLayout: TabLayout? = null
    private var viewPager: ViewPager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_announcement, frameLayout)
        initViews()
        initListeners()
    }

    private fun initViews() {
        viewPager = findViewById(R.id.viewPager)
        setupViewPager(viewPager)
        tabLayout = findViewById(R.id.tabs)
        tabLayout?.setupWithViewPager(viewPager)
    }

    private fun initListeners() {
        imgMenu!!.setOnClickListener { drawerLayout!!.openDrawer(GravityCompat.START) }
    }

    private fun setupViewPager(viewPager: ViewPager?) {
        val lang = LocaleHelper.getLanguage(this)

        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(NoticeFragment(), getStringByLocale(R.string.notice, Locale(lang)))
        adapter.addFragment(EventFragment(), getStringByLocale(R.string.event, Locale(lang)))
        adapter.addFragment(HolidayFragment(), getStringByLocale(R.string.holiday, Locale(lang)))
        viewPager!!.adapter = adapter
    }
}