package com.tulib.alfaezounschool.activity.ui.parents

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IParentsRepository


@Suppress("UNCHECKED_CAST")
class ParentsViewModelFactory(private var parentsRepository: IParentsRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return ParentsViewModel(parentsRepository) as T
    }
}