package com.tulib.alfaezounschool.activity.ui.dashboard

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.db.database.AppDatabase
import com.tulib.alfaezounschool.data.repository.IAttendanceRepository
import com.tulib.alfaezounschool.data.repository.ICommonRepository
import com.tulib.alfaezounschool.data.repository.INoticeRepository

@Suppress("UNCHECKED_CAST")
class DashboardViewModelFactory(private var noticeRepository: INoticeRepository, private var commonRepository: ICommonRepository,
                                private var attendanceRepository: IAttendanceRepository,
                                private var appDatabase: AppDatabase) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return DashboardViewModel(noticeRepository,commonRepository,attendanceRepository,appDatabase) as T
    }
}