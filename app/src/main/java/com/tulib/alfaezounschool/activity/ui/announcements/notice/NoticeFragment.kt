package com.tulib.alfaezounschool.activity.ui.announcements.notice

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.adapter.NoticeAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.NoticeModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance

class NoticeFragment : Fragment() {

    private val noticeViewModelFactory : NoticeViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: NoticeViewModel

    var recyclerViewNotificaion: RecyclerView? = null
    var noticeAdapter: NoticeAdapter? = null

  lateinit  var   notificationModelArrayList : ArrayList<NoticeModel> ;
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_notice, container, false)
        viewModel = ViewModelProvider(this, noticeViewModelFactory).get(NoticeViewModel::class.java)
        initViews(v)
        return v
    }

    private fun initViews(v: View) {
        recyclerViewNotificaion = v.findViewById(R.id.recyclerNotice)
         notificationModelArrayList =  ArrayList();
        noticeAdapter =  NoticeAdapter(requireActivity(),notificationModelArrayList);
        recyclerViewNotificaion?.setLayoutManager(LinearLayoutManager(activity))
        recyclerViewNotificaion?.setAdapter(noticeAdapter)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.initializeLiveDataForLoader().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(requireActivity() as AppCompatActivity) else loaderDialog.hideLoader(requireActivity() as AppCompatActivity)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForNoticeResponse().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        noticeAdapter?.setData(result)
                        viewModel.emptyLiveDataForNoticeResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error)
                viewModel.emptyLiveDataError()
            }
        })

        viewModel.getNoticeList()
    }
}