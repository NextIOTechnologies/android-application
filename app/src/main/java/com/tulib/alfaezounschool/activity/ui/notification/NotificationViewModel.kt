package com.tulib.alfaezounschool.activity.ui.notification

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.INotificationRepository
import kotlinx.coroutines.launch

class NotificationViewModel(private var notificationRepository: INotificationRepository) : ViewModel()
{
    private lateinit var liveDataForNotificationResponse : MutableLiveData<ArrayList<NotificationModel>>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

    fun initializeLiveDataForNotificationResponse(): LiveData<ArrayList<NotificationModel>> {
        liveDataForNotificationResponse = MutableLiveData()
        return liveDataForNotificationResponse
    }
    fun emptyLiveDataForNotificationResponse(){liveDataForNotificationResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getNotificationList() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = notificationRepository.notifications()
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<NotificationList>
                    if(data.data.notificationList.size==0)
                    {
                        liveDataError.value = "No Notifications Available"
                    }
                    else liveDataForNotificationResponse.value = data.data.notificationList
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
