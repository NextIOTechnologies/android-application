package com.tulib.alfaezounschool.activity.ui.marks

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.squareup.picasso.Picasso
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.data.db.database.AppDatabase
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IMarksRepository
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


class MarksViewModel(var marksRepository: IMarksRepository,var database: AppDatabase) : ViewModel()
{
    private lateinit var liveDataForMarksResponse : MutableLiveData<MarksList>
      var liveDataStudentName = MutableLiveData<String>()
    private lateinit var liveDataStudentClass : MutableLiveData<String>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>
    var liveDataUserClass: MutableLiveData<String> = MutableLiveData()


    fun getImageUrl(): String? {

        var url =""
        runBlocking {
            url =  database.getUserDao().getUser()?.photo?:""
        }
        // The URL will usually come from a model (i.e Profile)
        return url
    }

    init {
        viewModelScope.launch{
            liveDataStudentName.value = database.getUserDao().getUser()?.name
            liveDataUserClass.value = database.getUserDao().getUser()?.userClass?:"Class 10-A"
        }

    }
    fun initializeLiveDataForMarksResponse(): LiveData<MarksList> {
        liveDataForMarksResponse = MutableLiveData()
        return liveDataForMarksResponse
    }
    fun emptyLiveDataForMarksResponse(){liveDataForMarksResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}

    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}

    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}



    fun marks(id:String) {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = marksRepository.marks(id)

            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<MarksList>
                    if(data.data.marksList.size==0)
                    {
                        liveDataError.value = "No Marks Available"
                    }
                    else liveDataForMarksResponse.value = data.data

                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}

@BindingAdapter("bind:imageUrl")
fun loadImage(view: ImageView, imageUrl: String?) {
    Picasso.get()
            .load(imageUrl)
            .placeholder(R.drawable.pic)
            .into(view)
}
