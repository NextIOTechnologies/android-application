package com.tulib.alfaezounschool.activity.ui.message.message_view

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.adapter.CustomDropDownAdapter
import com.tulib.alfaezounschool.adapter.MessageAdapter
import com.tulib.alfaezounschool.adapter.UserDropDownAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.MessagesModel
import com.tulib.alfaezounschool.data.network.response.response_models.User
import com.tulib.alfaezounschool.data.network.response.response_models.UserTypeModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 * Use the [MessageFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MessageFragment : Fragment() {

    private var spinnerDialogMessageUser: Spinner?=null
    private var edtDialogMessageSubject: EditText?=null
    private var edtDialogMessageWrite: EditText?=null
    private var  btnDialogMessageSend: Button?=null

    var recyclerViewMessage: RecyclerView? = null
    var messageAdapter: MessageAdapter? = null
    var messageModelArrayList: ArrayList<MessagesModel>? = null
    var imgMenu: ImageView? = null
    var imgMessage: ImageView? = null
    var dialog: Dialog? = null
    lateinit var userTypeList : ArrayList<UserTypeModel>

    private val messageViewModelFactory : MessageViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: MessageViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_message, container, false)
        viewModel = ViewModelProvider(this, messageViewModelFactory).get(MessageViewModel::class.java)
        initViews(v)
        initListeners()
        return v
    }

    private fun initViews(v: View) {
        dialog = Dialog(requireContext())
        imgMessage = v.findViewById(R.id.imgMessage)
        recyclerViewMessage = v.findViewById(R.id.recyclerMessage)
        messageModelArrayList = ArrayList()

        messageAdapter = MessageAdapter(requireContext(), messageModelArrayList)
        recyclerViewMessage?.setLayoutManager(LinearLayoutManager(activity))
        recyclerViewMessage?.setAdapter(messageAdapter)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.initializeLiveDataForConversationSentResponse().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        viewModel.getMessageList()
                       dialog?.dismiss()
                        viewModel.emptyLiveDataForConversationSentResponse()
                    }
                })

        viewModel.initializeLiveDataForMessageUserResponse().observe(viewLifecycleOwner,
                { result ->
                    if (result != null ) {
                        if( result.usersList.size>0)
                        {
                            val userDropDownAdapter = UserDropDownAdapter(requireContext(), result.usersList)
                            spinnerDialogMessageUser?.adapter = userDropDownAdapter
                            edtDialogMessageSubject?.isEnabled=true
                            edtDialogMessageWrite?.isEnabled=true
                            btnDialogMessageSend?.isEnabled=true
                            spinnerDialogMessageUser?.isEnabled=true
                        }else {
                            val adapter = ArrayAdapter.createFromResource(requireActivity() as AppCompatActivity, R.array.user, android.R.layout.simple_spinner_item);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerDialogMessageUser?.setAdapter(adapter);


                            edtDialogMessageSubject?.isEnabled = false
                            edtDialogMessageWrite?.isEnabled = false
                            btnDialogMessageSend?.isEnabled = false
                            spinnerDialogMessageUser?.isEnabled = false
                        }

                        viewModel.emptyLiveDataForMessageUserResponse()
                    }
                })



        viewModel.initializeLiveDataForLoader().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(requireActivity() as AppCompatActivity) else loaderDialog.hideLoader(requireActivity() as AppCompatActivity)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForConversationResponse().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        messageAdapter?.setData(result.conversationList)
                        userTypeList = result.usersList
                        viewModel.emptyLiveDataForConversationResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error)
                viewModel.emptyLiveDataError()
            }
        })

        viewModel.getMessageList()
    }

    private fun initListeners() {
        imgMessage!!.setOnClickListener { MessageDialog() }
    }

    private fun MessageDialog()
    {
        dialog!!.setContentView(R.layout.dialog_message)
        val imgClose: ImageView
        val spinnerDialogGroup: Spinner

        //var linearDownload: LinearLayout
        imgClose = dialog!!.findViewById(R.id.imgDialogMessageClose)
        edtDialogMessageSubject = dialog!!.findViewById(R.id.edtDialogMessageSubject)
        edtDialogMessageWrite = dialog!!.findViewById(R.id.edtDialogMessageWrite)
        btnDialogMessageSend = dialog!!.findViewById(R.id.btnDialogMessageSend)
        spinnerDialogGroup = dialog!!.findViewById(R.id.spinnerDialogGroup)
        spinnerDialogMessageUser = dialog!!.findViewById(R.id.spinnerDialogUser)
        userTypeList.add(UserTypeModel("Select Group","0"))
        val customDropDownAdapter = CustomDropDownAdapter(this.requireContext(),userTypeList )
        spinnerDialogGroup.adapter = customDropDownAdapter
        spinnerDialogGroup?.setSelection(customDropDownAdapter.getCount());
        imgClose.setOnClickListener { dialog!!.dismiss() }
        btnDialogMessageSend?.setOnClickListener {
            var condition_result=true;
            if(edtDialogMessageWrite?.text.toString().equals("")){
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity, "Message must be required");
                condition_result=false;
            }
            if(edtDialogMessageSubject?.text.toString().equals("")){
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity, "Subject must be required");
                condition_result=false;
            }

            viewModel.message = edtDialogMessageWrite?.text.toString()
            viewModel.subject = edtDialogMessageSubject?.text.toString()
            viewModel.userId = (spinnerDialogMessageUser?.selectedItem as User).userID




            if(condition_result){viewModel.sendNewMessage()}
            }



        spinnerDialogGroup?.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>?, arg1: View?, position: Int, id: Long) {
                if (position != userTypeList?.size - 1) {
                    viewModel.userTypeId = userTypeList.get(position).usertypeID
                    viewModel.getMessageUsers()
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        })

        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.show()
    }
}