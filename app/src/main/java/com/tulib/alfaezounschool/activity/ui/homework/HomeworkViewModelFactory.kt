package com.tulib.alfaezounschool.activity.ui.homework

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IHomeWorkRepository



@Suppress("UNCHECKED_CAST")
class HomeworkViewModelFactory(private var homeWorkRepository: IHomeWorkRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return HomeWorkViewModel(homeWorkRepository) as T
    }
}