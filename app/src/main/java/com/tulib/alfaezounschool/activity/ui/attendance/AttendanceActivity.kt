package com.tulib.alfaezounschool.activity.ui.attendance

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.github.sundeepk.compactcalendarview.CompactCalendarView
import com.github.sundeepk.compactcalendarview.CompactCalendarView.CompactCalendarViewListener
import com.github.sundeepk.compactcalendarview.domain.Event
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.AttendanceModel
import com.tulib.alfaezounschool.databinding.ActivityAttendanceBinding
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AttendanceActivity : BaseActivity() {

    private val attendanceViewModelFactory: AttendanceViewModelFactory by AppController.kodein().instance()
    private val loaderDialog: LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: AttendanceViewModel
    private var attendenceList: ArrayList<AttendanceModel>? = null


    var compactCalendarView: CompactCalendarView? = null
    private val simpleDateFormat = SimpleDateFormat("MMMM-yyyy", Locale.getDefault())
    private val DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    var tx_date: TextView? = null
    var ly_left: LinearLayout? = null
    var ly_right: LinearLayout? = null
    var myCalendar: Calendar? = null
    var c: Date? = null
    var formattedDate: String? = null

    var txtTotalPresent: TextView? = null
    var txtTotalAbsent: TextView? = null
    var txtTotalLeave: TextView? = null

    var txtTotalHoliday: TextView? = null
    var txtTotalLate: TextView? = null
    var txtTotalLateExcuse: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityAttendanceBinding = DataBindingUtil.inflate(layoutInflater, R.layout.activity_attendance, frameLayout, true);
        viewModel = ViewModelProvider(this, attendanceViewModelFactory).get(AttendanceViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this



        init()
        calendarlistener()
        setdate()
        tx_date!!.text = "" + formattedDate
        ly_right!!.setOnClickListener {
            compactCalendarView!!.showCalendarWithAnimation()
            compactCalendarView!!.showPreviousMonth()
        }
        ly_left!!.setOnClickListener {
            compactCalendarView!!.showCalendarWithAnimation()
            compactCalendarView!!.showNextMonth()
        }

        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if (result != null) {
                        if (result) loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForAttendanceResponse().observe(this,
                { result ->
                    if (result != null) {

                        attendenceList = result.attendanceList
                        setdate(attendenceList)
                        viewModel.emptyLiveDataForAttendanceResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this, error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this as AppCompatActivity, error)
                viewModel.emptyLiveDataError()
            }
        })

        AppController.studentid?.let {
            viewModel.getAttendanceList(it
            )
        }
    }


    fun init() {
        compactCalendarView = findViewById<View>(R.id.compactcalendar_view) as CompactCalendarView
        tx_date = findViewById<View>(R.id.text) as TextView
        ly_left = findViewById<View>(R.id.layout_left) as LinearLayout
        ly_right = findViewById<View>(R.id.layout_right) as LinearLayout
        txtTotalPresent = findViewById(R.id.txtTotalPresent)
        txtTotalAbsent = findViewById(R.id.txtTotalAbsent)
        txtTotalLeave = findViewById(R.id.txtTotalLeave)

        txtTotalHoliday = findViewById(R.id.txtTotalHoliday)
        txtTotalLate = findViewById(R.id.txtTotalLate)
        txtTotalLateExcuse = findViewById(R.id.txtTotalLateExcuse)


    }

    //calendar method
    fun calendarlistener() {
        compactCalendarView!!.setListener(object : CompactCalendarViewListener {
            override fun onDayClick(dateClicked: Date) {
//                if (DateFormat.format(dateClicked) == "2018-11-21") {
//                    Toast.makeText(applicationContext, DateFormat.format(dateClicked) + " This day your brother birth day ", Toast.LENGTH_LONG).show()
//                } else {
//                    Toast.makeText(applicationContext, DateFormat.format(dateClicked) + " In This day no Events Available", Toast.LENGTH_LONG).show()
//                }
            }

            override fun onMonthScroll(firstDayOfNewMonth: Date) {
                compactCalendarView!!.removeAllEvents()
                setdate(attendenceList)
                tx_date!!.text = simpleDateFormat.format(firstDayOfNewMonth)
            }
        })
    }

    //get current date
    fun setdate(attendenceList: ArrayList<AttendanceModel>? = null) {

        c = Calendar.getInstance().time
        formattedDate = simpleDateFormat!!.format(c)
        compactCalendarView!!.setUseThreeLetterAbbreviation(true)
        myCalendar = Calendar.getInstance()
        attendenceList?.let {

            val simpleMonthYearFormat = SimpleDateFormat("MM-yyyy")
            val simpleYearFormat = SimpleDateFormat("yyyy")
            val simpleMonthFormat = SimpleDateFormat("MM")
            val currentMonth = compactCalendarView!!.firstDayOfCurrentMonth
            var position = -1
            for (j in it.indices) {
                val availableMonth = simpleMonthYearFormat.parse(it[j].monthyear)

                if (availableMonth == currentMonth) {
                    position = j
                    break
                }
            }
            if (position > -1) {

                var monthly_total_present = 0;
                var monthly_total_absent = 0;
                var monthly_total_leave = 0;

                var monthly_total_holiday = 0;
                var monthly_total_late = 0;
                var monthly_total_late_excuse = 0;
                for (i in 1..31) {

                    myCalendar?.set(Calendar.YEAR, simpleYearFormat.format(currentMonth).toInt())
                    myCalendar?.set(Calendar.MONTH, simpleMonthFormat.format(currentMonth).toInt() - 1)
                    myCalendar?.set(Calendar.DAY_OF_MONTH, i)

                    val value = getValue(it[position], i)
                    val color = when (value) {
                        "P" -> "#38b859"
                        "A" -> "#f42727"
                        "LA" -> "#f9ac58"
                        "H" -> "#17177f"
                        "L" -> "#844206"
                        "LE" -> "#1ac1a1"
                        else -> ""
                    }
                    if (value.equals("P")) {
                        monthly_total_present += 1;
                    } else if(value.equals("A")){
                        monthly_total_absent += 1
                    }else if(value.equals("LA")){
                        monthly_total_leave += 1
                    }else if(value.equals("H")){
                        monthly_total_holiday += 1
                    }else if(value.equals("L")){
                        monthly_total_late += 1
                    }else if(value.equals("LE")){
                        monthly_total_late_excuse += 1
                    }


                    if (color.isNotEmpty()) {
                        val event = Event(Color.parseColor(color), myCalendar!!.getTimeInMillis(), "test")
                        compactCalendarView!!.addEvent(event)
                    }
                }
                txtTotalPresent?.setText(monthly_total_present.toString())
                txtTotalAbsent?.setText(monthly_total_absent.toString())
                txtTotalLeave?.setText(monthly_total_leave.toString())
                txtTotalHoliday?.setText(monthly_total_holiday.toString())
                txtTotalLate?.setText(monthly_total_late.toString())
                txtTotalLateExcuse?.setText(monthly_total_late_excuse.toString())

            }

        }
    }

    fun getValue(attendanceModel: AttendanceModel, position: Int): String? {
        return when (position) {
            1 -> attendanceModel.a1
            2 -> attendanceModel.a2
            3 -> attendanceModel.a3
            4 -> attendanceModel.a4
            5 -> attendanceModel.a5
            6 -> attendanceModel.a6
            7 -> attendanceModel.a7
            8 -> attendanceModel.a8
            9 -> attendanceModel.a9
            10 -> attendanceModel.a10
            11 -> attendanceModel.a11
            12 -> attendanceModel.a12
            13 -> attendanceModel.a13
            14 -> attendanceModel.a14
            15 -> attendanceModel.a15
            16 -> attendanceModel.a16
            17 -> attendanceModel.a17
            18 -> attendanceModel.a18
            19 -> attendanceModel.a19
            20 -> attendanceModel.a20
            21 -> attendanceModel.a21
            22 -> attendanceModel.a22
            23 -> attendanceModel.a23
            24 -> attendanceModel.a24
            25 -> attendanceModel.a25
            26 -> attendanceModel.a26
            27 -> attendanceModel.a27
            28 -> attendanceModel.a28
            29 -> attendanceModel.a29
            30 -> attendanceModel.a30
            31 -> attendanceModel.a31
            else -> null
        }

    }
}