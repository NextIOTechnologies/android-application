package com.tulib.alfaezounschool.activity.ui.forgot_password.verification_code

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.BaseResponse
import com.tulib.alfaezounschool.data.network.response.response_models.MainResponse
import com.tulib.alfaezounschool.data.network.response.response_models.MessageResponse
import com.tulib.alfaezounschool.data.repository.IForgotPasswordRepository
import com.tulib.alfaezounschool.model.ForgotPasswordSingletonModel
import com.tulib.alfaezounschool.utils.DataUtil
import kotlinx.coroutines.launch

class VerificationCodeViewModel(private var forgotPasswordRepository: IForgotPasswordRepository) : ViewModel()
{
    private lateinit var liveDataForForgotPasswordResponse : MutableLiveData<String>
    private lateinit var liveDataForVerifyCode: MutableLiveData<Boolean>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

    var liveDataDigit: MutableLiveData<String> = MutableLiveData()


    fun initializeLiveDataForVerifyCode(): LiveData<Boolean> {
        liveDataForVerifyCode = MutableLiveData()
        return liveDataForVerifyCode
    }
    fun emptyLiveDataForVerifyCode(){liveDataForVerifyCode.value= null}

    fun initializeLiveDataForForgotPasswordResponse(): LiveData<String> {
        liveDataForForgotPasswordResponse = MutableLiveData()
        return liveDataForForgotPasswordResponse
    }
    fun emptyLiveDataForLoginResponse(){liveDataForForgotPasswordResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}

    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}

    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun validateData()  = (liveDataDigit.value?:"").length==4

    fun resendSmsCode() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = forgotPasswordRepository.forgotPasswordEmail(
                    DataUtil.getForgotPasswordEmailJson( ForgotPasswordSingletonModel.getModel().email?:""))
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<MessageResponse>
                    liveDataDigit.value=""
                    liveDataForForgotPasswordResponse.value = data.data.message
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }


    fun verifySmsCode() {
        viewModelScope.launch {
            ForgotPasswordSingletonModel.getModel().smsCode = liveDataDigit.value?:""
            liveDataForLoader.value = true
            val authResponse = forgotPasswordRepository.forgotPasswordVerificationCode(
                    DataUtil.getVerifySmsCodeJson( ForgotPasswordSingletonModel.getModel().email?:"",liveDataDigit.value?:""))
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> { liveDataForVerifyCode.value = true }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
