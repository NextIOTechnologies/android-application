package com.tulib.alfaezounschool.activity.ui.message.conversation_view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tulib.alfaezounschool.R
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.adapter.ConversationAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.ConversationModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import kotlinx.android.synthetic.main.activity_message_converstion.*
import org.kodein.di.generic.instance
import java.util.ArrayList

class MessageConverstionActivity : BaseActivity() {

    var recyclerViewMessage: RecyclerView? = null
    var messageAdapter: ConversationAdapter? = null
    var messageModelArrayList: ArrayList<ConversationModel>? = null


    private val conversationViewModelFactory : ConversationViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: ConversationViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_message_converstion, frameLayout)

        viewModel = ViewModelProvider(this, conversationViewModelFactory).get(ConversationViewModel::class.java)
        initViews()
        viewModel.id = intent?.extras?.getString("id")?:""
        btnSend.setOnClickListener {
            viewModel.message = edtWriteMessage.text.toString()
            viewModel.sendNewMessage()
        }

        viewModel.initializeLiveDataForConversationSentResponse().observe(this,
                { result ->
                    if(result!=null)
                    {
                        edtWriteMessage.setText("")
                        viewModel.getConversationList()
                        viewModel.emptyLiveDataForConversationSentResponse()
                    }
                })

        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForConversationResponse().observe(this,
                { result ->
                    if(result!=null)
                    {
                        messageAdapter?.setData(result.messages)
                        viewModel.emptyLiveDataForConversationResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this as AppCompatActivity,error)
                viewModel.emptyLiveDataError()
            }
        })

        viewModel.getConversationList()
    }

    private fun initViews() {
        recyclerViewMessage = findViewById(R.id.recyclerMessage)
        messageModelArrayList = ArrayList()

        messageAdapter = ConversationAdapter(this, messageModelArrayList)
        recyclerViewMessage?.setLayoutManager(LinearLayoutManager(this))
        recyclerViewMessage?.setAdapter(messageAdapter)
    }
}