package com.tulib.alfaezounschool.activity.ui.message.message_view

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IConversationRepository


@Suppress("UNCHECKED_CAST")
class MessageViewModelFactory(private var conversationRepository: IConversationRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return MessageViewModel(conversationRepository) as T
    }
}