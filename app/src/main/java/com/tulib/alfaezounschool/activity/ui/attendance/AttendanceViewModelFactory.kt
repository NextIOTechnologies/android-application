package com.tulib.alfaezounschool.activity.ui.attendance

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IAttendanceRepository


@Suppress("UNCHECKED_CAST")
class AttendanceViewModelFactory(private var attendanceRepository: IAttendanceRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return AttendanceViewModel(attendanceRepository) as T
    }
}