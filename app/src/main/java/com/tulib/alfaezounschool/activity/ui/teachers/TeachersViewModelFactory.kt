package com.tulib.alfaezounschool.activity.ui.teachers

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.ITeachersRepository


@Suppress("UNCHECKED_CAST")
class TeachersViewModelFactory(private var teachersRepository: ITeachersRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return TeachersViewModel(teachersRepository) as T
    }
}