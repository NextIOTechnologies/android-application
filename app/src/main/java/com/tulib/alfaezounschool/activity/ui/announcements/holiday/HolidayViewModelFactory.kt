package com.tulib.alfaezounschool.activity.ui.announcements.holiday

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IHolidayRepository

@Suppress("UNCHECKED_CAST")
class HolidayViewModelFactory(private var holidayRepository: IHolidayRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return HolidayViewModel(holidayRepository) as T
    }
}