package com.tulib.alfaezounschool.activity.ui.studentactivity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IStudentActivityRepository


@Suppress("UNCHECKED_CAST")
class StudentActivityViewModelFactory(private var studentActivityRepository: IStudentActivityRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return StudentActivityViewModel(studentActivityRepository) as T
    }
}