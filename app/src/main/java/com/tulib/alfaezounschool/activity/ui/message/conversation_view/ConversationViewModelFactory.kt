package com.tulib.alfaezounschool.activity.ui.message.conversation_view

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IConversationRepository

@Suppress("UNCHECKED_CAST")
class ConversationViewModelFactory(private var conversationRepository: IConversationRepository)
    : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return ConversationViewModel(conversationRepository) as T
    }
}