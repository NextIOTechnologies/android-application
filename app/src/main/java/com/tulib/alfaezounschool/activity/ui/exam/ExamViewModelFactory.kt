package com.tulib.alfaezounschool.activity.ui.exam

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IExamRepository


@Suppress("UNCHECKED_CAST")
class ExamViewModelFactory(private var examRepository: IExamRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return ExamViewModel(examRepository) as T
    }
}