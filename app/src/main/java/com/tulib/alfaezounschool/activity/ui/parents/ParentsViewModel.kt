package com.tulib.alfaezounschool.activity.ui.parents

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IParentsRepository

import kotlinx.coroutines.launch

class ParentsViewModel(private var parentsRepository: IParentsRepository) : ViewModel()
{
    private lateinit var liveDataForParentResponse : MutableLiveData<ParentsList>

    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>


    val studentID: String=""
    val name: String=""
    val className: String=""
    val roll: String=""
    val phone: String=""
    val photo: String=""

    fun initializeliveDataForParentResponse(): LiveData<ParentsList> {
        liveDataForParentResponse = MutableLiveData()
        return liveDataForParentResponse
    }
    fun emptyliveDataForParentResponse(){liveDataForParentResponse.value= null}



    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}


    fun getParents() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = parentsRepository.getParents()
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<ParentsList>
                        liveDataForParentResponse.value = data.data

                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
