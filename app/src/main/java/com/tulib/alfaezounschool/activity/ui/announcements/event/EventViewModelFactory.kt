package com.tulib.alfaezounschool.activity.ui.announcements.event

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IEventRepository


@Suppress("UNCHECKED_CAST")
class EventViewModelFactory(private var evenetRepository: IEventRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return EventViewModel(evenetRepository) as T
    }
}