package com.tulib.alfaezounschool.activity.ui.complain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IComplainRepository
import com.tulib.alfaezounschool.utils.DataUtil
import kotlinx.coroutines.launch

class ComplainViewModel(private var complainRepository: IComplainRepository) : ViewModel()
{
    private lateinit var liveDataForComplainResponse : MutableLiveData<ComplainList>
    private lateinit var liveDataForComplainUserResponse : MutableLiveData<UserList>
    private lateinit var liveDataForComplainSentResponse : MutableLiveData<Boolean>

    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

     var description :String =""
     var title :String =""
     var userTypeId :String =""
     var userId :String =""


    fun initializeLiveDataForComplainUserResponse(): LiveData<UserList> {
        liveDataForComplainUserResponse = MutableLiveData()
        return liveDataForComplainUserResponse
    }
    fun emptyLiveDataForComplainUserResponse(){liveDataForComplainUserResponse.value= null}

    fun initializeLiveDataForComplainResponse(): LiveData<ComplainList> {
        liveDataForComplainResponse = MutableLiveData()
        return liveDataForComplainResponse
    }
    fun emptyLiveDataForComplainResponse(){liveDataForComplainResponse.value= null}

    fun initializeLiveDataForComplainSentResponse(): LiveData<Boolean> {
        liveDataForComplainSentResponse = MutableLiveData()
        return liveDataForComplainSentResponse
    }
    fun emptyLiveDataForComplainSentResponse(){liveDataForComplainSentResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getComplains() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = complainRepository.complains()
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<ComplainList>
                    if(data.data.complains.size==0)
                    {
                        liveDataError.value = "No Complains Available"
                    }
                     liveDataForComplainResponse.value = data.data
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }

    fun getComplainUsers() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = complainRepository.getComplainUsers(userTypeId)
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<UserList>
                        liveDataForComplainUserResponse.value = data.data
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }


    fun sendComplain() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = complainRepository.sendComplain(DataUtil.getComplainJson(description,title,userTypeId,userId))
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    liveDataForComplainSentResponse.value=true
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
