package com.tulib.alfaezounschool.activity.ui.dashboard


import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.squareup.picasso.Picasso
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.db.database.AppDatabase
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IAttendanceRepository
import com.tulib.alfaezounschool.data.repository.ICommonRepository
import com.tulib.alfaezounschool.data.repository.INoticeRepository
import com.tulib.alfaezounschool.utils.UserType
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class DashboardViewModel(private var noticeRepository: INoticeRepository,private var commonRepository: ICommonRepository,
                         private var attendanceRepository: IAttendanceRepository,
                         private var appDatabase: AppDatabase) : ViewModel()
{
    private lateinit var liveDataForAttendanceResponse : MutableLiveData<AttendanceList>
    private lateinit var liveDataForNoticeResponse : MutableLiveData<ArrayList<NoticeModel>>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataForCalender : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>
    var liveDataUserName: MutableLiveData<String> = MutableLiveData()
    var liveDataUserClass: MutableLiveData<String> = MutableLiveData()
    var liveDataUsersStudentCount: MutableLiveData<String> = MutableLiveData()
    var liveDataUsersParentCount: MutableLiveData<String> = MutableLiveData()
    var liveDataUsersTeacherCount: MutableLiveData<String> = MutableLiveData()


    init {
        viewModelScope.launch {
            if(!AppController.fetchCommonData)
            { getCommonData() }
            val user = appDatabase.getUserDao().getUser()
            setUserType(user?.usertypeID?.toInt()?:0)
            liveDataForCalender.value= AppController.userType==UserType.Student

            if(AppController.userType==UserType.Student) {
                liveDataUserName.value = user?.name
            }else{
                liveDataUserName.value = "Select Student"
            }


            liveDataUserClass.value = user?.userClass?:""

        }
    }

    fun getAttendanceList(id:String) {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = attendanceRepository.attendanceList(id)
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<AttendanceList>
                    liveDataForAttendanceResponse.value = data.data
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }

    fun getCommonData() {
        viewModelScope.launch {
            val authResponse = commonRepository.commonData()
            when (authResponse) {
                is Resource.Success<*> -> {
                    Log.d("TAG", "getCommonDataxxxxx: ")
//                    AppController.fetchCommonData=true
                    val data = authResponse.value as MainResponse<CommonResponse>
                    val dao = appDatabase.getUserDao()
                    val token = dao.getUser()?.token
                    val user = data.data.profile
                    val userscount = data.data.userscount
                    user.token = token!!
                    dao.editUser(user)
                    if (AppController.userType == UserType.Student) {
                        liveDataUserName.value = appDatabase.getUserDao().getUser()?.name
                        AppController.studentid = appDatabase.getUserDao().getUser()?.loginuserID

                    } else {
                        liveDataUserName.value = "Select Student"
                    }
                    liveDataUserClass.value = appDatabase.getUserDao().getUser()?.userClass ?: ""
                    liveDataUsersStudentCount.value = userscount.students
                    liveDataUsersParentCount.value = userscount.parents
                    liveDataUsersTeacherCount.value = userscount.teachers

                }
            }
        }
    }

    fun getImageUrl(): String? {

        var url =""
        runBlocking {
            url =  appDatabase.getUserDao().getUser()?.photo?:""
        }
        // The URL will usually come from a model (i.e Profile)
        return url
    }


    fun initializeLiveDataForAttendanceResponse(): LiveData<AttendanceList> {
        liveDataForAttendanceResponse = MutableLiveData()
        return liveDataForAttendanceResponse
    }
    fun emptyLiveDataForAttendanceResponse(){liveDataForAttendanceResponse.value= null}


    fun initializeLiveDataForCalender(): LiveData<Boolean> {
        liveDataForCalender = MutableLiveData()
        return liveDataForCalender
    }
    fun emptyLiveDataForCalender(){liveDataForCalender.value= null}


    fun initializeLiveDataForNoticeResponse(): LiveData<ArrayList<NoticeModel>> {
        liveDataForNoticeResponse = MutableLiveData()
        return liveDataForNoticeResponse
    }
    fun emptyLiveDataForNoticeResponse(){liveDataForNoticeResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getNoticeList() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = noticeRepository.notices()
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<NoticeList>
                    if (data.data.noticeList.size == 0) {
                        //liveDataError.value = "No Notices Available"
                    } else {
                        liveDataForNoticeResponse.value = data.data.noticeList
                    }
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }

    fun setUserType(type:Int)
    {
        AppController.userType = when(type)
        {
            3-> UserType.Student
            4-> UserType.Parent
            2-> UserType.Teacher
            1-> UserType.Admin

            else-> null
        }
    }
}
@BindingAdapter("bind:imageUrl")
fun loadImage(view: ImageView, imageUrl: String?) {
    Picasso.get()
            .load(imageUrl)
            .placeholder(R.drawable.pic)
            .into(view)
}
