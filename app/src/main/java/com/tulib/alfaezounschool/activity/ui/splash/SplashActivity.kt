package com.tulib.alfaezounschool.activity.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.MainActivity
import com.tulib.alfaezounschool.activity.ui.login.LoginActivity
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.utils.Common
import com.tulib.alfaezounschool.utils.LocaleHelper
import kotlinx.coroutines.*
import nye.health.data.network.api_call.session.SessionManager
import org.kodein.di.generic.instance

class SplashActivity : AppCompatActivity() {

    private val sessionManager : SessionManager by AppController.kodein().instance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Common.FullScreen(this@SplashActivity)
        StartHandler()
    }

    private fun StartHandler() {
        CoroutineScope (Dispatchers.Main ).launch {
            delay(3000)

            startActivity(Intent(this@SplashActivity, if(sessionManager.isLogin()) MainActivity::class.java else  LoginActivity::class.java))
            finish()
        }
    }

    override fun attachBaseContext(base: android.content.Context?){
        super.attachBaseContext(base?.let { LocaleHelper.onAttach(it) })
    }
}