package com.tulib.alfaezounschool.activity.ui.forgot_password.forgot_password_email

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IForgotPasswordRepository


@Suppress("UNCHECKED_CAST")
class ForgotPasswordViewModelFactory(var forgotPasswordRepository: IForgotPasswordRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return ForgotPasswordEmailViewModel(forgotPasswordRepository) as T
    }
}