package com.tulib.alfaezounschool.activity.ui.base_activity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.db.database.AppDatabase

@Suppress("UNCHECKED_CAST")
class BaseActivityViewModelFactory(var appDatabase: AppDatabase) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return BaseActivityViewModel(appDatabase) as T
    }
}