package com.tulib.alfaezounschool.activity.ui.timetable

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.ITimeTableRepository
import kotlinx.coroutines.launch

class TimeTableViewModel(private var timeTableRepository: ITimeTableRepository) : ViewModel()
{
    private lateinit var liveDataForTimeTableResponse : MutableLiveData<TimeTableList>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

    fun initializeLiveDataForTimeTableResponse(): LiveData<TimeTableList> {
        liveDataForTimeTableResponse = MutableLiveData()
        return liveDataForTimeTableResponse
    }
    fun emptyLiveDataForTimeTableResponse(){liveDataForTimeTableResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getTimeTableList(id:String) {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = timeTableRepository.timetableList(id)
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<TimeTableList>
                    liveDataForTimeTableResponse.value = data.data
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
