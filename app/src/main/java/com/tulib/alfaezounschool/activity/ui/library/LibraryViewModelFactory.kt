package com.tulib.alfaezounschool.activity.ui.library

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.ILibraryRepository


@Suppress("UNCHECKED_CAST")
class LibraryViewModelFactory(private var librartRepository: ILibraryRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return LibraryViewModel(librartRepository) as T
    }
}