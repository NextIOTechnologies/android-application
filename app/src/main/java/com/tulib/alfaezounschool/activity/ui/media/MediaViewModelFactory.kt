package com.tulib.alfaezounschool.activity.ui.media

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IMediaRepository


@Suppress("UNCHECKED_CAST")
class MediaViewModelFactory(private var mediaRepository: IMediaRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return MediaViewModel(mediaRepository) as T
    }
}