package com.tulib.alfaezounschool.activity.ui.exam

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.adapter.ExamScheduleAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.ExamScheduleModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Use the [ExamFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ExamScheduleFragment : Fragment() {

    private val examViewModelFactory : ExamViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: ExamViewModel

    var recyclerViewExamSchedule: RecyclerView? = null
    var examScheduleAdapter: ExamScheduleAdapter? = null
    var examScheduleModelArrayList: ArrayList<ExamScheduleModel>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_exam_schedule, container, false)
        viewModel = ViewModelProvider(this, examViewModelFactory).get(ExamViewModel::class.java)
        initViews(v)
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // Redirecting to next destination


        viewModel.initializeLiveDataForLoader().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(requireActivity() as AppCompatActivity) else loaderDialog.hideLoader(requireActivity() as AppCompatActivity)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForExamResponse().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        examScheduleAdapter?.setData(result)
                        viewModel.emptyLiveDataForExamResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error)
                viewModel.emptyLiveDataError()
            }
        })
        AppController.studentid?.let {
            viewModel.getExamList(it
            )
        }
    }
    private fun initViews(v: View)
    {
        recyclerViewExamSchedule = v.findViewById(R.id.recyclerExamSchedule)
        examScheduleModelArrayList = ArrayList()
        examScheduleAdapter = ExamScheduleAdapter(activity as Context, examScheduleModelArrayList)
        recyclerViewExamSchedule?.setLayoutManager(LinearLayoutManager(activity))
        recyclerViewExamSchedule?.setAdapter(examScheduleAdapter)
    }


}