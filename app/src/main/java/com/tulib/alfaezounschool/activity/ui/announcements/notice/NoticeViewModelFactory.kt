package com.tulib.alfaezounschool.activity.ui.announcements.notice

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.INoticeRepository


@Suppress("UNCHECKED_CAST")
class NoticeViewModelFactory(private var noticeRepository: INoticeRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return NoticeViewModel(noticeRepository) as T
    }
}