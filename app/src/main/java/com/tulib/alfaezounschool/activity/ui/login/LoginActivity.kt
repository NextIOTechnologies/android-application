package com.tulib.alfaezounschool.activity.ui.login

import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.MainActivity
import com.tulib.alfaezounschool.activity.ui.forgot_password.forgot_password_email.ForgotPasswordActivity
import com.tulib.alfaezounschool.activity.ui.splash.SplashActivity
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.ResponseMessages
import com.tulib.alfaezounschool.databinding.ActivityLoginBinding
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import com.tulib.alfaezounschool.utils.Common
import org.kodein.di.generic.instance
import java.util.*
import com.tulib.alfaezounschool.utils.LocaleHelper


class LoginActivity : AppCompatActivity() {

    private val loginViewModelFactory: LoginViewModelFactory by AppController.kodein().instance()
    private val loaderDialog: LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: LoginViewModel
    private var device_token:String = "";
    private var btnLogin: Button? = null
    var txtClickHere: TextView? = null
    var linearSelectLanguage: LinearLayout? = null
    var dialog: Dialog? = null
    var imgLangImage: ImageView? = null
    var tviLangName: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppController.userType=null
        AppController.studentid =""

        //   var  binding :LoginActivityBinding =   DataBindingUtil.setContentView(this, R.layout.activity_login);
        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        viewModel = ViewModelProvider(this, loginViewModelFactory).get(LoginViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this

        Common.FullScreen(this@LoginActivity)
        initViews()
        initListeners()


        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(ContentValues.TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            device_token = task.result.toString()
            Log.d(ContentValues.TAG, "Refreshed token: $device_token")
            // Log and toast
        })

        // Redirecting to next destination
        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if (result != null) {
                        if (result) loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForLoginResponse().observe(this,
                { result ->
                    if (result != null) {
                        Common.startActivity(this@LoginActivity, MainActivity::class.java)
                        finish()
                        viewModel.emptyLiveDataForLoginResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this, error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this, error)
                viewModel.emptyLiveDataError()
            }
        })
    }


    private fun initViews() {
        btnLogin = findViewById(R.id.btn_login)
        txtClickHere = findViewById(R.id.txtClickHere)
        linearSelectLanguage = findViewById<LinearLayout>(R.id.linearSelectLanguage)
        dialog = Dialog(this)


    }

    private fun initListeners() {
        btnLogin!!.setOnClickListener {

            if (viewModel.validateLoginData())
                viewModel.login(device_token)
            else
                AppUtils.showSnackBar(this, ResponseMessages.SOME_THING_WENT_WRONG.message)
        }
        txtClickHere!!.setOnClickListener { Common.startActivity(this@LoginActivity, ForgotPasswordActivity::class.java) }
        linearSelectLanguage?.setOnClickListener(View.OnClickListener { DialogSelectLanguage() })

        tviLangName
        tviLangName = findViewById(R.id.tviLangName)

        imgLangImage = findViewById<ImageView>(R.id.imgLangImage)
        // imgMenu = findViewById(R.id.imgSettingMenu);
        val select_lang: String? = LocaleHelper.getLanguage(getApplicationContext())
        if (select_lang == "ar") {
            imgLangImage!!.setImageResource(R.drawable.arabic)
            tviLangName!!.setText(getString(R.string.arabic))
        } else if (select_lang == "tr") {
            imgLangImage!!.setImageResource(R.drawable.turkish)
            tviLangName!!.setText(getString(R.string.turkish))

        } else {
            imgLangImage!!.setImageResource(R.drawable.english)
            tviLangName!!.setText(getString(R.string.english))

        }

        val myLocale =  Locale(select_lang);
        val res:Resources = getResources();
        val dm:DisplayMetrics = res.getDisplayMetrics();
        val conf:Configuration = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        onConfigurationChanged(conf);
    }

    private fun DialogSelectLanguage() {
        dialog?.setContentView(R.layout.dialog_select_language)
        val btnOk = dialog!!.findViewById<Button>(R.id.btnDialogLanguageOk)
        val imgClose = dialog!!.findViewById<ImageView>(R.id.imgDialogLanguageClose)
        val imgArabicCheck = dialog!!.findViewById<ImageView>(R.id.imgArabicCheck)
        val imgEnglishCheck = dialog!!.findViewById<ImageView>(R.id.imgEnglishCheck)
        val imgTurkishCheck = dialog!!.findViewById<ImageView>(R.id.imgTurkishCheck)
        var select_lang: String? = LocaleHelper.getLanguage(getApplicationContext())
        if (select_lang == "ar") {
            imgLangImage!!.setImageResource(R.drawable.arabic)
            imgArabicCheck.visibility = View.VISIBLE
            imgEnglishCheck.visibility = View.INVISIBLE
            imgTurkishCheck.visibility = View.INVISIBLE
        } else if (select_lang == "tr") {
            imgLangImage!!.setImageResource(R.drawable.turkish)
            imgArabicCheck.visibility = View.INVISIBLE
            imgEnglishCheck.visibility = View.INVISIBLE
            imgTurkishCheck.visibility = View.VISIBLE
        } else {
            imgLangImage!!.setImageResource(R.drawable.english)
            imgArabicCheck.visibility = View.INVISIBLE
            imgEnglishCheck.visibility = View.VISIBLE
            imgTurkishCheck.visibility = View.INVISIBLE
        }
        val linlLanguageArabic: LinearLayout? = dialog?.findViewById<LinearLayout>(R.id.linlLanguageArabic)
        val linlLanguageEnglish: LinearLayout = dialog!!.findViewById<LinearLayout>(R.id.linlLanguageEnglish)
        val linlLanguageTurkish: LinearLayout = dialog!!.findViewById<LinearLayout>(R.id.linlLanguageTurkish)



        linlLanguageArabic?.setOnClickListener(View.OnClickListener {
            select_lang="ar"
            imgArabicCheck.visibility = View.VISIBLE
            imgEnglishCheck.visibility = View.INVISIBLE
            imgTurkishCheck.visibility = View.INVISIBLE
        })
        linlLanguageEnglish.setOnClickListener(View.OnClickListener {
            select_lang="en"
            imgArabicCheck.visibility = View.INVISIBLE
            imgEnglishCheck.visibility = View.VISIBLE
            imgTurkishCheck.visibility = View.INVISIBLE
        })
        linlLanguageTurkish.setOnClickListener(View.OnClickListener {
            select_lang="tr"
            imgArabicCheck.visibility = View.INVISIBLE
            imgEnglishCheck.visibility = View.INVISIBLE
            imgTurkishCheck.visibility = View.VISIBLE
        })

        btnOk.setOnClickListener {
            LocaleHelper.setLocale(getApplicationContext(), select_lang, select_lang)
            recreate() //now restart.
            sendBroadcast(Intent("Language.changed"))
//            val intent = Intent(getApplicationContext(), SplashActivity::class.java)
//            finish()
//            finishAffinity()
//            startActivity(intent)

//            val myLocale =  Locale(select_lang);
//            val res:Resources = getResources();
//            val dm:DisplayMetrics = res.getDisplayMetrics();
//            val conf:Configuration = res.getConfiguration();
//            conf.locale = myLocale;
//            res.updateConfiguration(conf, dm);
//            onConfigurationChanged(conf);
            val refresh:Intent =  Intent(this, LoginActivity::class.java)
            startActivity(refresh)
            finish();
            val config = resources.configuration
            val locale = Locale(select_lang)
            Locale.setDefault(locale)
            config.locale = locale

            resources.updateConfiguration(config, resources.displayMetrics)


//            recreate() //now restart.

            dialog!!.dismiss()
        }
        imgClose.setOnClickListener { dialog!!.dismiss() }
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.show()
    }


//
//    override fun attachBaseContext(base: android.content.Context?) {
//        super.attachBaseContext(base?.let { LocaleHelper.onAttach(it) })
//    }


    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase))

    }
    @Override
    override fun onConfigurationChanged( newConfig: Configuration) {
        // refresh your views here
        super.onConfigurationChanged(newConfig);
// Checks the active language
        LocaleHelper.onAttach(this);
        super.onConfigurationChanged(newConfig);

    }

}