package com.tulib.alfaezounschool.activity.ui.media

import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView
import android.widget.GridView
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.PreviewImage
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.adapter.MediaGridAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.MediaFilesModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import com.tulib.alfaezounschool.utils.Common
import kotlinx.android.synthetic.main.activity_notification_openner.*
import org.kodein.di.generic.instance
import java.util.*

class Media : BaseActivity() {
    private val mediaViewModelFactory: MediaViewModelFactory by AppController.kodein().instance()
    private val loaderDialog: LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: MediaViewModel


    var gridViewMedia: GridView? = null
    var mediaAdapter: MediaGridAdapter? = null
    var mediaModelArrayList: ArrayList<MediaFilesModel>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_media, frameLayout)
        initViews()
        initListeners()
        viewModel = ViewModelProvider(this, mediaViewModelFactory).get(MediaViewModel::class.java)

        loadData("0")

    }


    private fun loadData(id: String) {

        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if (result != null) {
                        if (result) loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeliveDataForParentResponse().observe(this,
                { result ->
                    if (result != null) {
                        mediaAdapter?.setData(result.filesList)
                        viewModel.emptyliveDataForParentResponse()
                        mediaModelArrayList?.addAll(result.filesList)
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this, error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this, error)
                viewModel.emptyLiveDataError()
            }
        })
        viewModel.getMedia(id)
        initViews()
    }

    private fun initViews() {
        gridViewMedia = findViewById(R.id.gridMedia)
        mediaModelArrayList = ArrayList()

        mediaAdapter = MediaGridAdapter(this@Media, mediaModelArrayList)
//        gridViewMedia?.setLayoutManager(LinearLayoutManager(this))

        gridViewMedia?.setAdapter(mediaAdapter)



        gridViewMedia!!.onItemClickListener = AdapterView.OnItemClickListener { parent: AdapterView<*>, _, pos, _ ->
            if (mediaModelArrayList!!.get(pos).type.equals("file")) {

                val intent = Intent(this@Media, PreviewImage::class.java)
                intent.putExtra("imgUrl",mediaModelArrayList!!.get(pos).fileUrl)
                startActivity(intent)
            } else {
                val intent = Intent(this@Media, FoldersOpenActivity::class.java)
                intent.putExtra("folderID",mediaModelArrayList!!.get(pos).id)
                intent.putExtra("folderName",mediaModelArrayList!!.get(pos).fileNameDisplay)
                startActivity(intent)
//                loadData(mediaModelArrayList!!.get(pos).id)
            }
        }

    }

    private fun initListeners() {
        imgMenu!!.setOnClickListener { drawerLayout!!.openDrawer(GravityCompat.START) }
    }


}