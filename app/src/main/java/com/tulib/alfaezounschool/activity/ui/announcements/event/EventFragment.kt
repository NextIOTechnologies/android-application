package com.tulib.alfaezounschool.activity.ui.announcements.event

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.adapter.EventAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.EventModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance

class EventFragment : Fragment() {

    private val eventViewModelFactory : EventViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: EventViewModel

    var recyclerViewEvent: RecyclerView? = null
    var eventAdapter: EventAdapter? = null
    lateinit  var   eventArrayList : ArrayList<EventModel> ;


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_event, container, false)
        viewModel = ViewModelProvider(this, eventViewModelFactory).get(EventViewModel::class.java)
        initViews(view)
        return view
    }

    private fun initViews(v: View) {
        recyclerViewEvent = v.findViewById(R.id.recyclerEvent)
        eventArrayList =  ArrayList();
        eventAdapter =  EventAdapter(requireActivity(),eventArrayList);
        recyclerViewEvent?.setLayoutManager(LinearLayoutManager(activity))
        recyclerViewEvent?.setAdapter(eventAdapter)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.initializeLiveDataForLoader().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                     //   if(result)loaderDialog.showLoader(requireActivity() as AppCompatActivity) else loaderDialog.hideLoader(requireActivity() as AppCompatActivity)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForEventResponse().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        eventAdapter?.setData(result)
                        viewModel.emptyLiveDataForEventResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error)
                viewModel.emptyLiveDataError()
            }
        })

        viewModel.getEventList()
    }
}