package com.tulib.alfaezounschool.activity.ui.students

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.IStudentsRepository


@Suppress("UNCHECKED_CAST")
class StudentsViewModelFactory(private var studentsRepository: IStudentsRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return StudentsViewModel(studentsRepository) as T
    }
}