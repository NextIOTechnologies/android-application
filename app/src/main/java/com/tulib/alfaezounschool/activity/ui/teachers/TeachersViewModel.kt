package com.tulib.alfaezounschool.activity.ui.teachers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.ITeachersRepository

import kotlinx.coroutines.launch

class TeachersViewModel(private var teachersRepository: ITeachersRepository) : ViewModel()
{
    private lateinit var liveDataForTeacherResponse : MutableLiveData<TeachersList>

    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>


    val studentID: String=""
    val name: String=""
    val className: String=""
    val roll: String=""
    val phone: String=""
    val photo: String=""

    fun initializeliveDataForTeacherResponse(): LiveData<TeachersList> {
        liveDataForTeacherResponse = MutableLiveData()
        return liveDataForTeacherResponse
    }
    fun emptyliveDataForTeacherResponse(){liveDataForTeacherResponse.value= null}



    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}


    fun getTeachers() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = teachersRepository.getTeachers()
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<TeachersList>
                        liveDataForTeacherResponse.value = data.data

                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
