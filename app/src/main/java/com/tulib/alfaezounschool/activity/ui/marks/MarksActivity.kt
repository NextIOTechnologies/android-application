package com.tulib.alfaezounschool.activity.ui.marks

import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import android.os.Bundle
import android.widget.ExpandableListView
import androidx.databinding.DataBindingUtil
import com.tulib.alfaezounschool.R
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.adapter.ExpandableAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.MarksExamModel
import com.tulib.alfaezounschool.data.network.response.response_models.MarksModel
import com.tulib.alfaezounschool.databinding.ActivityMarksBinding
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance
import java.util.ArrayList

class MarksActivity :  BaseActivity() {

    private val marksViewModelFactory : MarksViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: MarksViewModel
    private lateinit var headerList : ArrayList<MarksExamModel>
    private lateinit var childList : LinkedHashMap<String,List<MarksModel>>

    lateinit var expandableListAdapter:ExpandableAdapter
    lateinit var expandableListView:ExpandableListView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //   var  binding :LoginActivityBinding =   DataBindingUtil.setContentView(this, R.layout.activity_login);
        val binding : ActivityMarksBinding = DataBindingUtil.inflate(layoutInflater, R.layout.activity_marks,frameLayout,true);
        viewModel = ViewModelProvider(this, marksViewModelFactory).get(MarksViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this

        initViews()

        // Redirecting to next destination
        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForMarksResponse().observe(this,
                { result ->
                    if(result!=null)
                    {
                        if(result.marksList!=null && result.marksList.size>0)
                        {
                            headerList = ArrayList(result.marksList.size)
                            childList =  LinkedHashMap()
                            for(i in 0..result.marksList.size-1)
                            {
                                val exam = result.marksList.get(i)
                                val key = result.marksList.get(i).exam
                                headerList.add(exam)

                                childList.put(key,result.marksList.get(i).marks)
                            }
                           expandableListAdapter.setData(headerList,childList)
                        }

                        viewModel.emptyLiveDataForMarksResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error)
                viewModel.emptyLiveDataError()
            }
        })
        AppController.studentid?.let {
            viewModel.marks(it
            )
        }
    }

    private fun initViews() {
        expandableListView = findViewById(R.id.expListView)
        expandableListAdapter = ExpandableAdapter(this,ArrayList(),LinkedHashMap())
        expandableListView.setAdapter(expandableListAdapter)
    }
}