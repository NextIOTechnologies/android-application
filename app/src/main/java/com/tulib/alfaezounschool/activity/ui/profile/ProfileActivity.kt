package com.tulib.alfaezounschool.activity.ui.profile

import android.os.Bundle
import android.widget.ExpandableListView
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import com.squareup.picasso.Picasso
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.adapter.ProfileExpandableAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import com.tulib.alfaezounschool.utils.SharedPreference
import de.hdodenhof.circleimageview.CircleImageView
import org.kodein.di.generic.instance
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.LinkedHashMap


class ProfileActivity : BaseActivity() {

    private val profileViewModelFactory: ProfileViewModelFactory by AppController.kodein().instance()
    private val loaderDialog: LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: ProfileViewModel
    private lateinit var headerList: ArrayList<String>
    private lateinit var childList: LinkedHashMap<String, HashMap<String, String>>

    lateinit var expandableListAdapter: ProfileExpandableAdapter
    lateinit var expandableListView: ExpandableListView

    var imgMenuStudentImage: CircleImageView? = null

    //    Top Student Selection
//    private lateinit var studentsModel: StudentsViewModel
//    private val studentsViewModelFactory: StudentsViewModelFactory by AppController.kodein().instance()
//
//    var spinnerStudentList: Spinner? = null
//    var studentListArrayList: ArrayList<StudentModel>? = null
//
//    var studentmAdapter: SpinnerStudentListAdapter? = null

    var sharedPreference: SharedPreference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_profile, frameLayout)
        viewModel = ViewModelProvider(this, profileViewModelFactory).get(ProfileViewModel::class.java)

        sharedPreference = SharedPreference(this)

        initViews()
        initListeners()
//        initstudentModel()


        loadData();

    }


    private fun loadData() {
        // Redirecting to next destination
        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if (result != null) {
                        if (result) loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForStudentDetailsResponse().observe(this,
                { result ->
                    if (result != null) {
                        if (result != null) {
                            headerList = ArrayList(4)
                            childList = LinkedHashMap()

                            if (result.student_info != null) {
                                val studentList = LinkedHashMap<String, String>()
                                studentList.put("First Name", result.student_info.firstName)
                                studentList.put("Middle Name", result.student_info.middleName)
                                studentList.put("Last Name", result.student_info.lastName)
                                studentList.put("Surname", result.student_info.surname)
                                studentList.put("Roll No", result.student_info.roll)
                                studentList.put("Addmission", result.student_info.admission)
                                studentList.put("Education System", result.student_info.educationSystem)
                                studentList.put("Class", result.student_info.classX)
                                studentList.put("Section", result.student_info.section)
                                headerList.add("Student Information")
                                childList.put("Student Information", studentList)


                                Picasso.get()
                                        .load(result.student_info.photo)
                                        .placeholder(R.drawable.pic)
                                        .into(imgMenuStudentImage)
                            }

                            if (result.personal_info != null) {
                                val personalList = LinkedHashMap<String, String>()
                                personalList.put("DOB", result.personal_info.dob ?: "")
                                personalList.put("Email", result.personal_info.email)
                                personalList.put("Citizenship No", result.personal_info.citizenshipNO)
                                personalList.put("Gender", result.personal_info.gender)
                                personalList.put("His Her Order", result.personal_info.hisHerOrder)
                                personalList.put("Live With Parents", result.personal_info.liveWithParents)
                                personalList.put("No Of Siblings Brothers", result.personal_info.noOfSiblingsBrothers)
                                personalList.put("No Of Siblings Sisters", result.personal_info.noOfSiblingsSisters)
                                personalList.put("Parents Alive", result.personal_info.parentsAlive)
                                personalList.put("Passport No", result.personal_info.passportNO)
                                personalList.put("Phone No", result.personal_info.phone)
                                personalList.put("Scholarship By Name", result.personal_info.scholarshipByName)
                                headerList.add("Personal Details")
                                childList.put("Personal Details", personalList)
                            }

                            if (result.parents != null) {
                                val parentList = LinkedHashMap<String, String>()
                                parentList.put("Name", result.parents.name)
                                parentList.put("User Name", result.parents.username)
                                parentList.put("Father Name", result.parents.fatherName)
                                parentList.put("Mother Name", result.parents.motherName)
                                parentList.put("Address", result.parents.address)
                                parentList.put("Email", result.parents.email)
                                parentList.put("Father's Profession", result.parents.fatherProfession)
                                parentList.put("Mother's Profession", result.parents.motherProfession)
                                parentList.put("Phone No", result.parents.phone)
                                headerList.add("Guardian Details")
                                childList.put("Guardian Details", parentList)
                            }
                            if (result.documents != null) {
                                val documentList = LinkedHashMap<String, String>()
                                for (i in 0..result.documents.size - 1) {
                                    documentList.put("Title" + i, result.documents.get(i).title)
                                }


                                headerList.add("Documents")
                                childList.put("Documents", documentList)
                            }
                            expandableListAdapter.setData(headerList, childList)
                        }

                        viewModel.emptyLiveDataForStudentDetailsResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this, error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this, error)
                viewModel.emptyLiveDataError()
            }
        })

        val student_id:String = AppController.studentid!!


        AppController.studentid?.let {
            viewModel.studentDetails(it
            )
        }
//        Toast.makeText(this@ProfileActivity, student_id + " selected", Toast.LENGTH_SHORT).show()

    }


    private fun initViews() {
        expandableListView = findViewById(R.id.expListView)
        expandableListAdapter = ProfileExpandableAdapter(this, ArrayList(), LinkedHashMap())
        expandableListView.setAdapter(expandableListAdapter)
        imgMenuStudentImage = findViewById(R.id.imgMenuStudentImage)

    }


//    private fun initstudentModel() {
//        studentsModel = ViewModelProvider(this, studentsViewModelFactory).get(StudentsViewModel::class.java)
//        studentsModel.initializeLiveDataForLoader().observe(this,
//                { result ->
//                    if (result != null) {
////                        if(result)loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
//                        studentsModel.emptyLiveDataForLoader()
//                    }
//                })
//        studentsModel.initializeliveDataForStudentResponse().observe(this,
//                { result ->
//                    if (result != null) {
//                        studentmAdapter?.setData(result.studentsList)
//                        studentListArrayList = result.studentsList
//                        studentsModel.emptyliveDataForStudentResponse()
//                        initstudentViews()
//                    }
//                })
//        studentsModel.initializeLiveDataGenericError().observe(this, { error ->
//            error?.let {
//                AppUtils.showSnackBar(this, error.message)
//                studentsModel.emptyLiveDataGenericError()
//            }
//        })
//        // Showing error in bottom red popup
//        studentsModel.initializeLiveDataError().observe(this, { error ->
//            error?.let {
//                AppUtils.showSnackBar(this, error)
//                studentsModel.emptyLiveDataError()
//            }
//        })
//        studentsModel.getStudents()
//    }

//    private fun initstudentViews() {
//
////        Top Student Selection
//        spinnerStudentList = findViewById(R.id.spinner_studentlist)
////        studentListArrayList = ArrayList()
//        spinnerStudentList?.setAdapter(studentmAdapter)
//        spinnerStudentList?.setVisibility(View.INVISIBLE)
//        spinnerStudentList?.setVisibility(View.VISIBLE)
//        studentmAdapter = SpinnerStudentListAdapter(this, studentListArrayList!!)
//
//        if (studentListArrayList != null) {
////            studentListArrayList?.add(StudentModel("Select Group", "0", "", "", "", ""))
//
//            spinnerStudentList?.setAdapter(studentmAdapter)
//
//            if (AppController.studentidposition != null) {
//                spinnerStudentList?.setSelection(AppController.studentidposition!!)
//                SpinnerStudentListAdapter.textViewName?.setTextColor(Color.parseColor("#ffffff"))
//            }
//            Toast.makeText(this@ProfileActivity, "${AppController.studentidposition} selected", Toast.LENGTH_SHORT).show()
//            spinnerStudentList?.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
//                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//                    val clickedItem: StudentModel = parent.getItemAtPosition(position) as StudentModel
//                    SpinnerStudentListAdapter.textViewName?.setTextColor(Color.parseColor("#ffffff"))
//                    val clickedStudentName: String = clickedItem.name
//                    viewModel.studentDetails(clickedItem.studentID)
//                    viewModel.emptyLiveDataForStudentDetailsResponse()
//
//                    AppController.studentidposition = position
//
////                    Toast.makeText(this@ProfileActivity, "$clickedStudentName selected", Toast.LENGTH_SHORT).show()
//                }
//
//                override fun onNothingSelected(parent: AdapterView<*>?) {
//
//                }
//            })
//        }
//    }


    private fun initListeners() {
        imgMenu!!.setOnClickListener { drawerLayout!!.openDrawer(GravityCompat.START) }

    }
}
