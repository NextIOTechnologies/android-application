package com.tulib.alfaezounschool.activity.ui.announcements.event

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IEventRepository
import kotlinx.coroutines.launch

class EventViewModel(private var eventRepository: IEventRepository) : ViewModel()
{
    private lateinit var liveDataForEventResponse : MutableLiveData<ArrayList<EventModel>>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

    fun initializeLiveDataForEventResponse(): LiveData<ArrayList<EventModel>> {
        liveDataForEventResponse = MutableLiveData()
        return liveDataForEventResponse
    }
    fun emptyLiveDataForEventResponse(){liveDataForEventResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getEventList() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = eventRepository.events()
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<EventList>
                    if(data.data.eventList.size==0)
                    {
//                        liveDataError.value = "No Events Available"
                    }
                    else liveDataForEventResponse.value = data.data.eventList
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
