package com.tulib.alfaezounschool.activity

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.adapter.AccountAdapter
import com.tulib.alfaezounschool.model.InvoiceModel
import com.tulib.alfaezounschool.model.StudentListModel

import java.util.*

class AccountActivity : BaseActivity() {
    var recyclerViewAccount: RecyclerView? = null
    var accountAdapter: AccountAdapter? = null
    var invoiceModelArrayList: ArrayList<InvoiceModel>? = null
    var spinnerStudentList: Spinner? = null
    private var studentListModelArrayList: ArrayList<StudentListModel?>? = null
    private var mAdapter: SpinnerStudentListAdapterx? = null
    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getLayoutInflater().inflate(R.layout.activity_account, frameLayout)
        initViews()
    }

    private fun initViews() {
        recyclerViewAccount = findViewById<RecyclerView>(R.id.recyclerAccount)
        invoiceModelArrayList = ArrayList<InvoiceModel>()
        Data()
        accountAdapter = AccountAdapter(this@AccountActivity, invoiceModelArrayList!!)
        recyclerViewAccount?.setLayoutManager(LinearLayoutManager(this))
        recyclerViewAccount?.setAdapter(accountAdapter)
        StudentListData()
        spinnerStudentList = findViewById<Spinner>(R.id.spinner_studentlist)
        spinnerStudentList?.setVisibility(View.INVISIBLE)
        // if(DashboardFragment.count == 0) {
        spinnerStudentList?.setVisibility(View.VISIBLE)
        mAdapter = SpinnerStudentListAdapterx(this, studentListModelArrayList)
        spinnerStudentList?.setAdapter(mAdapter)
        spinnerStudentList?.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val clickedItem: StudentListModel = parent.getItemAtPosition(position) as StudentListModel
                SpinnerStudentListAdapterx.textViewName?.setTextColor(Color.parseColor("#ffffff"))
                val clickedCountryName: String = clickedItem.studentname
                Toast.makeText(this@AccountActivity, "$clickedCountryName selected", Toast.LENGTH_SHORT).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
    }

    private fun Data() {
        invoiceModelArrayList!!.add(InvoiceModel("Muhammad Zubair", "15 Nov,2020", "1,900", "0", "Fully Paid"))
        invoiceModelArrayList!!.add(InvoiceModel("Muhammad Zubair", "20 Oct,2020", "1,900", "1000", "Partial Paid"))
        invoiceModelArrayList!!.add(InvoiceModel("Muhammad Zubair", "27 Sep,2020", "1,900", "1,900", "Not Paid"))
    }

    private fun StudentListData() {
        studentListModelArrayList = ArrayList<StudentListModel?>()
        studentListModelArrayList!!.add(StudentListModel("Abdul Rehman Ali", "05-B", "",R.drawable.image))
        studentListModelArrayList!!.add(StudentListModel("Muhammad Zubair", "10-A", "",R.drawable.image))
    }
}

private class SpinnerStudentListAdapterx(context: Context?, countryList: ArrayList<StudentListModel?>?) :
        ArrayAdapter<StudentListModel?>(context!!, 0, countryList!!) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    @SuppressLint("ResourceAsColor")
    private fun initView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.row_spinner_studentlist, parent, false
            )
        }
        val imageView = convertView?.findViewById<ImageView>(R.id.imgSpinnerStudentImage)
        textViewName = convertView?.findViewById<TextView>(R.id.txtSpinnerStudent)
        val textViewClass: TextView? = convertView?.findViewById(R.id.txtSpinnerClass)
        val currentItem: StudentListModel? = getItem(position)
        if (currentItem != null) {
            imageView?.setImageResource(currentItem.studentlist_image)
            textViewName?.setText(currentItem.studentname)
            textViewClass?.setText(currentItem.studentlist_class)
        }
        return convertView!!
    }

    companion object {
        var textViewName: TextView? = null
    }
}