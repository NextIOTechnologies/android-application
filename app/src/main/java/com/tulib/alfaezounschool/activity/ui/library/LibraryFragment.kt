package com.tulib.alfaezounschool.activity.ui.library

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.adapter.LibraryAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.LibraryModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Use the [LibraryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LibraryFragment : Fragment() {

    private val libraryViewModelFactory : LibraryViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: LibraryViewModel

    var recyclerViewLibrary: RecyclerView? = null
    var libraryAdapter: LibraryAdapter? = null
    var libraryModelArrayList: ArrayList<LibraryModel>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_library, container, false)
        viewModel = ViewModelProvider(this, libraryViewModelFactory).get(LibraryViewModel::class.java)
        initViews(v)
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // Redirecting to next destination


        viewModel.initializeLiveDataForLoader().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(requireActivity() as AppCompatActivity) else loaderDialog.hideLoader(requireActivity() as AppCompatActivity)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForLibraryResponse().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        libraryAdapter?.setData(result)
                        viewModel.emptyLiveDataForLibraryResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error)
                viewModel.emptyLiveDataError()
            }
        })

        AppController.studentid?.let {
            viewModel.getLibraryList(it
            )
        }
    }
    private fun initViews(v: View)
    {
        recyclerViewLibrary = v.findViewById(R.id.recyclerLibrary)
        libraryModelArrayList = ArrayList()
        libraryAdapter = LibraryAdapter(activity as Context, libraryModelArrayList)
        recyclerViewLibrary?.setLayoutManager(LinearLayoutManager(activity))
        recyclerViewLibrary?.setAdapter(libraryAdapter)
    }


}