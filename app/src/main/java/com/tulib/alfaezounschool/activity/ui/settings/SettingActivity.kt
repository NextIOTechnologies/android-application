package com.tulib.alfaezounschool.activity.ui.settings

import android.app.Dialog
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.activity.ui.login.LoginActivity
import com.tulib.alfaezounschool.activity.ui.settings.change_password.ChangePasswordActivity
import com.tulib.alfaezounschool.activity.ui.splash.SplashActivity
import com.tulib.alfaezounschool.utils.Common
import com.tulib.alfaezounschool.utils.LocaleHelper
import java.util.*

class SettingActivity : BaseActivity() {
    var linearChangePassowrd: LinearLayout? = null
    var linearSelectLanguage: LinearLayout? = null
    var linearVisitWebsite: LinearLayout? = null
    var linearPrivacyPolicy: LinearLayout? = null

    var imgLangImage: ImageView? = null
    var dialog: Dialog? = null
    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getLayoutInflater().inflate(R.layout.activity_setting, frameLayout)
        initViews()
        initListeners()
    }

    private fun initViews() {
        dialog = Dialog(this)
        linearChangePassowrd = findViewById<LinearLayout>(R.id.linearChangePassword)
        linearSelectLanguage = findViewById<LinearLayout>(R.id.linearSelectLanguage)
        linearVisitWebsite = findViewById<LinearLayout>(R.id.linearVisitWebsite)
        linearPrivacyPolicy = findViewById<LinearLayout>(R.id.linearPrivacyPolicy)
        imgLangImage = findViewById<ImageView>(R.id.imgLangImage)
        // imgMenu = findViewById(R.id.imgSettingMenu);
        val select_lang: String? = LocaleHelper.getLanguage(getApplicationContext())
        if (select_lang == "ar") {
            imgLangImage!!.setImageResource(R.drawable.arabic)
        } else if (select_lang == "tr") {
            imgLangImage!!.setImageResource(R.drawable.turkish)
        } else {
            imgLangImage!!.setImageResource(R.drawable.english)
        }
    }

    private fun initListeners() {
        linearChangePassowrd?.setOnClickListener(View.OnClickListener { Common.startActivity(this@SettingActivity, ChangePasswordActivity::class.java) })
        linearSelectLanguage?.setOnClickListener(View.OnClickListener { DialogSelectLanguage() })
        linearVisitWebsite?.setOnClickListener(View.OnClickListener {
            val uri = Uri.parse("https://www.tulib.k12.tr/") // missing 'http://' will cause crashed
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        })


        linearPrivacyPolicy?.setOnClickListener(View.OnClickListener {
            val uri = Uri.parse("https://www.tulib.k12.tr/privacy-policy.html") // missing 'http://' will cause crashed
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        })
    }

    private fun DialogSelectLanguage() {
        dialog?.setContentView(R.layout.dialog_select_language)
        val btnOk = dialog!!.findViewById<Button>(R.id.btnDialogLanguageOk)
        val imgClose = dialog!!.findViewById<ImageView>(R.id.imgDialogLanguageClose)
        val imgArabicCheck = dialog!!.findViewById<ImageView>(R.id.imgArabicCheck)
        val imgEnglishCheck = dialog!!.findViewById<ImageView>(R.id.imgEnglishCheck)
        val imgTurkishCheck = dialog!!.findViewById<ImageView>(R.id.imgTurkishCheck)
        var select_lang: String? = LocaleHelper.getLanguage(getApplicationContext())
        if (select_lang == "ar") {
            imgLangImage!!.setImageResource(R.drawable.arabic)
            imgArabicCheck.visibility = View.VISIBLE
            imgEnglishCheck.visibility = View.INVISIBLE
            imgTurkishCheck.visibility = View.INVISIBLE
        } else if (select_lang == "tr") {
            imgLangImage!!.setImageResource(R.drawable.turkish)
            imgArabicCheck.visibility = View.INVISIBLE
            imgEnglishCheck.visibility = View.INVISIBLE
            imgTurkishCheck.visibility = View.VISIBLE
        } else {
            imgLangImage!!.setImageResource(R.drawable.english)
            imgArabicCheck.visibility = View.INVISIBLE
            imgEnglishCheck.visibility = View.VISIBLE
            imgTurkishCheck.visibility = View.INVISIBLE
        }
        val linlLanguageArabic: LinearLayout? = dialog?.findViewById<LinearLayout>(R.id.linlLanguageArabic)
        val linlLanguageEnglish: LinearLayout = dialog!!.findViewById<LinearLayout>(R.id.linlLanguageEnglish)
        val linlLanguageTurkish: LinearLayout = dialog!!.findViewById<LinearLayout>(R.id.linlLanguageTurkish)
        linlLanguageArabic?.setOnClickListener(View.OnClickListener {
            select_lang="ar"
            imgArabicCheck.visibility = View.VISIBLE
            imgEnglishCheck.visibility = View.INVISIBLE
            imgTurkishCheck.visibility = View.INVISIBLE
        })
        linlLanguageEnglish.setOnClickListener(View.OnClickListener {
            select_lang="en"
            imgArabicCheck.visibility = View.INVISIBLE
            imgEnglishCheck.visibility = View.VISIBLE
            imgTurkishCheck.visibility = View.INVISIBLE
        })
        linlLanguageTurkish.setOnClickListener(View.OnClickListener {
            select_lang="tr"
            imgArabicCheck.visibility = View.INVISIBLE
            imgEnglishCheck.visibility = View.INVISIBLE
            imgTurkishCheck.visibility = View.VISIBLE
        })
        btnOk.setOnClickListener {
            LocaleHelper.setLocale(getApplicationContext(), select_lang, select_lang)
            sendBroadcast(Intent("Language.changed"))
            recreate() //now restart.
            val refresh:Intent =  Intent(this, SplashActivity::class.java)
            startActivity(refresh)
            finish();
            val config = resources.configuration
            val locale = Locale(select_lang)
            Locale.setDefault(locale)
            config.locale = locale
            resources.updateConfiguration(config, resources.displayMetrics)
            dialog!!.dismiss()
        }
        imgClose.setOnClickListener { dialog!!.dismiss() }
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.show()
    }


    override fun attachBaseContext(base: android.content.Context?){
        super.attachBaseContext(base?.let { LocaleHelper.onAttach(it) })
    }


    @Override
    override fun onConfigurationChanged( newConfig: Configuration) {
        // refresh your views here
        super.onConfigurationChanged(newConfig);
// Checks the active language
        LocaleHelper.onAttach(this);
        super.onConfigurationChanged(newConfig);    }
}