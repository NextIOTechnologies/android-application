package com.tulib.alfaezounschool.activity.ui.base_activity

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.squareup.picasso.Picasso
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.db.database.AppDatabase
import com.tulib.alfaezounschool.utils.UserType
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


class BaseActivityViewModel(private var appDatabase: AppDatabase) : ViewModel()
{
    var liveDataUserName: MutableLiveData<String> = MutableLiveData()
    var liveDataUserClass: MutableLiveData<String> = MutableLiveData()
    var liveDataUserPicture: MutableLiveData<String> = MutableLiveData()


    init {
        viewModelScope.launch {
            val user = appDatabase.getUserDao().getUser()

            liveDataUserName.value = user?.name

            if(AppController.userType== UserType.Student) {
                liveDataUserClass.value = user?.userClass?:"Class"
            }else{
                var user_type="";
                if(user?.usertypeID.equals("1")){
                    user_type="Admin"
                }else if(user?.usertypeID.equals("2")){
                    user_type="Teacher"
                }else if(user?.usertypeID.equals("3")){
                    user_type="Student"
                }else if(user?.usertypeID.equals("4")){
                    user_type="Parent"
                }
                liveDataUserClass.value = user_type
            }
            liveDataUserPicture.value = ""
            

        }

    }
    fun getImageUrl(): String? {

        var url =""
        runBlocking {
            url =  appDatabase.getUserDao().getUser()?.photo?:""
        }
        // The URL will usually come from a model (i.e Profile)
        return url
    }

}

@BindingAdapter("bind:imageUrl")
fun loadImage(view: ImageView, imageUrl: String?) {
    Picasso.get()
            .load(imageUrl)
            .placeholder(R.drawable.pic)
            .into(view)
}
