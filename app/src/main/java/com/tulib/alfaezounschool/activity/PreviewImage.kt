package com.tulib.alfaezounschool.activity

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import com.tulib.alfaezounschool.R


class PreviewImage : AppCompatActivity() {
    var ivPreviewImagae: ImageView? =null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview_image);

        ivPreviewImagae=findViewById(R.id.ivPreviewImagae);
        var bundle :Bundle ?=intent.extras
        var imgUrl = bundle!!.getString("imgUrl")
        Picasso.get()
                .load(imgUrl)
                .into(ivPreviewImagae);

    }
}