package com.tulib.alfaezounschool.activity.ui.dashboard

import android.app.*
import android.content.ContentValues.TAG
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.activity.ui.accounts.Accounts
import com.tulib.alfaezounschool.activity.ui.announcements.AnnouncementActivity
import com.tulib.alfaezounschool.activity.ui.homework.HomeWorkActivity
import com.tulib.alfaezounschool.activity.ui.marks.MarksActivity
import com.tulib.alfaezounschool.activity.ui.studentactivity.StudentActivity
import com.tulib.alfaezounschool.activity.ui.timetable.TimeTableActivity
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.databinding.FragmentDashboardBinding
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import com.tulib.alfaezounschool.utils.Common
import com.github.sundeepk.compactcalendarview.CompactCalendarView
import com.github.sundeepk.compactcalendarview.CompactCalendarView.CompactCalendarViewListener
import com.github.sundeepk.compactcalendarview.domain.Event
import com.tulib.alfaezounschool.activity.ui.attendance.AttendanceActivity
import com.tulib.alfaezounschool.adapter.StudentSelectAdapter
import com.tulib.alfaezounschool.data.network.response.response_models.AttendanceModel
import com.tulib.alfaezounschool.data.network.response.response_models.StudentModel
import com.tulib.alfaezounschool.utils.UserType
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*
import com.squareup.picasso.Picasso
import com.tulib.alfaezounschool.activity.ui.parents.Parents
import com.tulib.alfaezounschool.activity.ui.students.Students
import com.tulib.alfaezounschool.activity.ui.students.StudentsViewModel
import com.tulib.alfaezounschool.activity.ui.students.StudentsViewModelFactory
import com.tulib.alfaezounschool.activity.ui.teachers.Teachers
import com.tulib.alfaezounschool.app.AppController.Companion.userType

class DashboardFragment : Fragment() {
    private val dashboardViewModelFactory: DashboardViewModelFactory by AppController.kodein().instance()
    private val loaderDialog: LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: DashboardViewModel
    private var attendenceList: ArrayList<AttendanceModel>? = null

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    var imgMenu: ImageView? = null
    var imgHomeWork: ImageView? = null
    var imgAttendance: ImageView? = null
    var imgOnlineExam: ImageView? = null
    var imgMark: ImageView? = null
    var imgInvoice: ImageView? = null
    var imgTimeTable: ImageView? = null
    var imgLogout: ImageView? = null
    var compactCalendarView: CompactCalendarView? = null
    private val simpleDateFormat = SimpleDateFormat("MMMM-yyyy", Locale.getDefault())
    private val DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    var tx_date: TextView? = null
    var ly_left: LinearLayout? = null
    var ly_right: LinearLayout? = null
    lateinit var myCalendar: Calendar
    var c: Date? = null
    var formattedDate: String? = null
    var dialog: Dialog? = null
    var contextx: Context? = null
    var imgMenuStudentDropIcon: ImageView? = null


    var relativeMenuStudentSelect: RelativeLayout? = null

    var txtMenuStudentName: TextView? = null
    var txtMenuStudentClass: TextView? = null
    var imgMenuStudentImage: CircleImageView? = null


    //Student Selection
    private lateinit var studentsModel: StudentsViewModel
    private val studentsViewModelFactory: StudentsViewModelFactory by AppController.kodein().instance()
    var studentListArrayList: ArrayList<StudentModel>? = null
    var studentmAdapter: StudentSelectAdapter? = null
    var recyclerViewStudentSelect: RecyclerView? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val binding: FragmentDashboardBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false)
        viewModel = ViewModelProvider(this, dashboardViewModelFactory).get(DashboardViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this
        contextx = this.requireContext()
        val dashboardViewModelFactory: DashboardViewModelFactory by AppController.kodein().instance()

        initViews(binding.root)
        initListeners()
        calendarlistener()
        setdate()


        tx_date!!.text = "" + formattedDate
        ly_right!!.setOnClickListener {
            compactCalendarView!!.showCalendarWithAnimation()
            compactCalendarView!!.showPreviousMonth()
        }
        ly_left!!.setOnClickListener {
            compactCalendarView!!.showCalendarWithAnimation()
            compactCalendarView!!.showNextMonth()
        }



        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        // Redirecting to next destination
        viewModel.initializeLiveDataForAttendanceResponse().observe(viewLifecycleOwner,
                { result ->
                    if (result != null) {

                        attendenceList = result.attendanceList
                        setdate()
                        viewModel.emptyLiveDataForAttendanceResponse()
                    }
                })

        // Redirecting to next destination
        viewModel.initializeLiveDataForCalender().observe(viewLifecycleOwner,
                { result ->
                    if (result != null) {

                        liCalender?.visibility = if (result) View.VISIBLE else View.GONE
                        viewModel.emptyLiveDataForCalender()
                        init_student()
                    }
                })

        viewModel.initializeLiveDataForLoader().observe(viewLifecycleOwner,
                { result ->
                    if (result != null) {
                        if (result) loaderDialog.showLoader(requireActivity() as AppCompatActivity) else loaderDialog.hideLoader(requireActivity() as AppCompatActivity)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        tvSeeAll.setOnClickListener {
            Common.startActivity(requireContext(), AnnouncementActivity::class.java)

        }

        // Redirecting to next destination
        viewModel.initializeLiveDataForNoticeResponse().observe(viewLifecycleOwner,
                { result ->
                    if (result != null) {
                        if (result.size > 0) {
                            relative_notice.visibility = View.VISIBLE
                            tvNoticeTitle.text = result.get(0).title
                            tvNoticeDetails.text = result.get(0).notice
                            val simpleDateFormat = SimpleDateFormat("yyyy-mm-dd")
                            val simpleDateFormat2 = SimpleDateFormat("dd mm,yyyy")
                            val date = simpleDateFormat.parse(result.get(0).date)
                            txtNoticeDate.text = simpleDateFormat2.format(date)
                        }
                        viewModel.emptyLiveDataForNoticeResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity, error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity, error)
                viewModel.emptyLiveDataError()
            }
        })

        viewModel.getNoticeList()
        AppController.studentid?.let {
            viewModel.getAttendanceList(it
            )
        }


    }


    private fun initViews(v: View) {
        dialog = Dialog(v.context)

        imgHomeWork = v.findViewById(R.id.imgHomeWork)
        imgTimeTable = v.findViewById(R.id.imgTimeTable)
        imgOnlineExam = v.findViewById(R.id.imgStudentActivity)
        imgAttendance = v.findViewById(R.id.imgAttendance)
        imgMark = v.findViewById(R.id.imgMark)
        imgInvoice = v.findViewById(R.id.imgInvoice)
        imgLogout = v.findViewById(R.id.imgDasboardLogout)
        compactCalendarView = v.findViewById<View>(R.id.compactcalendar_view) as CompactCalendarView
        tx_date = v.findViewById<View>(R.id.text) as TextView
        ly_left = v.findViewById<View>(R.id.layout_left) as LinearLayout
        ly_right = v.findViewById<View>(R.id.layout_right) as LinearLayout
        imgMenuStudentImage = v.findViewById(R.id.imgMenuStudentImage)
        relativeMenuStudentSelect = v.findViewById(R.id.relative_menu_student_select)
        txtMenuStudentName = v.findViewById(R.id.txtMenuStudentName)
        txtMenuStudentClass = v.findViewById(R.id.txtMenuStudentClass)
        imgMenuStudentDropIcon = v.findViewById(R.id.imgMenuStudentDropIcon)


    }

    private fun initListeners() {
        imgAttendance!!.setOnClickListener {

            Common.startActivity(requireActivity(), AttendanceActivity::class.java)
            //AppUtils.showSnackBar(requireActivity() as AppCompatActivity,"Coming Soon")

        }
        imgOnlineExam!!.setOnClickListener { Common.startActivity(requireActivity(), StudentActivity::class.java) }
        imgHomeWork!!.setOnClickListener { Common.startActivity(requireActivity(), HomeWorkActivity::class.java) }
        imgTimeTable!!.setOnClickListener { Common.startActivity(requireActivity(), TimeTableActivity::class.java) }
        imgMark!!.setOnClickListener { Common.startActivity(requireActivity(), MarksActivity::class.java) }
        imgInvoice!!.setOnClickListener {
            //   count = 1
            Common.startActivity(requireActivity(), Accounts::class.java)

        }
        imgLogout!!.setOnClickListener {
            (requireActivity() as BaseActivity).logout()
        }


    }


    //calendar method
    fun calendarlistener() {
        compactCalendarView!!.setListener(object : CompactCalendarViewListener {
            override fun onDayClick(dateClicked: Date) {
                if (DateFormat.format(dateClicked) == "2018-11-21") {
                    Toast.makeText(activity, DateFormat.format(dateClicked) + " This day your brother birth day ", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(activity, DateFormat.format(dateClicked) + " In This day no Events Available", Toast.LENGTH_LONG).show()
                }
            }

            override fun onMonthScroll(firstDayOfNewMonth: Date) {
                compactCalendarView!!.removeAllEvents()
                setdate()
                tx_date!!.text = simpleDateFormat.format(firstDayOfNewMonth)
            }
        })
    }


    //get current date
    fun setdate() {
        c = Calendar.getInstance().time
        formattedDate = simpleDateFormat!!.format(c)
        compactCalendarView!!.setUseThreeLetterAbbreviation(true)
        myCalendar = Calendar.getInstance()
        attendenceList?.let {

            val simpleMonthYearFormat = SimpleDateFormat("MM-yyyy")
            val simpleYearFormat = SimpleDateFormat("yyyy")
            val simpleMonthFormat = SimpleDateFormat("MM")
            val currentMonth = compactCalendarView!!.firstDayOfCurrentMonth
            var position = -1
            for (j in it.indices) {
                val availableMonth = simpleMonthYearFormat.parse(it[j].monthyear)

                if (availableMonth == currentMonth) {
                    position = j
                    break
                }
            }
            if (position > -1) {
                for (i in 1..31) {

                    myCalendar?.set(Calendar.YEAR, simpleYearFormat.format(currentMonth).toInt())
                    myCalendar?.set(Calendar.MONTH, simpleMonthFormat.format(currentMonth).toInt() - 1)
                    myCalendar?.set(Calendar.DAY_OF_MONTH, i)

                    val value = getValue(it[position], i)
                    val color = when (value) {
                        "P" -> "#38b859"
                        "A" -> "#f42727"
                        "LA" -> "#f9ac58"
                        "H" -> "#17177f"
                        "L" -> "#844206"
                        "LE" -> "#1ac1a1"
                        else -> ""
                    }

                    if (color.isNotEmpty()) {
                        val event = Event(Color.parseColor(color), myCalendar!!.getTimeInMillis(), "test")
                        compactCalendarView!!.addEvent(event)
                    }
                }
            }
        }
    }

    fun getValue(attendanceModel: AttendanceModel, position: Int): String? {
        return when (position) {
            1 -> attendanceModel.a1
            2 -> attendanceModel.a2
            3 -> attendanceModel.a3
            4 -> attendanceModel.a4
            5 -> attendanceModel.a5
            6 -> attendanceModel.a6
            7 -> attendanceModel.a7
            8 -> attendanceModel.a8
            9 -> attendanceModel.a9
            10 -> attendanceModel.a10
            11 -> attendanceModel.a11
            12 -> attendanceModel.a12
            13 -> attendanceModel.a13
            14 -> attendanceModel.a14
            15 -> attendanceModel.a15
            16 -> attendanceModel.a16
            17 -> attendanceModel.a17
            18 -> attendanceModel.a18
            19 -> attendanceModel.a19
            20 -> attendanceModel.a20
            21 -> attendanceModel.a21
            22 -> attendanceModel.a22
            23 -> attendanceModel.a23
            24 -> attendanceModel.a24
            25 -> attendanceModel.a25
            26 -> attendanceModel.a26
            27 -> attendanceModel.a27
            28 -> attendanceModel.a28
            29 -> attendanceModel.a29
            30 -> attendanceModel.a30
            31 -> attendanceModel.a31
            else -> null
        }

    }


    fun init_student() {


        Log.d("TAG", "onCreateViewxxx: " + UserType.Student + " ssdx:" + userType)
        if (AppController.userType !== UserType.Student) {
            imgMenuStudentDropIcon?.visibility = View.VISIBLE
            relativeMenuStudentSelect!!.setOnClickListener {
                DialogSearchStudent()
            }
            initstudentModel()


        } else {
            imgMenuStudentDropIcon?.visibility = View.GONE
        }
        if (AppController.userType == UserType.Admin) {
            linearl_user_count?.visibility = View.VISIBLE
//            linear_item_h_t_a?.visibility = View.GONE
//            linear_item_a_m_a?.visibility = View.GONE

            relative_students.setOnClickListener { Common.startActivity(requireActivity(), Students::class.java) }
            relative_parents.setOnClickListener { Common.startActivity(requireActivity(), Parents::class.java) }
            relative_teachers.setOnClickListener { Common.startActivity(requireActivity(), Teachers::class.java) }

        }else{
            linearl_user_count?.visibility = View.GONE
//            linear_item_h_t_a?.visibility = View.VISIBLE
//            linear_item_a_m_a?.visibility = View.VISIBLE
        }

    }

    private fun DialogSearchStudent() {
        dialog?.setContentView(R.layout.dialog_search_students)
        recyclerViewStudentSelect = dialog!!.findViewById<RecyclerView>(R.id.recyclerSearchStudent)

        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        studentmAdapter = StudentSelectAdapter(activity as Context, studentListArrayList)
        recyclerViewStudentSelect?.setLayoutManager(LinearLayoutManager(contextx))
        recyclerViewStudentSelect?.setAdapter(studentmAdapter)


        val searchViewDialogSearchStudent = dialog!!.findViewById<androidx.appcompat.widget.SearchView>(R.id.searchViewDialogSearchStudent)

        searchViewDialogSearchStudent.setOnQueryTextListener(object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                studentmAdapter!!.filter.filter(newText)
                return false
            }

        })

        studentmAdapter!!.setOnItemClickListener(object : StudentSelectAdapter.ClickListener {
            override fun onClick(pos: Int, aView: View) {
                val name = studentListArrayList?.get(pos)?.name
                val className = studentListArrayList?.get(pos)?.className
                val studentID = studentListArrayList?.get(pos)?.studentID
                val studentPhoto = studentListArrayList?.get(pos)?.photo

                //The App Crash here
                Log.d(TAG, "onClick:t " + pos)
                txtMenuStudentName?.setText(name);
                txtMenuStudentClass?.setText(className);

                Picasso.get()
                        .load(studentPhoto)
                        .placeholder(R.drawable.pic)
                        .into(imgMenuStudentImage)

                AppController.studentid = studentID
            //    Toast.makeText(context, "$name selected", Toast.LENGTH_SHORT).show()
                dialog!!.dismiss()
            }
        })



        dialog!!.show()
    }


    private fun initstudentModel() {


        studentsModel = ViewModelProvider(this, studentsViewModelFactory).get(StudentsViewModel::class.java)
        studentsModel.initializeLiveDataForLoader().observe(requireActivity() as AppCompatActivity,
                { result ->
                    if (result != null) {
                        if (result) loaderDialog.showLoader(requireActivity() as AppCompatActivity) else loaderDialog.hideLoader(requireActivity() as AppCompatActivity)
                        studentsModel.emptyLiveDataForLoader()
                    }
                })
        studentsModel.initializeliveDataForStudentResponse().observe(requireActivity() as AppCompatActivity,
                { result ->
                    if (result != null) {
                        studentmAdapter?.setData(result.studentsList)
                        studentListArrayList = result.studentsList
                        studentsModel.emptyliveDataForStudentResponse()

                        for (row in studentListArrayList!!) {

                            if (!AppController.studentid?.isEmpty()!!) {
                                if (row.studentID.toLowerCase(Locale.ROOT).contains(AppController.studentid!!.toLowerCase(Locale.ROOT))) {
                                    txtMenuStudentName?.setText(row.name);
                                    txtMenuStudentClass?.setText(row.className);
                                    AppController.studentid = row.studentID
                                }
                            }
                        }
                    }
                })
        studentsModel.initializeLiveDataGenericError().observe(requireActivity() as AppCompatActivity, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity, error.message)
                studentsModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        studentsModel.initializeLiveDataError().observe(requireActivity() as AppCompatActivity, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity, error)
                studentsModel.emptyLiveDataError()
            }
        })
        studentsModel.getStudents()
    }
}