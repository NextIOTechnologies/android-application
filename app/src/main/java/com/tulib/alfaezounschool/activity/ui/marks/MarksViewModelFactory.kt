package com.tulib.alfaezounschool.activity.ui.marks

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.db.database.AppDatabase
import com.tulib.alfaezounschool.data.repository.IMarksRepository

@Suppress("UNCHECKED_CAST")
class MarksViewModelFactory(var marksRepository: IMarksRepository,val appDatabase: AppDatabase) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return MarksViewModel(marksRepository,appDatabase) as T
    }
}