package com.tulib.alfaezounschool.activity.ui.teachers

import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.adapter.TeachersAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.TeachersModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance
import java.util.*

class Teachers : BaseActivity() {
    private val teachersViewModelFactory : TeachersViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: TeachersViewModel


    var recyclerViewTeachers: RecyclerView? = null
    var teachersAdapter: TeachersAdapter? = null
    var teachersModelArrayList: ArrayList<TeachersModel>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_teachers, frameLayout)
        initViews()
        initListeners()
        viewModel = ViewModelProvider(this, teachersViewModelFactory).get(TeachersViewModel::class.java)

        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeliveDataForTeacherResponse().observe(this,
                { result ->
                    if(result!=null)
                    {
                        teachersAdapter?.setData(result.teachersList)
                        viewModel.emptyliveDataForTeacherResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error)
                viewModel.emptyLiveDataError()
            }
        })
            viewModel.getTeachers()
        }

    private fun initViews() {
        recyclerViewTeachers = findViewById(R.id.recyclerTeachers)
        teachersModelArrayList = ArrayList()

        teachersAdapter = TeachersAdapter(this@Teachers, teachersModelArrayList)
        recyclerViewTeachers?.setLayoutManager(LinearLayoutManager(this))
        recyclerViewTeachers?.setAdapter(teachersAdapter)
    }

    private fun initListeners() {
        imgMenu!!.setOnClickListener { drawerLayout!!.openDrawer(GravityCompat.START) }
    }


}