package com.tulib.alfaezounschool.activity.ui.timetable

import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.adapter.TimeTableAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.TimeTableList
import com.tulib.alfaezounschool.data.network.response.response_models.TimeTableModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance
import java.util.*

class TimeTableActivity : BaseActivity() {
    var recyclerViewTimeTable: RecyclerView? = null
    var timeTableAdapter: TimeTableAdapter? = null
    var timeTableModelArrayList: ArrayList<TimeTableModel>? = null

    private val timeTableViewModelFactory : TimeTableViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: TimeTableViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_time_table, frameLayout)
        initViews()
        initListeners()
        viewModel = ViewModelProvider(this, timeTableViewModelFactory).get(TimeTableViewModel::class.java)
        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForTimeTableResponse().observe(this,
                { result ->
                    if(result!=null)
                    {
                        timeTableAdapter?.setData(getList(result),result)
                        viewModel.emptyLiveDataForTimeTableResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error)
                viewModel.emptyLiveDataError()
            }
        })
        AppController.studentid?.let {
            viewModel.getTimeTableList(it
            )
        }
    }

    private fun initViews() {
        recyclerViewTimeTable = findViewById(R.id.recyclerTimeTable)
        timeTableModelArrayList = ArrayList()
        timeTableAdapter = TimeTableAdapter(this@TimeTableActivity, timeTableModelArrayList!!)
        recyclerViewTimeTable?.setLayoutManager(LinearLayoutManager(this))
        recyclerViewTimeTable?.setAdapter(timeTableAdapter)
    }

    private fun initListeners() {
        imgMenu!!.setOnClickListener { drawerLayout!!.openDrawer(GravityCompat.START) }
    }

    fun getList(result:TimeTableList):ArrayList<TimeTableModel>
    {
        val timeTableModelArrayList= ArrayList<TimeTableModel>()

        if(result.mondayList?.size?:0>0)
        {
            val data = result.mondayList?.get(0)!!
            data.day="MONDAY"
            timeTableModelArrayList.add(data)
        }
        if(result.sundayList?.size?:0>0)
        {
            val data = result.sundayList?.get(0)!!
            data.day="SUNDAY"
            timeTableModelArrayList.add(data)
        }

        if(result.tuesdayList?.size?:0>0)
        {
            val data = result.tuesdayList?.get(0)!!
            data.day="TUESDAY"
            timeTableModelArrayList.add(data)
        }

        if(result.wednesdayList?.size?:0>0)
        {
            val data = result.wednesdayList?.get(0)!!
            data.day="WEDNESDAY"
            timeTableModelArrayList.add(data)
        }

        if(result.thursdayList?.size?:0>0)
        {
            val data = result.thursdayList?.get(0)!!
            data.day="THURSDAY"
            timeTableModelArrayList.add(data)
        }
        if(result.fridayList?.size?:0>0)
        {
            val data = result.fridayList?.get(0)!!
            data.day="FRIDAY"
            timeTableModelArrayList.add(data)
        }
        if(result.saturdayList?.size?:0>0)
        {
            val data = result.saturdayList?.get(0)!!
            data.day="SATURDAY"
            timeTableModelArrayList.add(data)
        }

        return timeTableModelArrayList
    }


}