package com.tulib.alfaezounschool.activity.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IProfileRepository
import kotlinx.coroutines.launch


class ProfileViewModel(var profileRepository: IProfileRepository) : ViewModel()
{
    private lateinit var liveDataForStudentDetailsResponse : MutableLiveData<StudentDetails>

    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>


    fun initializeLiveDataForStudentDetailsResponse (): LiveData<StudentDetails> {
        liveDataForStudentDetailsResponse = MutableLiveData()
        return liveDataForStudentDetailsResponse
    }
    fun emptyLiveDataForStudentDetailsResponse (){liveDataForStudentDetailsResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}

    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}

    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}



    fun studentDetails(id:String) {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = profileRepository.studentInfo(id)

            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<StudentDetails>
                     liveDataForStudentDetailsResponse.value = data.data
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
