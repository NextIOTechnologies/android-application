package com.tulib.alfaezounschool.activity

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.widget.ImageView
import androidx.cardview.widget.CardView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.activity.ui.dashboard.DashboardFragment
import com.tulib.alfaezounschool.activity.ui.exam.ExamScheduleFragment
import com.tulib.alfaezounschool.activity.ui.library.LibraryFragment
import com.tulib.alfaezounschool.activity.ui.message.message_view.MessageFragment
import com.tulib.alfaezounschool.activity.ui.notification.NotificationFragment
import com.tulib.alfaezounschool.app.AppController.Companion.fetchCommonData
import com.tulib.alfaezounschool.utils.Common
import com.tulib.alfaezounschool.utils.Common.AddFragment
import com.tulib.alfaezounschool.utils.LocaleHelper.onAttach


class MainActivity : BaseActivity() {
    private var bottomNavigationView: BottomNavigationView? = null
    var selectedId = 2
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_main, frameLayout)
        onAttach(this)
        initViews()
        initListeners()


    }

    override fun onDestroy() {
        super.onDestroy()
        fetchCommonData = false
    }

    private fun initViews() {
        bottomNavigationView = findViewById(R.id.bottom_nav)
        cardHome = findViewById(R.id.cardBottomHome)
        imgSelected = findViewById(R.id.imgHomePurple)
        bottomNavigationView?.setOnNavigationItemSelectedListener(listener)

        val notification_trigger_id :String = getIntent().getStringExtra("NOTIFICATION_TRIGGER_ID").toString();

        if(notification_trigger_id.equals("Notifications")){
            imgSelected!!.setImageResource(R.drawable.notification_icon)
            AddFragment(NotificationFragment(), this@MainActivity)
        }else{
            imgSelected!!.setImageResource(R.drawable.home_icon)
            AddFragment(DashboardFragment(), this@MainActivity)
        }
        imgSelected!!.setImageResource(R.drawable.home_purple)
        bottomNavigationView?.getMenu()?.findItem(R.id.dashboard)?.isChecked = true
        if (Common.count == 1) {
            bottomNavigationView?.getMenu()?.findItem(R.id.message)?.isChecked = true
        } else if (Common.count == 2) {
            bottomNavigationView?.getMenu()?.findItem(R.id.library)?.isChecked = true
        } else if (Common.count == 3) {
            bottomNavigationView?.getMenu()?.findItem(R.id.notification)?.isChecked = true
        }
    }

    private fun initListeners() {
//        imgMenu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                drawerLayout.openDrawer(GravityCompat.START);
//            }
//        });
        cardHome!!.setOnClickListener {
            imgSelected!!.setImageResource(R.drawable.home_purple)
            bottomNavigationView!!.menu.findItem(R.id.dashboard).isChecked = true
            AddFragment(DashboardFragment(), this@MainActivity)
        }
    }

    var listener = BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
        when (menuItem.itemId) {
            R.id.report -> {
                imgSelected!!.setImageResource(R.drawable.home_icon)
                AddFragment(ExamScheduleFragment(), this@MainActivity)
            }
            R.id.message -> {
                imgSelected!!.setImageResource(R.drawable.home_icon)
                AddFragment(MessageFragment(), this@MainActivity)
            }
            R.id.notification -> {
                imgSelected!!.setImageResource(R.drawable.home_icon)
                AddFragment(NotificationFragment(), this@MainActivity)
            }
            R.id.library -> {
                imgSelected!!.setImageResource(R.drawable.home_icon)
                //  Common.AddFragment(new ChatListFragment(), MainActivity.this);
                AddFragment(LibraryFragment(), this@MainActivity)
            }
        }
        true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        // Common.AddFragment(new DashboardFragment(), MainActivity.this);
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(onAttach(newBase!!))
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        onAttach(this)
    }
}