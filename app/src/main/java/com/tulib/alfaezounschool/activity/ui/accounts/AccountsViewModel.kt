package com.tulib.alfaezounschool.activity.ui.accounts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IAccountsRepository
import kotlinx.coroutines.launch

class AccountsViewModel(private var accountsRepository: IAccountsRepository) : ViewModel()
{
    private lateinit var liveDataForAccountsResponse : MutableLiveData<ArrayList<AccountsModel>>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

    fun initializeLiveDataForAccountsResponse(): LiveData<ArrayList<AccountsModel>> {
        liveDataForAccountsResponse = MutableLiveData()
        return liveDataForAccountsResponse
    }
    fun emptyLiveDataForNotificationResponse(){liveDataForAccountsResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getAccountsList() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = accountsRepository.accountsList()
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<AccountsList>
                    if(data.data.accountsList.size==0)
                    {
                        liveDataError.value = "No Invoice Available"
                    }
                    else liveDataForAccountsResponse.value = data.data.accountsList
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
