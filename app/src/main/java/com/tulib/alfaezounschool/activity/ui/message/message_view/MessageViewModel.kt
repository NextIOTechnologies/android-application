package com.tulib.alfaezounschool.activity.ui.message.message_view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IConversationRepository
import com.tulib.alfaezounschool.utils.DataUtil
import kotlinx.coroutines.launch

class MessageViewModel(private var conversationRepository: IConversationRepository) : ViewModel()
{
    private lateinit var liveDataForConversationResponse : MutableLiveData<MessagesList>
    private lateinit var liveDataForConversationSentResponse : MutableLiveData<Boolean>
    private lateinit var liveDataForMessageUserResponse : MutableLiveData<UserList>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

     var subject :String =""
     var message :String =""

    var userTypeId :String =""
    var userId :String =""

    fun initializeLiveDataForMessageUserResponse(): LiveData<UserList> {
        liveDataForMessageUserResponse = MutableLiveData()
        return liveDataForMessageUserResponse
    }
    fun emptyLiveDataForMessageUserResponse(){liveDataForMessageUserResponse.value= null}


    fun initializeLiveDataForConversationResponse(): LiveData<MessagesList> {
        liveDataForConversationResponse = MutableLiveData()
        return liveDataForConversationResponse
    }
    fun emptyLiveDataForConversationResponse(){liveDataForConversationResponse.value= null}

    fun initializeLiveDataForConversationSentResponse(): LiveData<Boolean> {
        liveDataForConversationSentResponse = MutableLiveData()
        return liveDataForConversationSentResponse
    }
    fun emptyLiveDataForConversationSentResponse(){liveDataForConversationSentResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getMessageList() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = conversationRepository.conversations()
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<MessagesList>
                    if(data.data.conversationList.size==0)
                    {
                        liveDataError.value = "No Conversations Available"
                    }
                     liveDataForConversationResponse.value = data.data
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }

    fun getMessageUsers() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = conversationRepository.getMessageUserList(userTypeId)
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<UserList>
                    liveDataForMessageUserResponse.value = data.data
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }

    fun sendNewMessage() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = conversationRepository.sendNewMessage(DataUtil.getNewMessageJson(subject,message,userTypeId,userId))
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    liveDataForConversationSentResponse.value=true
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
