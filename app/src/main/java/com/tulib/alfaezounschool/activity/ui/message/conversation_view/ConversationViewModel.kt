package com.tulib.alfaezounschool.activity.ui.message.conversation_view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IConversationRepository
import com.tulib.alfaezounschool.utils.DataUtil
import kotlinx.coroutines.launch

class ConversationViewModel(private var conversationRepository: IConversationRepository) : ViewModel()
{
    private lateinit var liveDataForConversationResponse : MutableLiveData<ConversationList>
    private lateinit var liveDataForConversationSentResponse : MutableLiveData<Boolean>

    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

    var id :String=""
     var subject :String =""
     var message :String =""
     lateinit var userTypeModel:UserTypeModel

    fun initializeLiveDataForConversationResponse(): LiveData<ConversationList> {
        liveDataForConversationResponse = MutableLiveData()
        return liveDataForConversationResponse
    }
    fun emptyLiveDataForConversationResponse(){liveDataForConversationResponse.value= null}

    fun initializeLiveDataForConversationSentResponse(): LiveData<Boolean> {
        liveDataForConversationSentResponse = MutableLiveData()
        return liveDataForConversationSentResponse
    }
    fun emptyLiveDataForConversationSentResponse(){liveDataForConversationSentResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getConversationList() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = conversationRepository.conversationsView(id)
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<ConversationList>
                    if(data.data.messages.size==0)
                    {
                        liveDataError.value = "No Conversations Available"
                    }
                     liveDataForConversationResponse.value = data.data
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }


    fun sendNewMessage() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = conversationRepository.sendMessage(DataUtil.getMessageJson(id,message))
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    liveDataForConversationSentResponse.value=true
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
