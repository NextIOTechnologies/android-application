package com.tulib.alfaezounschool.activity.ui.base_activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.annotation.StringRes
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.MainActivity
import com.tulib.alfaezounschool.activity.ui.login.LoginActivity
import com.tulib.alfaezounschool.activity.ui.settings.SettingActivity
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.api_call.api_manger.LogoutApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.activity.ui.profile.ProfileActivity
import com.tulib.alfaezounschool.utils.AppUtils
import com.tulib.alfaezounschool.utils.Common
import com.tulib.alfaezounschool.utils.LocaleHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance
import java.util.*
import com.tulib.alfaezounschool.activity.ui.notification.NotificationOpennerActivity
import com.tulib.alfaezounschool.adapter.MenuAdapter
import com.tulib.alfaezounschool.databinding.ActivityBaseBinding

open class BaseActivity : AppCompatActivity() {
    private val baseActivityViewModelFactory : BaseActivityViewModelFactory by AppController.kodein().instance()
    private lateinit var viewModel: BaseActivityViewModel


      var drawerLayout: DrawerLayout?=null
    private val logoutApiManager : LogoutApiManager by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    var actionBarDrawerToggle: ActionBarDrawerToggle? = null
    var cardHome: CardView? = null
    var imgSelected: ImageView? = null
    private lateinit var recyclerViewMenu: RecyclerView
    var menuAdapter: MenuAdapter? = null
    var imgBack: ImageView? = null
    var imgLogout: ImageView? = null
    lateinit var  imgHome: ImageView
    var imgProfile: ImageView? = null
    var imgNotification: ImageView? = null
    var imgSetting: ImageView? = null
    @JvmField
    var imgMenu: ImageView? = null
    var cardDashboard: CardView? = null
    var cardSetting: CardView? = null
    var cardNotification: CardView? = null
    var cardProfile: CardView? = null
    var image = intArrayOf(R.drawable.dashboard, R.drawable.attendance_white, R.drawable.student_activity, R.drawable.homework_white, R.drawable.timetable_white, R.drawable.media, R.drawable.grade, R.drawable.account, R.drawable.announcement, R.drawable.complain, R.drawable.profile)

    @JvmField
    var frameLayout: FrameLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LocaleHelper.onAttach(this)
        val binding : ActivityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);
        viewModel = ViewModelProvider(this, baseActivityViewModelFactory).get(BaseActivityViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this

        initViews()
        initListeners()





    }



    fun Context.getStringByLocale(@StringRes stringRes: Int, locale: Locale, vararg formatArgs: Any): String {
        val configuration = Configuration(resources.configuration)
        configuration.setLocale(locale)
        return createConfigurationContext(configuration).resources.getString(stringRes, *formatArgs)
    }

    @SuppressLint("RestrictedApi")
    private fun initViews() {
        frameLayout = findViewById(R.id.content_frame_activity)
        imgMenu = findViewById(R.id.menu)
        cardHome = findViewById(R.id.cardBottomHome)
        imgSelected = findViewById(R.id.imgHomePurple)
        recyclerViewMenu = findViewById(R.id.recyclerMenuItem)
        drawerLayout = findViewById(R.id.drawerlayout)
        imgBack = findViewById(R.id.imgMenuBack)
        imgLogout = findViewById(R.id.imgMenuLogout)
        imgHome = findViewById(R.id.imgHome)
        imgProfile = findViewById(R.id.imgProfile)
        imgNotification = findViewById(R.id.imgNotification)
        imgSetting = findViewById(R.id.imgSetting)
        cardProfile = findViewById(R.id.cardProfile)
        cardDashboard = findViewById(R.id.cardHome)
        cardSetting = findViewById(R.id.cardSetting)
        cardNotification = findViewById(R.id.cardNotification)
        imgHome.setImageResource(R.drawable.home_purple)

        val lang = LocaleHelper.getLanguage(this)

        var name = arrayOf(
                getStringByLocale(R.string.dashboard,Locale(lang)),
                getStringByLocale(R.string.attendance,Locale(lang)),
                getStringByLocale(R.string.student_activity,Locale(lang)),
                getStringByLocale(R.string.home_work,Locale(lang)),
                getStringByLocale(R.string.time_table,Locale(lang)),
                getStringByLocale(R.string.media,Locale(lang)),
                getStringByLocale(R.string.mark,Locale(lang)),
                getStringByLocale(R.string.account,Locale(lang)),
                getStringByLocale(R.string.announcement,Locale(lang)),
                getStringByLocale(R.string.complain,Locale(lang)),
                getStringByLocale(R.string.studentList,Locale(lang))
        )


        menuAdapter = MenuAdapter(this, image, name )
        recyclerViewMenu.setLayoutManager(LinearLayoutManager(this))
        recyclerViewMenu.setAdapter(menuAdapter)
        actionBarDrawerToggle = object : ActionBarDrawerToggle(this, drawerLayout,
                R.string.drawer_open,
                R.string.drawer_close) {
            override fun onOptionsItemSelected(item: MenuItem): Boolean {
                if (item != null && item.itemId == android.R.id.home) {
                    drawerLayout?.let {
                        if (it.isDrawerOpen(Gravity.LEFT)) {
                            it.closeDrawer(Gravity.LEFT)
                        } else {
                            it.openDrawer(Gravity.LEFT)
                        }
                    }
                }
                return false
            }
        }
        drawerLayout?.closeDrawers()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        actionBarDrawerToggle!!.syncState()
    }

    private fun initListeners() {
        imgMenu!!.setOnClickListener {
            drawerLayout!!.openDrawer(GravityCompat.START)

        }
        imgBack!!.setOnClickListener {
            drawerLayout!!.closeDrawers()
        }
        imgLogout!!.setOnClickListener {
            logout()
        }
        cardProfile!!.setOnClickListener {
            imgProfile!!.setImageResource(R.drawable.profile_purple)
            imgHome.setImageResource(R.drawable.home)
            imgNotification!!.setImageResource(R.drawable.notification)
            imgSetting!!.setImageResource(R.drawable.setting)
            Common.startActivity(this@BaseActivity, ProfileActivity::class.java)
            drawerLayout!!.closeDrawers()
        }
        cardSetting!!.setOnClickListener {
            imgSetting!!.setImageResource(R.drawable.setting_purple)
            imgHome.setImageResource(R.drawable.home)
            imgNotification!!.setImageResource(R.drawable.notification)
            imgProfile!!.setImageResource(R.drawable.profile)
            Common.startActivity(this@BaseActivity, SettingActivity::class.java)
            drawerLayout!!.closeDrawers()
        }
        cardDashboard!!.setOnClickListener {
            imgHome.setImageResource(R.drawable.home_purple)
            imgSetting!!.setImageResource(R.drawable.setting)
            imgNotification!!.setImageResource(R.drawable.notification)
            imgProfile!!.setImageResource(R.drawable.profile)
            Common.startActivity(this@BaseActivity, MainActivity::class.java)
            drawerLayout!!.closeDrawers()
        }
        cardNotification!!.setOnClickListener { view ->
            Common.count = 3
            imgNotification!!.setImageResource(R.drawable.notification_purple)
            imgSetting!!.setImageResource(R.drawable.setting)
            imgHome.setImageResource(R.drawable.home)
            imgProfile!!.setImageResource(R.drawable.profile)
            Common.startActivity(this@BaseActivity, NotificationOpennerActivity::class.java)
            drawerLayout!!.closeDrawers()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        //Common.AddFragment(new DashboardFragment(), MainActivity.this);
    }

    public override fun onStart() {
        super.onStart()

        // Enclose everything in a try block so we can just
        // use the default view if anything goes wrong.
        try {
            // Get the font size value from SharedPreferences.
            val settings = getSharedPreferences("com.tulib.alfaezounschool", MODE_PRIVATE)

            // Get the font size option.  We use "FONT_SIZE" as the key.
            // Make sure to use this key when you set the value in SharedPreferences.
            // We specify "Medium" as the default value, if it does not exist.
            val fontSizePref = settings.getString("FONT_SIZE", "Medium")

            // Select the proper theme ID.
            // These will correspond to your theme names as defined in themes.xml.
            var themeID = R.style.FontSizeMedium
            if (fontSizePref === "Small") {
                themeID = R.style.FontSizeSmall
            } else if (fontSizePref === "Large") {
                themeID = R.style.FontSizeLarge
            }
            // Set the theme for the activity.
            setTheme(themeID)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }



    public fun logout()
    {
        CoroutineScope(Dispatchers.Main).launch {
            loaderDialog.showLoader(this@BaseActivity)
            val authResponse = logoutApiManager.logout()
            loaderDialog.hideLoader(this@BaseActivity)
            when (authResponse) {
                is Resource.Success<*> -> {
                    Common.startActivity(this@BaseActivity, LoginActivity::class.java)
                    finish()
                }
                is Resource.Error ->  AppUtils.showSnackBar(this@BaseActivity,authResponse.value)
                is Resource.GenericError ->
                    AppUtils.showSnackBar(this@BaseActivity,authResponse.value.message)
            }
        }

    }

    override fun attachBaseContext(base: android.content.Context?){
        super.attachBaseContext(base?.let { LocaleHelper.onAttach(it) })
    }


    @Override
    override fun onConfigurationChanged( newConfig: Configuration) {
        // refresh your views here
        super.onConfigurationChanged(newConfig);
// Checks the active language
        LocaleHelper.onAttach(this);

    }




}