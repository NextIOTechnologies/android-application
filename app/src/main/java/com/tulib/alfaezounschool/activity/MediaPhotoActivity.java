package com.tulib.alfaezounschool.activity;

import android.os.Bundle;
import android.widget.GridView;

import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity;
import com.tulib.alfaezounschool.adapter.MediaPhotoGridAdapter;
import com.tulib.alfaezounschool.R;

public class MediaPhotoActivity extends BaseActivity {
    GridView gridView;
    int[] photo = {R.drawable.photo,R.drawable.photo,R.drawable.photo,R.drawable.photo,R.drawable.photo,R.drawable.photo};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_media,frameLayout);
        initViews();
    }

    private void initViews(){
        gridView = findViewById(R.id.gridMedia);
        MediaPhotoGridAdapter myAdapter = new MediaPhotoGridAdapter(MediaPhotoActivity.this,photo);
        gridView.setAdapter(myAdapter);
    }

}