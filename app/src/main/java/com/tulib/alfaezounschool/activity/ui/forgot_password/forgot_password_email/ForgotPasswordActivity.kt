package com.tulib.alfaezounschool.activity.ui.forgot_password.forgot_password_email


import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.forgot_password.verification_code.VerificationCodeActivity
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.ResponseMessages
import com.tulib.alfaezounschool.databinding.ActivityForgotPasswordBinding
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import com.tulib.alfaezounschool.utils.Common
import org.kodein.di.generic.instance

class ForgotPasswordActivity : AppCompatActivity() {
    var btnSubmit: Button? = null
    var imgBack: ImageView? = null
    private val forgotPasswordViewModelFactory : ForgotPasswordViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: ForgotPasswordEmailViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val binding :ActivityForgotPasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        viewModel = ViewModelProvider(this, forgotPasswordViewModelFactory).get(ForgotPasswordEmailViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this

        initViews()
        initListeners()

        // Redirecting to next destination
        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForForgotPasswordResponse().observe(this,
                { result ->
                    if(result!=null)
                    {  Common.CustomDialog(this@ForgotPasswordActivity, VerificationCodeActivity::class.java,result, "Ok")
                        viewModel.emptyLiveDataForLoginResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error)
                viewModel.emptyLiveDataError()
            }
        })
    }

    private fun initViews() {

        btnSubmit = findViewById(R.id.btnForgotPasswordSubmit)
        imgBack = findViewById(R.id.imgForgotBack)
    }

    private fun initListeners() {
        btnSubmit!!.setOnClickListener {
            if(viewModel.validateForgtoPasswordData())
                viewModel.forgotPasswordEmail()
            else
                AppUtils.showSnackBar(this, ResponseMessages.SOME_THING_WENT_WRONG.message)
        }

        imgBack!!.setOnClickListener { onBackPressed() }
    }
}