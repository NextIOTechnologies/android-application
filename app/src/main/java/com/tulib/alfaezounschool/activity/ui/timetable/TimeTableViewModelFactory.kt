package com.tulib.alfaezounschool.activity.ui.timetable

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.ITimeTableRepository



@Suppress("UNCHECKED_CAST")
class TimeTableViewModelFactory(private var timeTableRepository: ITimeTableRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return TimeTableViewModel(timeTableRepository) as T
    }
}