package com.tulib.alfaezounschool.activity.ui.login

import android.content.ContentValues.TAG
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.BaseResponse
import com.tulib.alfaezounschool.data.network.response.response_models.LoginResponse
import com.tulib.alfaezounschool.data.network.response.response_models.MainResponse
import com.tulib.alfaezounschool.data.repository.ILoginRepository
import com.tulib.alfaezounschool.utils.DataUtil
import kotlinx.coroutines.launch
import nye.health.data.network.api_call.session.ISessionManager



class LoginViewModel(private var loginRepository: ILoginRepository, private var sessionManager: ISessionManager) : ViewModel()
{
    private lateinit var liveDataForLoginResponse : MutableLiveData<Boolean>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>


    var liveDataUserName: MutableLiveData<String> = MutableLiveData()
    var liveDataPassword: MutableLiveData<String> = MutableLiveData()


    fun initializeLiveDataForLoginResponse(): LiveData<Boolean> {
        liveDataForLoginResponse = MutableLiveData()
        return liveDataForLoginResponse
    }
    fun emptyLiveDataForLoginResponse(){liveDataForLoginResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun validateLoginData()  = (liveDataUserName.value?:"").length>0 && (liveDataPassword.value?:"").length>0


    fun login(device_token:String) {


        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = loginRepository.login(
                    DataUtil.getLoginJson(liveDataUserName.value?.trim() ?: "",
                            liveDataPassword.value?.trim() ?: "",device_token?.trim() ?: ""))
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<LoginResponse>
                    val user = data.data.profile
                    user.token = data.data.token

                    sessionManager.saveUser(user)
                    liveDataForLoginResponse.value = true

                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
