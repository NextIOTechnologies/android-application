package com.tulib.alfaezounschool.activity.ui.forgot_password.forgot_password_email

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.BaseResponse
import com.tulib.alfaezounschool.data.network.response.response_models.MainResponse
import com.tulib.alfaezounschool.data.network.response.response_models.MessageResponse
import com.tulib.alfaezounschool.data.repository.IForgotPasswordRepository
import com.tulib.alfaezounschool.model.ForgotPasswordSingletonModel
import com.tulib.alfaezounschool.utils.DataUtil
import kotlinx.coroutines.launch

class ForgotPasswordEmailViewModel(private var forgotPasswordRepository: IForgotPasswordRepository) : ViewModel()
{
    private lateinit var liveDataForForgotPasswordResponse : MutableLiveData<String>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>


    var liveDataEmail: MutableLiveData<String> = MutableLiveData()



    fun initializeLiveDataForForgotPasswordResponse(): LiveData<String> {
        liveDataForForgotPasswordResponse = MutableLiveData()
        return liveDataForForgotPasswordResponse
    }
    fun emptyLiveDataForLoginResponse(){liveDataForForgotPasswordResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun validateForgtoPasswordData()  = (liveDataEmail.value?:"").length>0


    fun forgotPasswordEmail() {
        viewModelScope.launch {
            ForgotPasswordSingletonModel.getModel().email = liveDataEmail.value?.trim() ?: ""
            liveDataForLoader.value = true
            val authResponse = forgotPasswordRepository.forgotPasswordEmail(
                    DataUtil.getForgotPasswordEmailJson(liveDataEmail.value?.trim() ?: ""))
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<MessageResponse>
                    liveDataForForgotPasswordResponse.value = data.data.message
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
