package com.tulib.alfaezounschool.activity.ui.announcements.holiday

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IHolidayRepository
import kotlinx.coroutines.launch

class HolidayViewModel(private var holidayRepository: IHolidayRepository) : ViewModel()
{
    private lateinit var liveDataForHolidayResponse : MutableLiveData<ArrayList<HolidayModel>>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

    fun initializeLiveDataForHolidayResponse(): LiveData<ArrayList<HolidayModel>> {
        liveDataForHolidayResponse = MutableLiveData()
        return liveDataForHolidayResponse
    }
    fun emptyLiveDataForHolidayResponse(){liveDataForHolidayResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getHolidayList() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = holidayRepository.holidays()
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<HolidayList>
                    if(data.data.holidayList.size==0)
                    {
//                        liveDataError.value = "No Holiday Announcements Available"
                    }
                    else liveDataForHolidayResponse.value = data.data.holidayList
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
