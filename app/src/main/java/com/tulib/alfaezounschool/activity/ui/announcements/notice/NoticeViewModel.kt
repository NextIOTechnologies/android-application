package com.tulib.alfaezounschool.activity.ui.announcements.notice

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.INoticeRepository
import kotlinx.coroutines.launch

class NoticeViewModel(private var noticeRepository: INoticeRepository) : ViewModel()
{
    private lateinit var liveDataForNoticeResponse : MutableLiveData<ArrayList<NoticeModel>>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

    fun initializeLiveDataForNoticeResponse(): LiveData<ArrayList<NoticeModel>> {
        liveDataForNoticeResponse = MutableLiveData()
        return liveDataForNoticeResponse
    }
    fun emptyLiveDataForNoticeResponse(){liveDataForNoticeResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getNoticeList() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = noticeRepository.notices()
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<NoticeList>
                    if(data.data.noticeList.size==0)
                    {
//                        liveDataError.value = "No Notices Available"
                    }
                    else liveDataForNoticeResponse.value = data.data.noticeList
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
