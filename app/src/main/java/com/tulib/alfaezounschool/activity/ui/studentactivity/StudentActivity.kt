package com.tulib.alfaezounschool.activity.ui.studentactivity

import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.adapter.StudentActivityAdapter
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.StudentActivityModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance
import java.util.*

class StudentActivity : BaseActivity() {
    private val studentActivityViewModelFactory : StudentActivityViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: StudentActivityViewModel


    var recyclerViewStudentActivity: RecyclerView? = null
    var studentActivityAdapter: StudentActivityAdapter? = null
    var studentActivityModelArrayList: ArrayList<StudentActivityModel>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutInflater.inflate(R.layout.activity_student, frameLayout)
        initViews()
        initListeners()
        viewModel = ViewModelProvider(this, studentActivityViewModelFactory).get(StudentActivityViewModel::class.java)

        viewModel.initializeLiveDataForLoader().observe(this,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(this) else loaderDialog.hideLoader(this)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForStudentActivityResponse().observe(this,
                { result ->
                    if(result!=null)
                    {
                        studentActivityAdapter?.setData(result)
                        viewModel.emptyLiveDataForNotificationResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(this, { error ->
            error?.let {
                AppUtils.showSnackBar(this,error)
                viewModel.emptyLiveDataError()
            }
        })
        AppController.studentid?.let {
            viewModel.getStudentActivityList(it
            )
        }
    }

    private fun initViews() {
        recyclerViewStudentActivity = findViewById(R.id.recyclerStudentActivity)
        studentActivityModelArrayList = ArrayList()

        studentActivityAdapter = StudentActivityAdapter(this@StudentActivity, studentActivityModelArrayList)
        recyclerViewStudentActivity?.setLayoutManager(LinearLayoutManager(this))
        recyclerViewStudentActivity?.setAdapter(studentActivityAdapter)
    }

    private fun initListeners() {
        imgMenu!!.setOnClickListener { drawerLayout!!.openDrawer(GravityCompat.START) }
    }


}