package com.tulib.alfaezounschool.activity.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.ILoginRepository
import nye.health.data.network.api_call.session.ISessionManager

@Suppress("UNCHECKED_CAST")
class LoginViewModelFactory(var loginRepository: ILoginRepository, var sessionManager: ISessionManager) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return LoginViewModel(loginRepository,sessionManager) as T
    }
}