package com.tulib.alfaezounschool.activity.ui.students

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IStudentsRepository

import kotlinx.coroutines.launch

class StudentsViewModel(private var studentsRepository: IStudentsRepository) : ViewModel()
{
    private lateinit var liveDataForStudentResponse : MutableLiveData<StudentList>

    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>


    val studentID: String=""
    val name: String=""
    val className: String=""
    val roll: String=""
    val phone: String=""
    val photo: String=""

    fun initializeliveDataForStudentResponse(): LiveData<StudentList> {
        liveDataForStudentResponse = MutableLiveData()
        return liveDataForStudentResponse
    }
    fun emptyliveDataForStudentResponse(){liveDataForStudentResponse.value= null}



    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}


    fun getStudents() {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = studentsRepository.getStudents()
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<StudentList>
                        liveDataForStudentResponse.value = data.data

                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
