package com.tulib.alfaezounschool.activity.ui.notification

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.adapter.NotificationAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.R
import androidx.recyclerview.widget.LinearLayoutManager
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.NotificationModel
import com.tulib.alfaezounschool.dialog.LoaderDialog
import com.tulib.alfaezounschool.utils.AppUtils
import org.kodein.di.generic.instance
import java.util.ArrayList

/**
 * A simple [Fragment] subclass.
 * Use the [NotificationFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NotificationFragment : Fragment() {
    private val notificationViewModelFactory : NotificationViewModelFactory by AppController.kodein().instance()
    private val loaderDialog : LoaderDialog by AppController.kodein().instance()
    private lateinit var viewModel: NotificationViewModel

    var recyclerViewNotificaion: RecyclerView? = null
    var notificationAdapter: NotificationAdapter? = null
    var notificationModelArrayList: ArrayList<NotificationModel>? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View?
    {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_notification, container, false)
        viewModel = ViewModelProvider(this, notificationViewModelFactory).get(NotificationViewModel::class.java)
        initViews(v)
        return v
    }

    private fun initViews(v: View)
    {
        recyclerViewNotificaion = v.findViewById(R.id.recyclerNotification)
        notificationModelArrayList = ArrayList<NotificationModel>()
        notificationAdapter = NotificationAdapter(activity as Context, notificationModelArrayList)
        recyclerViewNotificaion?.setLayoutManager(LinearLayoutManager(activity))
        recyclerViewNotificaion?.setAdapter(notificationAdapter)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        viewModel.initializeLiveDataForLoader().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        if(result)loaderDialog.showLoader(requireActivity() as AppCompatActivity) else loaderDialog.hideLoader(requireActivity() as AppCompatActivity)
                        viewModel.emptyLiveDataForLoader()
                    }
                })
        // Redirecting to next destination
        viewModel.initializeLiveDataForNotificationResponse().observe(viewLifecycleOwner,
                { result ->
                    if(result!=null)
                    {
                        notificationAdapter?.setData(result)
                        viewModel.emptyLiveDataForNotificationResponse()
                    }
                })

        // Showing error in bottom red popup
        viewModel.initializeLiveDataGenericError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error.message)
                viewModel.emptyLiveDataGenericError()
            }
        })
        // Showing error in bottom red popup
        viewModel.initializeLiveDataError().observe(viewLifecycleOwner, { error ->
            error?.let {
                AppUtils.showSnackBar(requireActivity() as AppCompatActivity,error)
                viewModel.emptyLiveDataError()
            }
        })

        viewModel.getNotificationList()
    }

}