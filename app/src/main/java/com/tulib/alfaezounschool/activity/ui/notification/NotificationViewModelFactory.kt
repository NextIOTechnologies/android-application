package com.tulib.alfaezounschool.activity.ui.notification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tulib.alfaezounschool.data.repository.INotificationRepository


@Suppress("UNCHECKED_CAST")
class NotificationViewModelFactory(private var notificationRepository: INotificationRepository) : ViewModelProvider.NewInstanceFactory()
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return NotificationViewModel(notificationRepository) as T
    }
}