package com.tulib.alfaezounschool.activity.ui.studentactivity

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.data.repository.IStudentActivityRepository
import kotlinx.coroutines.launch

class StudentActivityViewModel(private var studentActivityRepository: IStudentActivityRepository) : ViewModel()
{
    private lateinit var liveDataForStudentActivityResponse : MutableLiveData<ArrayList<StudentActivityModel>>
    private lateinit var liveDataForLoader : MutableLiveData<Boolean>
    private lateinit var liveDataGenericError :MutableLiveData<BaseResponse>
    private lateinit var liveDataError :MutableLiveData<String>

    fun initializeLiveDataForStudentActivityResponse(): LiveData<ArrayList<StudentActivityModel>> {
        liveDataForStudentActivityResponse = MutableLiveData()
        return liveDataForStudentActivityResponse
    }
    fun emptyLiveDataForNotificationResponse(){liveDataForStudentActivityResponse.value= null}

    fun initializeLiveDataForLoader(): LiveData<Boolean> {
        liveDataForLoader = MutableLiveData()
        return liveDataForLoader
    }
    fun emptyLiveDataForLoader(){liveDataForLoader.value= null}


    fun initializeLiveDataGenericError(): LiveData<BaseResponse> {
        liveDataGenericError = MutableLiveData()
        return liveDataGenericError
    }
    fun emptyLiveDataGenericError() {liveDataGenericError.value = null}


    fun initializeLiveDataError(): LiveData<String> {
        liveDataError = MutableLiveData()
        return liveDataError
    }
    fun emptyLiveDataError() {liveDataError.value = null}

    fun getStudentActivityList(id:String) {
        viewModelScope.launch {
            liveDataForLoader.value = true
            val authResponse = studentActivityRepository.studentActivityList(id)
            liveDataForLoader.value = false
            when (authResponse) {
                is Resource.Success<*> -> {
                    val data = authResponse.value as MainResponse<StudentActivityList>
                    if(data.data.studentActivityList.size==0)
                    {
                        liveDataError.value = "No Studentactivity Available"
                    }
                    else liveDataForStudentActivityResponse.value = data.data.studentActivityList
                }
                is Resource.Error -> liveDataError.value = authResponse.value
                is Resource.GenericError ->
                    liveDataGenericError.value = authResponse.value
            }
        }
    }
}
