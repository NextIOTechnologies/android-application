package com.tulib.alfaezounschool.data.network.response.response_models


import com.google.gson.annotations.SerializedName
import com.tulib.alfaezounschool.model.User

data class CommonResponse(@SerializedName("profile") val profile: User,
                          @SerializedName("totalUsersCounts") val userscount: UsersCount)