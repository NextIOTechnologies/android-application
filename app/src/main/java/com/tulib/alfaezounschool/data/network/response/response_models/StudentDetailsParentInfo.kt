package com.tulib.alfaezounschool.data.network.response.response_models


data class StudentDetailsParentInfo(
    val address: String,
    val email: String,
    val fatherName: String,
    val fatherProfession: String,
    val motherName: String,
    val motherProfession: String,
    val name: String,
    val phone: String,
    val photo: String,
    val username: String
)