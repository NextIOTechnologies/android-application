package com.tulib.alfaezounschool.data.network.response.response_models


data class HolidayModel(
    val details: String,
    val fromDate: String,
    val holidayID: String,
    val title: String,
    val toDate: String
)