package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface

interface ICommonRepository
{
    suspend  fun commonData() : Resource
}

class CommonRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : ICommonRepository
{
   override suspend   fun  commonData() = responseMiddleware.networkCall {
       apiManager.commonData()
   }


}