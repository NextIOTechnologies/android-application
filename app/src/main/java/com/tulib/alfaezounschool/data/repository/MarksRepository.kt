package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface

interface IMarksRepository
{
    suspend  fun marks(id:String) : Resource
}

class MarksRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IMarksRepository
{
   override suspend   fun  marks(id:String) = responseMiddleware.networkCall {
       apiManager.marks(id)
   }


}