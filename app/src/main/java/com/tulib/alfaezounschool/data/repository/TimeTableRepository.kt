package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface


interface ITimeTableRepository
{
    suspend  fun timetableList(id:String) : Resource
}

class TimeTableRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : ITimeTableRepository
{
   override suspend   fun  timetableList(id:String) = responseMiddleware.networkCall {
       apiManager.timeTable(id)
   }

}