package nye.health.data.network.api_call.session

import com.tulib.alfaezounschool.data.db.database.DatabaseInterface
import com.tulib.alfaezounschool.model.User


interface ISessionManager
{
    suspend fun cleanUserData()
    suspend fun getToken():String?
    suspend fun saveUser(user: User)
    suspend fun isLogin():Boolean
}

class SessionManager(var databaseInterface: DatabaseInterface): ISessionManager
{
    override suspend fun cleanUserData() = databaseInterface.deleteUser()

    override suspend fun getToken()= databaseInterface.getUser()?.token
    override suspend fun saveUser(user: User) { databaseInterface.insert(user) }

    override suspend fun isLogin() = databaseInterface.getUser() !=null
}