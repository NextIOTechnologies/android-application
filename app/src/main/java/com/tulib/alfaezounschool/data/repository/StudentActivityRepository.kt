package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface



interface IStudentActivityRepository
{
    suspend  fun studentActivityList(id:String) : Resource
}

class StudentActivityRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IStudentActivityRepository
{
   override suspend   fun  studentActivityList(id:String) = responseMiddleware.networkCall {
       apiManager.studentActivityList(id)
   }


}