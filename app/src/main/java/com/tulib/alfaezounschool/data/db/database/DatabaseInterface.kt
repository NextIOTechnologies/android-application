package com.tulib.alfaezounschool.data.db.database


import com.tulib.alfaezounschool.model.User


interface DatabaseInterface {
    suspend  fun getUser(): User?
    suspend fun insert(user : User)
    suspend fun deleteUser()
    suspend fun editUser(user: User?)
}