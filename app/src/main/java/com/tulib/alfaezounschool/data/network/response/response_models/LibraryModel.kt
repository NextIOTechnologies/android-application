package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName


data class LibraryModel(
    val ebooksID: String,
    val name: String,
    val author: String,
    @SerializedName("cover_photo")
    val coverPhoto: String,
    val file: String,
    val `class`: String
)