package com.tulib.alfaezounschool.data.db.database


import androidx.room.Database
import androidx.room.RoomDatabase
import com.tulib.alfaezounschool.data.db.dao.UserDao
import com.tulib.alfaezounschool.model.User
import com.tulib.alfaezounschool.utils.AppConstants.DATABASE_VERSION

@Database(entities = [User::class], version = DATABASE_VERSION,exportSchema = false)

abstract class AppDatabase : RoomDatabase() {

    abstract fun getUserDao() : UserDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        private val LOCK = Any()

        operator fun invoke(database: DatabaseConfig) = instance ?: synchronized(LOCK)
        {
            instance ?: database.getDatabase().also { instance = it }
        }

    }
}