package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface



interface INoticeRepository
{
    suspend  fun notices() : Resource
}

class NoticeRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : INoticeRepository
{
   override suspend   fun  notices() = responseMiddleware.networkCall {
       apiManager.notices()
   }


}