package com.tulib.alfaezounschool.data.network.response.response_models


import com.google.gson.annotations.SerializedName

data class HomeWorkModel(
    val assignmentID: String,
    @SerializedName("create_date")
    val createDate: String,
    val deadlinedate: String,
    val description: String,
    val `file`: String,
    val subject: String,
    val title: String
)