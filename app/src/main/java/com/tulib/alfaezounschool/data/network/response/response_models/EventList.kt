package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class EventList(@SerializedName("events") val eventList: ArrayList<EventModel>)