package nye.health.data.network.api_call.middle_ware

import android.util.Log
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.network.response.ResponseCodes
import com.tulib.alfaezounschool.data.network.response.ResponseMessages
import com.tulib.alfaezounschool.data.network.response.response_models.BaseResponse
import com.tulib.alfaezounschool.utils.AppConstants
import com.google.gson.Gson
import nye.health.data.network.api_call.api_request.ApiRequest
import nye.health.data.network.api_call.network_handler.NetworkAvailableInterface
import retrofit2.Response
import java.io.IOException

interface ResponseMiddlewareInterface
{
    suspend fun <T> networkCallBackground(call: suspend ()-> Response<T>): Resource
    suspend fun <T> networkCall(showLoader: Boolean = true, call: suspend ()-> Response<T>): Resource
    suspend fun <T> parseApiResponse(response: Response<T>?): Resource
}

class ResponseMiddleware(
        var networkAvailableInterface: NetworkAvailableInterface,
    ) : ApiRequest(), ResponseMiddlewareInterface
{
    override suspend fun <T> networkCallBackground(call: suspend ()-> Response<T>)= networkCall(showLoader = false, call)

    override suspend fun <T> networkCall(showLoader: Boolean, call: suspend ()-> Response<T>): Resource
     {
              if(networkAvailableInterface.isInternetIsAvailable())
              {
                  var response :Response<T>? = null
                  try {
                      response= apiRequest { call() }
                     }
                      catch (e: IOException)
                      {
                          Log.d("TAG", "networkCalxxxl: "+e)
                          return    Resource.Error(ResponseMessages.SESSION_TIMEOUT.message)
                      }
                      catch (e: Exception)
                      {
                          Log.d("TAG", "networkCalxxxl: "+e)
                          return   Resource.Error(ResponseMessages.SOME_THING_WENT_WRONG.message)
                      }
                     return parseApiResponse(response)
              }
              else return Resource.Error(AppConstants.INTERNET_ERROR)
     }

    override suspend fun <T> parseApiResponse(response: Response<T>?): Resource
    {
        response?.let {
            return when(response.code()) {
                ResponseCodes.SUCCESS_CODE_200.code -> Resource.Success(response.body() as T)
                // 400 Bad request - any parameter is missing or wrong
                ResponseCodes.ERROR_CODE_500.code ->  Resource.Error(ResponseMessages.SOME_THING_WENT_WRONG.message)
                else -> {
                    try {
                        val baseResponse = Gson().fromJson(
                                response.errorBody()?.charStream(),
                                BaseResponse::class.java)
                        Resource.GenericError(baseResponse)
                    }
                    catch (e:Exception)
                    {
                        Resource.Error(ResponseMessages.SOME_THING_WENT_WRONG.message)
                    }

                }
            }
        }?: kotlin.run {
            return Resource.Error(ResponseMessages.SOME_THING_WENT_WRONG.message)
        }
    }
}