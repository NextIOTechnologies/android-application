package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface



interface ILogoutRepository
{
    suspend  fun logout() : Resource
}

class LogoutRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : ILogoutRepository
{
   override suspend   fun  logout() = responseMiddleware.networkCall {
       apiManager.logout()
   }


}