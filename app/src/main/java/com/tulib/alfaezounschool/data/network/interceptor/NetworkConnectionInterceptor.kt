package com.tulib.alfaezounschool.data.network.interceptor

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import nye.health.data.network.api_call.session.ISessionManager
import okhttp3.Interceptor
import okhttp3.Response

class NetworkConnectionInterceptor(var sessionManagerInterface: ISessionManager) : Interceptor
{
    override fun intercept(chain: Interceptor.Chain): Response
    {
        var tokenAvailable = false
        var token = ""
        val job = GlobalScope.launch {
            tokenAvailable = sessionManagerInterface.isLogin()
            if (tokenAvailable) token = sessionManagerInterface.getToken()!!

        }
        runBlocking {
            job.join()
        }
        val originalRequest = chain.request()
        val request = originalRequest.newBuilder()
            .addHeader("Authorization",if(tokenAvailable) "Bearer "+token else "")
            // .method(originalRequest.method, originalRequest.body)
            .build()
        return chain.proceed(request)
    }
}