package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class ComplainList(@SerializedName("complains") val complains: ArrayList<ComplainModel>,
                        @SerializedName("usertypes") val usersList: ArrayList<UserTypeModel>
                            )