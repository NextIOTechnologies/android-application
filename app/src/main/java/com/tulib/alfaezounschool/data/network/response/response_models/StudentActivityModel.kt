package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName


data class StudentActivityModel(
        @SerializedName("student_name")
        val studentName: String,
        @SerializedName("teacher_name")
        val teacherName: String,
        val subject: String,
        val oral: String,
        val writing: String,
        val behaviour: String,
        val present: String,
        val participation: String,
        val note: String,
        val date: String
)