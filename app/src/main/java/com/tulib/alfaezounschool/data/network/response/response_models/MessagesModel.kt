package com.tulib.alfaezounschool.data.network.response.response_models


import com.google.gson.annotations.SerializedName

data class MessagesModel(
    @SerializedName("conversation_id")
    val conversationId: String,
    @SerializedName("create_date")
    val createDate: String,
    val name: String,
    val subject: String
)