package com.tulib.alfaezounschool.data.network.response.response_models



open  class BaseResponse
{
    val status : Boolean? = null
    val message : String? = null
}