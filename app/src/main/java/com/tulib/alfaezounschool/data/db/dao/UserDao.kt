package com.tulib.alfaezounschool.data.db.dao

import androidx.annotation.NonNull
import androidx.room.*
import com.tulib.alfaezounschool.model.User
import com.tulib.alfaezounschool.utils.AppConstants

@Dao
interface UserDao {
    @NonNull
    @Query("SELECT * FROM ${AppConstants.USER_TABLE} ORDER BY primaryId DESC LIMIT 1")
    suspend  fun getUser(): User?


    @NonNull
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user : User) : Long

    @NonNull
    @Query("DELETE FROM ${AppConstants.USER_TABLE}")
    suspend fun deleteUser()


    @Update
    suspend fun editUser(user: User?)
}