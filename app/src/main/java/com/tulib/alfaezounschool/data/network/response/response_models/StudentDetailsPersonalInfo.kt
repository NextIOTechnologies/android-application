package com.tulib.alfaezounschool.data.network.response.response_models


data class StudentDetailsPersonalInfo(
    val citizenshipNO: String,
    val dob: String?,
    val email: String,
    val gender: String,
    val hisHerOrder: String,
    val liveWithParents: String,
    val noOfSiblingsBrothers: String,
    val noOfSiblingsSisters: String,
    val parentsAlive: String,
    val passportNO: String,
    val phone: String,
    val scholarshipByName: String
)