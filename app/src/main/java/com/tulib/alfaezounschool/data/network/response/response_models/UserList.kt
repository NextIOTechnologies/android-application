package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class UserList(@SerializedName("users") val usersList: ArrayList<User>)