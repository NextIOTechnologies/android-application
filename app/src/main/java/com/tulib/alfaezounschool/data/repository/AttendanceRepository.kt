package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface

interface IAttendanceRepository
{
    suspend  fun attendanceList(id:String) : Resource
}

class AttendanceRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IAttendanceRepository
{
   override suspend   fun  attendanceList(id:String) = responseMiddleware.networkCall {
       apiManager.attendanceList(id)
   }

}