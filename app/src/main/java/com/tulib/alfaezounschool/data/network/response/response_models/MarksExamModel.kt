package com.tulib.alfaezounschool.data.network.response.response_models



data class MarksExamModel(
    val exam: String,
    val examID: String,
    val grade: String,
    val marks: List<MarksModel>
)