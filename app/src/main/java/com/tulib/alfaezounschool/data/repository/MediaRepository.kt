package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface

interface IMediaRepository
{
    suspend  fun mediaList(id: String) : Resource
}

class MediaRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IMediaRepository
{

    override suspend   fun  mediaList(id: String) = responseMiddleware.networkCall {
        apiManager.mediaList(id)
    }
}