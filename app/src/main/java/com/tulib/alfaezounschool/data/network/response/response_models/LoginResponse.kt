package com.tulib.alfaezounschool.data.network.response.response_models

import com.tulib.alfaezounschool.model.User

data class LoginResponse( var  token : String,var profile:User)
