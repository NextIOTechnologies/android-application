package com.tulib.alfaezounschool.data.network.response.response_models


data class ComplainModel(
    val attachment: Any,
    val complainID: String,
    val createDate: String,
    val description: String,
    val name: String,
    val title: String,
    val type: String
)