package nye.health.data.network.api_call.network_handler

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.annotation.RequiresApi
import com.tulib.alfaezounschool.app.AppController

interface NetworkAvailableInterface {
    fun isInternetIsAvailable():Boolean
}
class NetworkAvailable : NetworkAvailableInterface
{
    @RequiresApi(Build.VERSION_CODES.M)
    override  fun isInternetIsAvailable(): Boolean
    {
        val connectivityManager =
            AppController.applicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        connectivityManager.activeNetwork.also {
            it?.let {
                val networkCapabilities = connectivityManager.getNetworkCapabilities(it)
                if (networkCapabilities!=null && (networkCapabilities.hasTransport(
                        NetworkCapabilities.TRANSPORT_WIFI
                    ) || networkCapabilities.hasTransport(
                        NetworkCapabilities.TRANSPORT_CELLULAR))
                )
                    return true

            }?:run{ return false }
        }
        return false
    }
}