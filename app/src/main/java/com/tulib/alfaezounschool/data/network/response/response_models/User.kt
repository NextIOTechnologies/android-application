package com.tulib.alfaezounschool.data.network.response.response_models

data class User(
    val name: String,
    val userID: String
)