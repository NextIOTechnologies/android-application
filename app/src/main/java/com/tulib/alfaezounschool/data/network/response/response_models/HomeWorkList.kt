package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class HomeWorkList(@SerializedName("assignments") val homeWorkList: ArrayList<HomeWorkModel>,
                        @SerializedName("classesID") val classesID: String
                        )