package com.tulib.alfaezounschool.data.network.response.response_models


import com.google.gson.annotations.SerializedName

data class NoticeModel(
    @SerializedName("create_date")
    val createDate: String,
    val date: String,
    val notice: String,
    val noticeID: String,
    val title: String
)