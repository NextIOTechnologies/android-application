package com.tulib.alfaezounschool.data.network.response.response_models

data class UsersCount(
    val students: String,
    val teachers: String,
    val parents: String
)