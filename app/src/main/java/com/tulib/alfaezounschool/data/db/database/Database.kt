package com.tulib.alfaezounschool.data.db.database

import com.tulib.alfaezounschool.model.User


class Database( private val database : AppDatabase):DatabaseInterface {




    override suspend fun getUser() = database.getUserDao().getUser()


    override suspend fun insert(user: User)
    {
       database.getUserDao().insert(user)
    }

    override suspend fun deleteUser()
    {
        database.getUserDao().deleteUser()
    }

    override suspend fun editUser(user: User?)
    {
        database.getUserDao().editUser(user)
    }
}