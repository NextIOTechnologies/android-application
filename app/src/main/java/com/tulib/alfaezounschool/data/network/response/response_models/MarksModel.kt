package com.tulib.alfaezounschool.data.network.response.response_models


data class MarksModel(
    val highestMarks: String,
    val obtainedMarks: String,
    val subject: String
)