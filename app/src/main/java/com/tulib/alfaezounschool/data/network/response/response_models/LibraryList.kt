package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class LibraryList(@SerializedName("ebooks") val libraryList: ArrayList<LibraryModel>)