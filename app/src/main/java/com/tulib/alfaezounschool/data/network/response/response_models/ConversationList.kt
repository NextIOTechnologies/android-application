package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class ConversationList(@SerializedName("messages") val messages: ArrayList<ConversationModel>)