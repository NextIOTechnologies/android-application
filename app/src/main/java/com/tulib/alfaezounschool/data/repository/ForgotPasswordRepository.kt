package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.google.gson.JsonObject
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface


interface IForgotPasswordRepository
{
    suspend  fun forgotPasswordEmail(body: JsonObject) : Resource
    suspend  fun forgotPasswordVerificationCode(body: JsonObject) : Resource

}

class ForgotPasswordRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IForgotPasswordRepository
{
   override suspend   fun  forgotPasswordEmail(body: JsonObject) = responseMiddleware.networkCall {
       apiManager.forgotPasswordEmail(body)
   }

    override suspend   fun  forgotPasswordVerificationCode(body: JsonObject) = responseMiddleware.networkCall {
       apiManager.forgotPasswordVerificationCode(body)
   }




}