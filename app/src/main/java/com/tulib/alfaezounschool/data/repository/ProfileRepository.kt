package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface

interface IProfileRepository
{
    suspend  fun studentInfo(id:String) : Resource
}

class ProfileRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IProfileRepository
{
   override suspend   fun  studentInfo(id:String) = responseMiddleware.networkCall {
       apiManager.studentDetails(id)
   }


}