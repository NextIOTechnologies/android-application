package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class MessagesList(@SerializedName("conversations") val conversationList: ArrayList<MessagesModel>,
                        @SerializedName("usertypes") val usersList: ArrayList<UserTypeModel>
                            )