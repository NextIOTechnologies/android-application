package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface



interface IEventRepository
{
    suspend  fun events() : Resource
}

class EventRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IEventRepository
{
   override suspend   fun  events() = responseMiddleware.networkCall {
       apiManager.events()
   }


}