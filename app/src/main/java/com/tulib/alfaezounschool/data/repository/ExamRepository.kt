package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface



interface IExamRepository
{
    suspend  fun examList(id:String) : Resource
}

class ExamRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IExamRepository
{
   override suspend   fun  examList(id:String) = responseMiddleware.networkCall {
       apiManager.examList(id)
   }


}