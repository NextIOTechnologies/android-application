package com.tulib.alfaezounschool.data.network.response.response_models


import com.google.gson.annotations.SerializedName

data class ConversationModel(
    val attach: Any,
    @SerializedName("conversation_id")
    val conversationId: String,
    @SerializedName("create_date")
    val createDate: String,
    val msg: String,
    @SerializedName("msg_id")
    val msgId: String,
    val photo: String,
    val sender: String,
    @SerializedName("user_id")
    val userId: String,
    val usertypeID: String
)