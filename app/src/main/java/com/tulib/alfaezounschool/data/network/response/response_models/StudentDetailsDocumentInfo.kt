package com.tulib.alfaezounschool.data.network.response.response_models




data class StudentDetailsDocumentInfo (
    val documentID: String,
    val `file`: String,
    val title: String
)