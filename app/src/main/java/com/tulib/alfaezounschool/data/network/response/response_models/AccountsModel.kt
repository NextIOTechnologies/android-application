package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName


data class AccountsModel(
        val maininvoiceID: String,
        val name: String,
        val maininvoicedate: String,
        val classes: String,
        val section: String,

        @SerializedName("invoice_total")
        val invoiceTotal: String,

        @SerializedName("discount_total")
        val discountTotal: String,

        @SerializedName("invoice_paid")
        val invoicePaid: String,

        @SerializedName("invoice_balance")
        val invoiceBalance: String,

        val status: String
)