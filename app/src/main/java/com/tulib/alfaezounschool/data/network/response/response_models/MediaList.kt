package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class MediaList(@SerializedName("files") val filesList: ArrayList<MediaFilesModel>)