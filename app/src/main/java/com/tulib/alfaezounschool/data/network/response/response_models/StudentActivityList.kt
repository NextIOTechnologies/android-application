package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class StudentActivityList(@SerializedName("studentactivities") val studentActivityList: ArrayList<StudentActivityModel>)