package com.tulib.alfaezounschool.data.network.response.response_models


data class NotificationModel(
    val date: String,
    val description: String,
    val title: String,
    val typeID: String,
    val typeParent: String
)