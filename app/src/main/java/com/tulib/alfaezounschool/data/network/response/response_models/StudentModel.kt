package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class StudentModel(
    val studentID: String,
    val name: String,
    @SerializedName("class")
    val className: String,
    val roll: String,
    val phone: String,
    val photo: String
)