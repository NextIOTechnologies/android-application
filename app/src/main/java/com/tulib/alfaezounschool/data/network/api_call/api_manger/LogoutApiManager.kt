package com.tulib.alfaezounschool.data.network.api_call.api_manger

import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.tulib.alfaezounschool.data.repository.ILogoutRepository
import nye.health.data.network.api_call.session.ISessionManager

interface ILogoutApiManager
{
    suspend fun logout(): Resource
}
class LogoutApiManager(private var logoutRepository: ILogoutRepository, private var sessionManagerInterface: ISessionManager): ILogoutApiManager {

    override suspend fun logout():Resource
    {
        val response =    logoutRepository.logout()
         when(response)
              {
                  is Resource.Success<*>->
                  {
                      sessionManagerInterface.cleanUserData()
                  }

              }
        return response
    }
}