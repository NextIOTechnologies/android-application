package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface

interface IHolidayRepository
{
    suspend  fun holidays() : Resource
}

class HolidayRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IHolidayRepository
{
   override suspend   fun  holidays() = responseMiddleware.networkCall {
       apiManager.holidays()
   }


}