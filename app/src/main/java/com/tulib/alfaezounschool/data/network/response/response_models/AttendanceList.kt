package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class AttendanceList(@SerializedName("attendance") val attendanceList: ArrayList<AttendanceModel>,

                          @SerializedName("totalcount") val totalcount: AttendanceTotalCount
                          )