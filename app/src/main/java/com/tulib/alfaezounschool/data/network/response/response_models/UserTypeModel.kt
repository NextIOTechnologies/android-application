package com.tulib.alfaezounschool.data.network.response.response_models


data class UserTypeModel(
    val usertype: String,
    val usertypeID: String
)