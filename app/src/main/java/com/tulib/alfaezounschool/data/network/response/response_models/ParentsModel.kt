package com.tulib.alfaezounschool.data.network.response.response_models


data class ParentsModel(
    val parentID: String,
    val name: String,
    val email: String,
    val phone: String,
    val photo: String
)