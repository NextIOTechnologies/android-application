package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.google.gson.JsonObject
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface



interface IChangePasswordRepository
{
    suspend  fun changePassword(body: JsonObject) : Resource
    suspend  fun forgotPassword(body: JsonObject) : Resource
}

class ChangePasswordRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IChangePasswordRepository
{
   override suspend   fun  changePassword(body: JsonObject) = responseMiddleware.networkCall {
       apiManager.changePassword(body)
   }

    override suspend   fun  forgotPassword(body: JsonObject) = responseMiddleware.networkCall {
        apiManager.forgotPassword(body)
    }


}