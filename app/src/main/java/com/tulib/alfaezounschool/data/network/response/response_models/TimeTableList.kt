package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class TimeTableList(@SerializedName( "SUNDAY") val sundayList: ArrayList<TimeTableModel>?=null,
                         @SerializedName( "MONDAY") val mondayList: ArrayList<TimeTableModel>?=null,
                         @SerializedName( "TUESDAY") val tuesdayList: ArrayList<TimeTableModel>?=null,
                         @SerializedName( "WEDNESDAY") val wednesdayList: ArrayList<TimeTableModel>?=null,
                         @SerializedName( "THURSDAY") val thursdayList: ArrayList<TimeTableModel>?=null,
                         @SerializedName( "FRIDAY") val fridayList: ArrayList<TimeTableModel>?=null,
                         @SerializedName( "SATURDAY") val saturdayList: ArrayList<TimeTableModel>?=null,
                         )