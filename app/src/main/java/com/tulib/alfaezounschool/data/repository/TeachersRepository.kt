package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.google.gson.JsonObject
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface

interface ITeachersRepository
{
    suspend  fun getTeachers() : Resource
}

class TeachersRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : ITeachersRepository
{

    override suspend   fun  getTeachers() = responseMiddleware.networkCall {
        apiManager.teachersList()
    }
}