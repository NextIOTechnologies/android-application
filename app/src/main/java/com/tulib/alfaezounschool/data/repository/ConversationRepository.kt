package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.google.gson.JsonObject
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface

interface IConversationRepository
{
    suspend  fun conversations() : Resource
    suspend  fun conversationsView(id:String) : Resource
    suspend  fun sendNewMessage(body: JsonObject) : Resource
    suspend  fun sendMessage(body: JsonObject) : Resource
    suspend  fun getMessageUserList(id: String) : Resource
}

class ConversationRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IConversationRepository
{
   override suspend   fun  conversations() = responseMiddleware.networkCall {
       apiManager.conversations()
   }

    override suspend   fun  conversationsView(id:String) = responseMiddleware.networkCall {
        apiManager.conversationsView(id)
    }
    override suspend   fun  sendNewMessage(body: JsonObject) = responseMiddleware.networkCall {
        apiManager.sendNewMessage(body)
    }

    override suspend   fun  sendMessage(body: JsonObject) = responseMiddleware.networkCall {
        apiManager.sendMessage(body)
    }

    override suspend   fun  getMessageUserList(id: String) = responseMiddleware.networkCall {
        apiManager.messageUsersList(id)
    }
}