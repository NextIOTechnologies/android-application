package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface



interface IAccountsRepository
{
    suspend  fun accountsList() : Resource
}

class AccountsRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IAccountsRepository
{
   override suspend   fun  accountsList() = responseMiddleware.networkCall {
       apiManager.accountsList()
   }


}