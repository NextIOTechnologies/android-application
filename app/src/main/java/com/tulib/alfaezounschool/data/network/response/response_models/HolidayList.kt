package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class HolidayList(@SerializedName("holidays") val holidayList: ArrayList<HolidayModel>)