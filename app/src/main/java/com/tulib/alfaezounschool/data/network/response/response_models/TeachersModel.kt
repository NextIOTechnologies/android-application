package com.tulib.alfaezounschool.data.network.response.response_models


data class TeachersModel(
    val teacherID: String,
    val name: String,
    val email: String,
    val phone: String,
    val photo: String
)