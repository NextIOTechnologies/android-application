package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.google.gson.JsonObject
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface

interface IComplainRepository
{
    suspend  fun complains() : Resource
    suspend  fun sendComplain(body: JsonObject) : Resource
    suspend  fun getComplainUsers(id:String) : Resource
}

class ComplainRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IComplainRepository
{
   override suspend   fun  complains() = responseMiddleware.networkCall {
       apiManager.complains()
   }


    override suspend   fun  sendComplain(body: JsonObject) = responseMiddleware.networkCall {
        apiManager.sendComplain(body)
    }

    override suspend   fun  getComplainUsers(id: String) = responseMiddleware.networkCall {
        apiManager.complainUsersList(id)
    }


}