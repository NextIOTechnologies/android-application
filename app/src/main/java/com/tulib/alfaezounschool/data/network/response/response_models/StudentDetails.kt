package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName


data class StudentDetails(@SerializedName("student_info") val student_info: StudentDetailsInfo,
                          @SerializedName("personal_info") val personal_info: StudentDetailsPersonalInfo,
                         @SerializedName("parents") val parents: StudentDetailsParentInfo,
                        @SerializedName("documents") val documents: ArrayList<StudentDetailsDocumentInfo>)