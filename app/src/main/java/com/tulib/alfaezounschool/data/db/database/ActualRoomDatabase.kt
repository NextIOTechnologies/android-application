package com.tulib.alfaezounschool.data.db.database

import android.content.Context
import androidx.room.Room
import com.tulib.alfaezounschool.utils.AppConstants.DATABASE_NAME

interface DatabaseConfig {
    fun getDatabase():AppDatabase
}
class ActualRoomDatabase(var context: Context) :DatabaseConfig {

    override fun getDatabase(): AppDatabase {
        return Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                DATABASE_NAME)
                .build()
    }
}