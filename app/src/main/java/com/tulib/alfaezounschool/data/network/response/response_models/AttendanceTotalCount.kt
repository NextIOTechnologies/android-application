package com.tulib.alfaezounschool.data.network.response.response_models

data class AttendanceTotalCount(
    val totalabsent: Int,
    val totalholiday: Int,
    val totallate: Int,
    val totallatewithexcuse: Int,
    val totalleave: Int,
    val totalpresent: Int,
    val totalweekend: Int
)