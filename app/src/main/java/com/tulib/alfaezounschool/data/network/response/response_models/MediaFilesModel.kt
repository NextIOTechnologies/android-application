package com.tulib.alfaezounschool.data.network.response.response_models


data class MediaFilesModel(
        val id: String,
        val fileUrl: String,
        val type: String,
        val fileNameDisplay: String,
)