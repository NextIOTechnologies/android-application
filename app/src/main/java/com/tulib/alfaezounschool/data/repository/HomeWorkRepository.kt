package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface

interface IHomeWorkRepository
{
    suspend  fun homeWork(id:String) : Resource
}

class HomeWorkRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IHomeWorkRepository
{
   override suspend   fun  homeWork(id:String) = responseMiddleware.networkCall {
       apiManager.homeWork(id)
   }


}