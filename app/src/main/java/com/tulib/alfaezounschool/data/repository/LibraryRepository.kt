package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface



interface ILibraryRepository
{
    suspend  fun libraryList(id:String) : Resource
}

class LibraryRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : ILibraryRepository
{
   override suspend   fun  libraryList(id:String) = responseMiddleware.networkCall {
       apiManager.libraryList(id)
   }


}