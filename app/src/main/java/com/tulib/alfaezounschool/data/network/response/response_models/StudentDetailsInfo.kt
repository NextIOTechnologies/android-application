package com.tulib.alfaezounschool.data.network.response.response_models


import com.google.gson.annotations.SerializedName

data class StudentDetailsInfo(
    val admission: String,
    @SerializedName("class")
    val classX: String,
    val educationSystem: String,
    val firstName: String,
    val middleName: String,
    val lastName: String,
    val surname: String,
    val photo: String,
    val roll: String,
    val section: String
)