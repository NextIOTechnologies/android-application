package com.tulib.alfaezounschool.data.network.api_call.api_manger

import com.tulib.alfaezounschool.data.network.response.response_models.*
import com.tulib.alfaezounschool.utils.AppConstants.BASE_URL
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface ApiManager
{
    //1-  This api will call on  login screen and after signup
      @POST("Signin")
      suspend  fun userLogin(@Body  body: JsonObject) : Response<MainResponse<LoginResponse>>

    //2-  This api will call on  login screen and after signup
    @POST("Change_password")
    suspend  fun changePassword(@Body  body: JsonObject) : Response<MainResponse<MessageResponse>>

    //3-  This api will call on  login screen and after signup
    @POST("Signin/signout")
    suspend  fun logout() : Response<Void>

    //4-  This api will call on  login screen and after signup
    @GET("Examschedule/{id}")
    suspend  fun examList(@Path("id") id:String) : Response<MainResponse<ExamList>>

    //5-  This api will call on  login screen and after signup
    @GET("Notifications")
    suspend  fun notifications() : Response<MainResponse<NotificationList>>

    //6-  This api will call on  login screen and after signup
    @GET("Conversations")
    suspend  fun conversations() : Response<MainResponse<MessagesList>>

    //7-  This api will call on  login screen and after signup
    @GET("Conversations/view/{id}")
    suspend  fun conversationsView(@Path("id") id:String): Response<MainResponse<ConversationList>>


    //8-  This api will call on  login screen and after signup
    @POST("Reset")
    suspend  fun forgotPasswordEmail(@Body  body: JsonObject): Response<MainResponse<MessageResponse>>


    //9-  This api will call on  login screen and after signup
    @POST("Reset/checkotp")
    suspend  fun forgotPasswordVerificationCode(@Body  body: JsonObject): Response<MainResponse<MessageResponse>>

    //10-  This api will call on  login screen and after signup
    @POST("Reset/changepassword")
    suspend  fun forgotPassword(@Body  body: JsonObject): Response<MainResponse<MessageResponse>>

    //10-  This api will call on  login screen and after signup
    @GET("Assignment/{id}")
    suspend  fun homeWork(@Path("id") id:String): Response<MainResponse<HomeWorkList>>

    //11-  This api will call on  login screen and after signup
    @GET("Routine/{id}")
    suspend  fun timeTable(@Path("id") id:String): Response<MainResponse<TimeTableList>>

    //12-  This api will call on  login screen and after signup
    @GET("Notice")
    suspend  fun notices(): Response<MainResponse<NoticeList>>

    //13-  This api will call on  login screen and after signup
    @GET("Event")
    suspend  fun events(): Response<MainResponse<EventList>>

    //14-  This api will call on  login screen and after signup
    @GET("Holiday")
    suspend  fun holidays(): Response<MainResponse<HolidayList>>

    //15-  This api will call on  login screen and after signup
    @GET("Mark/view/{id}")
    suspend  fun marks(@Path("id") id:String): Response<MainResponse<MarksList>>

    //16-  This api will call on  login screen and after signup
    @POST("Conversations/send_new_message")
    suspend  fun sendNewMessage(@Body  body: JsonObject): Response<MainResponse<NewMessageSendResponse>>

    //17-  This api will call on  login screen and after signup
    @POST("Conversations/send_message")
    suspend  fun sendMessage(@Body  body: JsonObject): Response<BaseResponse>


    //18-  This api will call on  login screen and after signup
    @GET("Complain")
    suspend  fun complains(): Response<MainResponse<ComplainList>>


    //19-  This api will call on  login screen and after signup
    @POST("Complain/add")
    suspend  fun sendComplain(@Body  body: JsonObject): Response<BaseResponse>

    //20-  This api will call on  login screen and after signup
    @GET("Student/view/{id}")
    suspend  fun studentDetails(@Path("id") id:String): Response<MainResponse<StudentDetails>>


    //21-  This api will call on  login screen and after signup
    @GET("Studentactivities/{id}")
    suspend  fun studentActivityList(@Path("id") id:String) : Response<MainResponse<StudentActivityList>>

    //22-  This api will call on  login screen and after signup
    @GET("Invoice")
    suspend  fun accountsList() : Response<MainResponse<AccountsList>>

    //23-  This api will call on  login screen and after signup
    @GET("Library/{id}")
    suspend  fun libraryList(@Path("id") id:String) : Response<MainResponse<LibraryList>>


    //24-  This api will call on  login screen and after signup
    @GET("Complain/get_users/{id}")
    suspend  fun complainUsersList(@Path("id") id:String): Response<MainResponse<UserList>>

    //25-  This api will call on  login screen and after signup
    @GET("Conversations/get_users/{id}")
    suspend  fun messageUsersList(@Path("id") id:String): Response<MainResponse<UserList>>

    //26-  This api will call on  login screen and after signup
    @GET("Sattendance/{id}")
    suspend  fun attendanceList(@Path("id") id:String): Response<MainResponse<AttendanceList>>

    //27-  This api will call on  login screen and after signup
    @GET("Common")
    suspend  fun commonData(): Response<MainResponse<CommonResponse>>

    @GET("Student")
    suspend  fun studentList(): Response<MainResponse<StudentList>>

    @GET("Parents")
    suspend  fun parentsList(): Response<MainResponse<ParentsList>>

    @GET("Teacher")
    suspend  fun teachersList(): Response<MainResponse<TeachersList>>

    @GET("Media/{id}")
    suspend  fun mediaList(@Path("id") id:String): Response<MainResponse<MediaList>>


    companion object
   {
      operator  fun invoke(interceptor: Interceptor)  : ApiManager
      {
          val gson = GsonBuilder()
                  .setLenient()
                  .create()

          val okHttpeClient: OkHttpClient

              okHttpeClient  = OkHttpClient.Builder()
                  .addNetworkInterceptor(interceptor)
                  .callTimeout(100, TimeUnit.SECONDS)
                  .readTimeout(100, TimeUnit.SECONDS)
                  .connectTimeout(100, TimeUnit.SECONDS).build()

         return Retrofit.Builder()
                .client(okHttpeClient)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(ApiManager::class.java)
      }
   }
}