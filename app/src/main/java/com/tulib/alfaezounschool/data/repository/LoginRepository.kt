package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.google.gson.JsonObject
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface



interface ILoginRepository
{
    suspend  fun login(body: JsonObject) : Resource
}

class LoginRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : ILoginRepository
{
   override suspend   fun  login(body: JsonObject) = responseMiddleware.networkCall {
       apiManager.userLogin(body)
   }


}