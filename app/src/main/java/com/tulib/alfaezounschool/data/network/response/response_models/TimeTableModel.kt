package com.tulib.alfaezounschool.data.network.response.response_models



data class TimeTableModel(
    val classes: String,
    var day: String?,
    val section: String,
    val subject: String,
    val teacher: String,
    val time: String
)