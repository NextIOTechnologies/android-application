package com.tulib.alfaezounschool.data.network.response.response_models


data class EventModel(
    val details: String,
    val eventID: String,
    val fromDate: String,
    val title: String,
    val toDate: String
)