package com.tulib.alfaezounschool.data.network.api_call.resource

import com.tulib.alfaezounschool.data.network.response.response_models.BaseResponse


//data class Resource<out T>(val status: ResponseStatus, val responseData: T?=null,
//                           val message: String? = null)
//{
//    companion object {
//        // 200
//        fun <T> success(data: T?): Resource<T> {
//            return Resource(ResponseStatus.SUCCESS, data)
//        }
//        // 400
//        fun <T> genericError(data: T): Resource<T> {
//            return Resource(ResponseStatus.GENERIC_ERROR, data)
//        }
//
//        fun <T> networkError(): Resource<T> {
//            return Resource(ResponseStatus.NETWORK_ERROR)
//        }
//
//        fun <T> error(message: String): Resource<T> {
//            return Resource(ResponseStatus.ERROR,message = message)
//        }
//    }
//}


sealed class Resource
{
    class Success <T> (val value:T):Resource()
    class GenericError  (val value: BaseResponse):Resource()
    class NetworkError:Resource()
    class Error(val value:String):Resource()
}
//enum class ResponseStatus
//{
//    // 200
//    SUCCESS,
//    ERROR,
//    GENERIC_ERROR,
//    NETWORK_ERROR,
//}