package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import com.google.gson.JsonObject
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface

interface IParentsRepository
{
    suspend  fun getParents() : Resource
}

class ParentsRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : IParentsRepository
{

    override suspend   fun  getParents() = responseMiddleware.networkCall {
        apiManager.parentsList()
    }
}