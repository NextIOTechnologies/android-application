package com.tulib.alfaezounschool.data.network.response.response_models


data class ExamScheduleModel(
    val date: String,
    val exam: String,
    val examfrom: String,
    val examscheduleID: String,
    val examto: String,
    val room: String,
    val subject: String
)