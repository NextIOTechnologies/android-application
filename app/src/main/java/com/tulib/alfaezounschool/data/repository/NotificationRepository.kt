package com.tulib.alfaezounschool.data.repository

import com.tulib.alfaezounschool.data.network.api_call.api_manger.ApiManager
import com.tulib.alfaezounschool.data.network.api_call.resource.Resource
import nye.health.data.network.api_call.middle_ware.ResponseMiddlewareInterface



interface INotificationRepository
{
    suspend  fun notifications() : Resource
}

class NotificationRepository(private val responseMiddleware: ResponseMiddlewareInterface,
                     private val  apiManager : ApiManager
) : INotificationRepository
{
   override suspend   fun  notifications() = responseMiddleware.networkCall {
       apiManager.notifications()
   }


}