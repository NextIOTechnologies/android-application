package com.tulib.alfaezounschool.data.network.response.response_models

import com.google.gson.annotations.SerializedName

data class ExamList(@SerializedName("examschedules") val examList: ArrayList<ExamScheduleModel>)