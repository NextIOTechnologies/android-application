package com.tulib.alfaezounschool.adapter

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.model.InvoiceModel
import java.util.*

class AccountAdapter(var context: Context, invoiceModelArrayList: ArrayList<InvoiceModel>) : RecyclerView.Adapter<AccountAdapter.AccountViewHolder?>() {
    var invoiceModelArrayList: ArrayList<InvoiceModel>
    var status: String? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountViewHolder {
        val v: View = LayoutInflater.from(context).inflate(R.layout.row_account, parent, false)
        return AccountViewHolder(v)
    }

    override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {
        holder.txtStudentName.setText(invoiceModelArrayList[position].invoice_studentname)
        holder.txtDate.setText(invoiceModelArrayList[position].invoice_date)
        holder.txtTotal.setText(invoiceModelArrayList[position].invoice_total)
        holder.txtBalance.setText(invoiceModelArrayList[position].invoice_balance)
        holder.txtFeeStatus.setText(invoiceModelArrayList[position].invoice_feestatus)
        status = holder.txtFeeStatus.getText().toString()
        if (status == "Fully Paid") {
            holder.txtFeeStatus.setTextColor(Color.parseColor("#4CAF50"))
        } else if (status == "Partial Paid") {
            holder.txtFeeStatus.setTextColor(Color.parseColor("#FFC107"))
        } else if (status == "Not Paid") {
            holder.txtFeeStatus.setTextColor(Color.parseColor("#F32B1C"))
        }
        holder.txtView.setOnClickListener(View.OnClickListener { view -> AccountDialog(view.context, position) })
    }



    inner class AccountViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtStudentName: TextView
        var txtDate: TextView
        var txtTotal: TextView
        var txtBalance: TextView
        var txtFeeStatus: TextView
        var txtView: TextView

        init {
            txtStudentName = itemView.findViewById<TextView>(R.id.txtInvoiceStudentName)
            txtDate = itemView.findViewById<TextView>(R.id.txtInvoiceDate)
            txtTotal = itemView.findViewById<TextView>(R.id.txtInvoiceTotal)
            txtBalance = itemView.findViewById<TextView>(R.id.txtInvoiceBalance)
            txtFeeStatus = itemView.findViewById<TextView>(R.id.txtInvoiceFeeStatus)
            txtView = itemView.findViewById<TextView>(R.id.txtInvoiceView)
        }
    }

    private fun AccountDialog(context: Context, i: Int) {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.dialog_account)
        val imgClose: ImageView
        var linearDownload: LinearLayout
        imgClose = dialog.findViewById(R.id.imgDialogAccountClose)
        imgClose.setOnClickListener { dialog.dismiss() }
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }

    init {
        this.invoiceModelArrayList = invoiceModelArrayList
    }

    override fun getItemCount(): Int {
        return invoiceModelArrayList.size
    }
}