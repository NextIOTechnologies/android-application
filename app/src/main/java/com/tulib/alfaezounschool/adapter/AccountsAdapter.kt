package com.tulib.alfaezounschool.adapter

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.adapter.AccountsAdapter.AccountsViewHolder
import com.tulib.alfaezounschool.data.network.response.response_models.AccountsModel
import java.util.*

class AccountsAdapter(var context: Context, var accountsModelArrayList: ArrayList<AccountsModel>?=null) : RecyclerView.Adapter<AccountsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountsViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_account, parent, false)
        return AccountsViewHolder(v)
    }

    override fun onBindViewHolder(holder: AccountsViewHolder, position: Int) {
        accountsModelArrayList?.let {list->
            holder.txtStudentName.setText(list[position].name)
            holder.txtDate.setText(list[position].maininvoicedate)
            holder.txtTotal.setText(list[position].invoiceTotal)
            holder.txtBalance.setText(list[position].invoiceBalance)
            holder.txtStatus.setText(list[position].status)

            var status=list[position].status;

            if (status.equals("Fully Paid")) {
                holder.txtStatus.setTextColor(Color.parseColor("#4CAF50"));
            } else if (status.equals("Partially Paid")) {
                holder.txtStatus.setTextColor(Color.parseColor("#FFC107"));
            } else if (status.equals("Not Paid")) {
                holder.txtStatus.setTextColor(Color.parseColor("#F32B1C"));
            }

            holder.txtView.setOnClickListener { view -> AccountsDialog(view.context, position)



            }
        }

    }

    override fun getItemCount(): Int {
        return accountsModelArrayList?.size?:0
    }

    inner class AccountsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtStudentName: TextView
        var txtDate: TextView
        var txtTotal: TextView
        var txtBalance: TextView
        var txtStatus: TextView
        var txtView:TextView;

        init {
            txtStudentName = itemView.findViewById(R.id.txtInvoiceStudentName);
            txtDate = itemView.findViewById(R.id.txtInvoiceDate);
            txtTotal = itemView.findViewById(R.id.txtInvoiceTotal);
            txtBalance = itemView.findViewById(R.id.txtInvoiceBalance);
            txtStatus = itemView.findViewById(R.id.txtInvoiceFeeStatus);
            txtView = itemView.findViewById(R.id.txtInvoiceView);

        }
    }



    private fun AccountsDialog(context: Context, position: Int) {
        accountsModelArrayList?.let {list->
            val dialog = Dialog(context)
            dialog.setContentView(R.layout.dialog_account)
            dialog.findViewById<TextView>(R.id.txtDialogAccountName).text = list[position].name
            dialog.findViewById<TextView>(R.id.txtDialogAccountDate).text = list[position].maininvoicedate
            dialog.findViewById<TextView>(R.id.txtDialogAccountClass).text = list[position].classes
            dialog.findViewById<TextView>(R.id.txtDialogAccountTotal).text = list[position].invoiceTotal
            dialog.findViewById<TextView>(R.id.txtDialogAccountPaid).text = list[position].invoicePaid
            dialog.findViewById<TextView>(R.id.txtDialogAccountBalance).text = list[position].invoiceBalance

            var txtStatus= dialog.findViewById<TextView>(R.id.txtDialogAccountStatus);
            txtStatus.setText(list[position].status)


            var status=list[position].status;

            if (status.equals("Fully Paid")) {
                txtStatus.setTextColor(Color.parseColor("#4CAF50"));
            } else if (status.equals("Partially Paid")) {
                txtStatus.setTextColor(Color.parseColor("#FFC107"));
            } else if (status.equals("Not Paid")) {
                txtStatus.setTextColor(Color.parseColor("#F32B1C"));
            }


            val imgClose: ImageView
            val btnOk: Button
            val linearDownload: LinearLayout
            imgClose = dialog.findViewById(R.id.imgDialogAccountClose)
            imgClose.setOnClickListener { dialog.dismiss() }

            btnOk = dialog.findViewById(R.id.btnDialogStudentActivityOk)
            btnOk.setOnClickListener { dialog.dismiss() }


            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        }


    }

    fun setData( list: ArrayList<AccountsModel>)
    {
        this.accountsModelArrayList=list
        notifyDataSetChanged()
    }
}