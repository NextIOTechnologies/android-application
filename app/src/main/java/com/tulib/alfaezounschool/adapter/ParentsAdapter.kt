package com.tulib.alfaezounschool.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.data.network.response.response_models.ParentsModel
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class ParentsAdapter(var context: Context, var parentModelArrayList: ArrayList<ParentsModel>? = null) : RecyclerView.Adapter<ParentsAdapter.ParentsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentsViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_parent_list, parent, false)
        return ParentsViewHolder(v)
    }

    override fun onBindViewHolder(holder: ParentsViewHolder, position: Int) {
        parentModelArrayList?.let { list ->
            holder.textViewName.setText(list[position].name)
            holder.textViewEmail.setText(list[position].email)
            Picasso.get()
                    .load(list[position].photo)
                    .placeholder(R.drawable.pic)
                    .into(holder.imageView)
        }
    }


    override fun getItemCount(): Int {
        return parentModelArrayList?.size ?: 0
    }

    inner class ParentsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: CircleImageView
        var textViewName: TextView
        var textViewEmail: TextView
        var txtView: RelativeLayout

        init {
            imageView = itemView.findViewById(R.id.imgParentImage);
            textViewName = itemView.findViewById(R.id.txtParent);
            textViewEmail = itemView.findViewById(R.id.txtEmail);
            txtView = itemView.findViewById(R.id.relative_parentlist);
        }
    }


    fun setData(list: ArrayList<ParentsModel>) {
        this.parentModelArrayList = list
        notifyDataSetChanged()
    }
}