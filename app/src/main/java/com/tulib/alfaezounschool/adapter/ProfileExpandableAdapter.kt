package com.tulib.alfaezounschool.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.tulib.alfaezounschool.R
import kotlin.collections.HashMap
import kotlin.collections.LinkedHashMap


class ProfileExpandableAdapter(private var context: Context, // header titles
                               private var listDataHeader: List<String>?,
        // child data in format of header title, child title
                               private var listDataChild: LinkedHashMap<String, HashMap<String, String>>?) : BaseExpandableListAdapter() {
    override fun getChild(groupPosition: Int, childPosititon: Int): Pair<String, String>? {
        listDataHeader?.get(groupPosition)?.let { key->

            var hashMap = (listDataChild?.get(key) as HashMap)
            var title = hashMap.keys.toTypedArray()[childPosititon]
            var text = hashMap.get(title)
            return Pair(title, text!!)
        }?: kotlin.run {
            return null
        }
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int,
                              isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView


            val infalInflater = context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if(groupPosition == groupCount-1)
        convertView = infalInflater.inflate(R.layout.item_student_details2, null)
        else  convertView = infalInflater.inflate(R.layout.item_student_details, null)

        convertView?.findViewById<TextView>(R.id.tvTitle)?.text= if(groupPosition == groupCount-1) "Title" else getChild(groupPosition, childPosition)?.first
        convertView?.findViewById<TextView>(R.id.tvText)?.text= getChild(groupPosition, childPosition)?.second
        return convertView!!
    }

    fun setData(listDataHeader: List<String>?,
            // child data in format of header title, child title
                listDataChild: LinkedHashMap<String, HashMap<String, String>>?)
    {
        this.listDataChild = listDataChild
        this.listDataHeader = listDataHeader
        notifyDataSetChanged()
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        val key = listDataHeader?.get(groupPosition)

        return if(key!=null)listDataChild?.get(key)!!.size else 0
    }

    override fun getGroup(groupPosition: Int): String? {
        return listDataHeader?.get(groupPosition)
    }

    override fun getGroupCount(): Int {
        return listDataHeader?.size?:0
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean,
                              convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val headerTitle = getGroup(groupPosition)


            val infalInflater = context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


            convertView = infalInflater.inflate(R.layout.list_group, null)


               convertView?.findViewById<TextView>(R.id.tvHeader)?.text=headerTitle
        convertView?.findViewById<ImageView>(R.id.imgProfileAdd)?.setImageResource(if (isExpanded) R.drawable.minus else R.drawable.plus)


        return convertView!!
    }

    override fun hasStableIds(): Boolean { return false }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

//    fun downloadFile() {
//        val DownloadUrl: String = audio1
//        val request1 = DownloadManager.Request(Uri.parse(DownloadUrl))
//        request1.setDescription("Sample Music File") //appears the same in Notification bar while downloading
//        request1.setTitle("File1.mp3")
//        request1.setVisibleInDownloadsUi(false)
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//            request1.allowScanningByMediaScanner()
//            request1.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN)
//        }
//        request1.setDestinationInExternalFilesDir(ApplicationProvider.getApplicationContext<Context>(), "/File", "Question1.mp3")
//        val manager1 = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager?
//        Objects.requireNonNull(manager1).enqueue(request1)
//        if (DownloadManager.STATUS_SUCCESSFUL == 8)
//        {
//            DownloadSuccess()
//        }
//    }
}