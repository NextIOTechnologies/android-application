package com.tulib.alfaezounschool.adapter

import android.content.Context
import android.widget.BaseExpandableListAdapter
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.data.network.response.response_models.MarksExamModel
import com.tulib.alfaezounschool.data.network.response.response_models.MarksModel

class ExpandableAdapter(private var context: Context, // header titles
                        private var listDataHeader: List<MarksExamModel>?,
        // child data in format of header title, child title
                        private var listDataChild: LinkedHashMap<String, List<MarksModel>>?) : BaseExpandableListAdapter() {

 var totalHightestMarks  =0.0f
 var totalObtainedMarks  =0.0f
    override fun getChild(groupPosition: Int, childPosititon: Int): MarksModel? {
        listDataHeader?.get(groupPosition)?.let {
            val key = it.exam

            return listDataChild?.get(key)?.get(childPosititon)
        }?: kotlin.run {
            return null
        }
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int,
                              isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView


            val infalInflater = context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            if(childPosition==0)
            {
                totalHightestMarks =0.0f
                totalObtainedMarks =0.0f
                convertView = infalInflater.inflate(R.layout.list_item_one, null)
            }

            else if(childPosition==getChildrenCount(groupPosition)-3)
            {
                convertView = infalInflater.inflate(R.layout.item_total, null)
                convertView?.findViewById<TextView>(R.id.txtMarkTotal)?.text= totalHightestMarks.toString()
                convertView?.findViewById<TextView>(R.id.txtMarkObtained)?.text= totalObtainedMarks.toString()
            }


           else if(childPosition==getChildrenCount(groupPosition)-2)
            {
                convertView = infalInflater.inflate(R.layout.item_grade, null)
                convertView?.findViewById<TextView>(R.id.tvGrade)?.text= listDataHeader?.get(groupPosition)?.grade
            }

           else if(childPosition==getChildrenCount(groupPosition)-1)
                convertView = infalInflater.inflate(R.layout.item_teachers_remark, null)
            else
            {
                val markModel = getChild(groupPosition, childPosition-1)
                totalHightestMarks += markModel?.highestMarks?.toFloat()?:0.0.toFloat()
                totalObtainedMarks += markModel?.obtainedMarks?.toFloat()?:0.0.toFloat()
             convertView = infalInflater.inflate(R.layout.list_item_one, null)
                convertView?.findViewById<TextView>(R.id.tvSubject)?.text= markModel?.subject
                convertView?.findViewById<TextView>(R.id.tvObtainedMarks)?.text= markModel?.obtainedMarks
                convertView?.findViewById<TextView>(R.id.tvHighestMarks)?.text= markModel?.highestMarks
            }



        return convertView!!
    }

    fun setData(listDataHeader: List<MarksExamModel>?,
            // child data in format of header title, child title
                listDataChild: LinkedHashMap<String, List<MarksModel>>?)
    {
        this.listDataChild = listDataChild
        this.listDataHeader = listDataHeader
        notifyDataSetChanged()
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        val key = listDataHeader?.get(groupPosition)?.exam

        return if(key!=null)listDataChild?.get(key)!!.size+4 else 0
    }

    override fun getGroup(groupPosition: Int): MarksExamModel? {
        return listDataHeader?.get(groupPosition)
    }

    override fun getGroupCount(): Int {
        return listDataHeader?.size?:0
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean,
                              convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val headerTitle = getGroup(groupPosition)
        if (convertView == null) {
            val infalInflater = context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = infalInflater.inflate(R.layout.list_group, null)
        }
        convertView?.findViewById<TextView>(R.id.tvHeader)?.text=headerTitle?.exam
        convertView?.findViewById<ImageView>(R.id.imgProfileAdd)?.setImageResource(if(isExpanded) R.drawable.minus else R.drawable.plus )


        return convertView!!
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }
}