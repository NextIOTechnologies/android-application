package com.tulib.alfaezounschool.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.data.network.response.response_models.TeachersModel
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class TeachersAdapter(var context: Context, var teacherModelArrayList: ArrayList<TeachersModel>? = null) : RecyclerView.Adapter<TeachersAdapter.TeachersViewHolder>() {

    override fun onCreateViewHolder(teacher: ViewGroup, viewType: Int): TeachersViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_teacher_list, teacher, false)
        return TeachersViewHolder(v)
    }

    override fun onBindViewHolder(holder: TeachersViewHolder, position: Int) {
        teacherModelArrayList?.let { list ->
            holder.textViewName.setText(list[position].name)
            holder.textViewEmail.setText(list[position].email)
            Picasso.get()
                    .load(list[position].photo)
                    .placeholder(R.drawable.pic)
                    .into(holder.imageView)
        }
    }


    override fun getItemCount(): Int {
        return teacherModelArrayList?.size ?: 0
    }

    inner class TeachersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: CircleImageView
        var textViewName: TextView
        var textViewEmail: TextView
        var txtView: RelativeLayout

        init {
            imageView = itemView.findViewById(R.id.imgTeacherImage);
            textViewName = itemView.findViewById(R.id.txtTeacher);
            textViewEmail = itemView.findViewById(R.id.txtEmail);
            txtView = itemView.findViewById(R.id.relative_teacherlist);
        }
    }


    fun setData(list: ArrayList<TeachersModel>) {
        this.teacherModelArrayList = list
        notifyDataSetChanged()
    }
}