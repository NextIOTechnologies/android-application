package com.tulib.alfaezounschool.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.data.network.response.response_models.StudentModel
import java.util.*
import com.tulib.alfaezounschool.adapter.StudentSelectAdapter.StudentSelectViewHolder
import com.tulib.alfaezounschool.utils.AppConstants.URL_ATTACH_FILES
import de.hdodenhof.circleimageview.CircleImageView

class StudentSelectAdapter(var context: Context, var studentSelectModelArrayList: ArrayList<StudentModel>?=null) : RecyclerView.Adapter<StudentSelectViewHolder>(),Filterable  {

    var studentFilterList = ArrayList<StudentModel>()

    init {
        studentFilterList = studentSelectModelArrayList!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentSelectViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_spinner_studentlist, parent, false)
        return StudentSelectViewHolder(v)
    }

    override fun onBindViewHolder(holder: StudentSelectViewHolder, position: Int) {
        studentFilterList ?.let {list->
            holder.textViewName.setText(list[position].name)
            holder.textViewClass.setText(list[position].className)
            holder.txtView.setOnClickListener { view ->
                mClickListener.onClick(position, view)
            }

            Picasso.get()
                    .load(list[position].photo)
                    .placeholder(R.drawable.pic)
                    .into(holder.imageView)
        }

    }

    override fun getItemCount(): Int {
        return studentFilterList ?.size?:0
    }

    inner class StudentSelectViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: CircleImageView
        var textViewName: TextView
        var textViewClass: TextView
        var txtView: RelativeLayout

        init {
            imageView = itemView.findViewById(R.id.imgSpinnerStudentImage);
            textViewName = itemView.findViewById(R.id.txtSpinnerStudent);
            textViewClass = itemView.findViewById(R.id.txtSpinnerClass);
            txtView = itemView.findViewById(R.id.relative_studentlist);
        }
    }

    lateinit var mClickListener: ClickListener

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }

    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    studentFilterList = studentSelectModelArrayList!!
                } else {
                    val resultList = ArrayList<StudentModel>()
                    for (row in studentSelectModelArrayList!!) {
                        if (row.name.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))) {
                            resultList.add(row)
                        }
                    }
                    studentFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = studentFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                studentFilterList = results?.values as ArrayList<StudentModel>
                notifyDataSetChanged()
            }

        }
    }

    fun setData( list: ArrayList<StudentModel>)
    {
        this.studentFilterList =list
        notifyDataSetChanged()
    }
}