package com.tulib.alfaezounschool.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.squareup.picasso.Picasso;
import com.tulib.alfaezounschool.R;
import com.tulib.alfaezounschool.activity.MediaPhotoActivity;
import com.tulib.alfaezounschool.data.network.response.response_models.MediaFilesModel;

import java.sql.Array;
import java.util.ArrayList;


public class MediaGridAdapter extends BaseAdapter {
    Context context;
    ArrayList<MediaFilesModel> parentModelArrayList;

    public MediaGridAdapter(Context context, ArrayList<MediaFilesModel> parentModelArrayList) {
        this.context = context;
        this.parentModelArrayList = parentModelArrayList;
    }

    @Override
    public int getCount() {
        return parentModelArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public void setData( ArrayList<MediaFilesModel> list) {
        this.parentModelArrayList = list;
        this.notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_gridmedia, null);
        TextView txtFileName;// inflate the layout
        ImageView txtGridMediaImg;// inflate the layout

        txtFileName = view.findViewById(R.id.txtGridMediaFileName);
        txtGridMediaImg = view.findViewById(R.id.txtGridMediaImg);

        txtFileName.setText(parentModelArrayList.get(i).getFileNameDisplay());


            if(parentModelArrayList.get(i).getType().equals("file")){
                Picasso.get()
                        .load(parentModelArrayList.get(i).getFileUrl())
                        .into(txtGridMediaImg);
            }


        return view;
    }

    interface OnItemClickListener {
         void onItemClick(Array item, int pos);
    }
}
