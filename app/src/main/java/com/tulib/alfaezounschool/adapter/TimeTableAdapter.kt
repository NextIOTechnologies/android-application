package com.tulib.alfaezounschool.adapter

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.adapter.TimeTableAdapter.TimeTableViewHolder
import com.tulib.alfaezounschool.data.network.response.response_models.TimeTableList
import com.tulib.alfaezounschool.data.network.response.response_models.TimeTableModel
import java.util.*

class TimeTableAdapter(var context: Context, var timeTableModelArrayList: ArrayList<TimeTableModel>?=null) : RecyclerView.Adapter<TimeTableViewHolder>() {
    var day: String? = null
    var timeTableList :TimeTableList?=null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimeTableViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_timetable, parent, false)
        return TimeTableViewHolder(v)
    }

    override fun onBindViewHolder(holder: TimeTableViewHolder, position: Int) {

        timeTableModelArrayList?.let {list->
            holder.txtDay.text = list[position].day
            holder.txtTime.text = list[position].time
            holder.txtSubject.text = list[position].subject
            holder.txtClass.text = list[position].classes
            holder.txtName.text = list[position].teacher
            if (position == 0) {
                holder.txtDay.setTextColor(Color.parseColor("#fbb015"))
                holder.imgPin.setImageResource(R.drawable.orange_pin)
            }
            if (position == 1) {
                holder.txtDay.setTextColor(Color.parseColor("#e45d8d"))
                holder.imgPin.setImageResource(R.drawable.pink_pin)
            }
            if (position == 2) {
                holder.txtDay.setTextColor(Color.parseColor("#24c2a1"))
                holder.imgPin.setImageResource(R.drawable.seagreen_pin)
            }
            if (position == 3) {
                holder.txtDay.setTextColor(Color.parseColor("#d16811"))
                holder.imgPin.setImageResource(R.drawable.brown_pin)
            }
            if (position == 4) {
                holder.txtDay.setTextColor(Color.parseColor("#23a9f0"))
                holder.imgPin.setImageResource(R.drawable.blue_pin)
            }
            if (position == 5) {
                holder.txtDay.setTextColor(Color.parseColor("#e45d8d"))
                holder.imgPin.setImageResource(R.drawable.pink_pin)
            }
            if (position == 6) {
                holder.txtDay.setTextColor(Color.parseColor("#24c2a1"))
                holder.imgPin.setImageResource(R.drawable.seagreen_pin)
            }

            holder.txtSeeAll.setOnClickListener { view ->
                day = holder.txtDay.text.toString()
                TimeTableDialog(view.context, position)
            }
        }
    }

    override fun getItemCount(): Int { return timeTableModelArrayList?.size?:0 }

    inner class TimeTableViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtDay: TextView
        var txtTime: TextView
        var txtSubject: TextView
        var txtClass: TextView
        var txtName: TextView
        var txtSeeAll: TextView
        var relativeLayoutTimeTable: RelativeLayout
        var imgPin: ImageView

        init {
            txtDay = itemView.findViewById(R.id.txtTimeTableDay)
            txtTime = itemView.findViewById(R.id.txtTimeTableTime)
            txtSubject = itemView.findViewById(R.id.txtTimeTableSubject)
            txtClass = itemView.findViewById(R.id.txtTimeTableClass)
            txtName = itemView.findViewById(R.id.txtTimeTableName)
            txtSeeAll = itemView.findViewById(R.id.txtTimeTableSeeAll)
            imgPin = itemView.findViewById(R.id.imgTimeTablePin)
            relativeLayoutTimeTable = itemView.findViewById(R.id.relativeTimeTable)
        }
    }

    private fun TimeTableDialog(context: Context, i: Int) {
        val timeTableModels: ArrayList<TimeTableModel>
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.dialog_timetable)
        val imgClose: ImageView
        val imgPin: ImageView
        val txtTimeTableDay: TextView
        imgPin = dialog.findViewById(R.id.imgPin)
        txtTimeTableDay = dialog.findViewById(R.id.txtDialogTimeTableDay)
        imgClose = dialog.findViewById(R.id.imgDialogTimeTableClose)
        if (i == 0) {
            imgPin.setImageResource(R.drawable.orange_pin)
            txtTimeTableDay.text = day
            txtTimeTableDay.setTextColor(Color.parseColor("#fbb015"))
        }
        if (i == 1) {
            imgPin.setImageResource(R.drawable.pink_pin)
            txtTimeTableDay.text = day
            txtTimeTableDay.setTextColor(Color.parseColor("#e45d8d"))
        }
        if (i == 2) {
            imgPin.setImageResource(R.drawable.seagreen_pin)
            txtTimeTableDay.text = day
            txtTimeTableDay.setTextColor(Color.parseColor("#24c2a1"))
        }
        if (i == 3) {
            imgPin.setImageResource(R.drawable.brown_pin)
            txtTimeTableDay.text = day
            txtTimeTableDay.setTextColor(Color.parseColor("#d16811"))
        }
        if (i == 4) {
            imgPin.setImageResource(R.drawable.blue_pin)
            txtTimeTableDay.text = day
            txtTimeTableDay.setTextColor(Color.parseColor("#23a9f0"))
        }

        if (i == 5) {
            imgPin.setImageResource(R.drawable.pink_pin)
            txtTimeTableDay.text = day
            txtTimeTableDay.setTextColor(Color.parseColor("#e45d8d"))
        }
        if (i == 6) {
            imgPin.setImageResource(R.drawable.seagreen_pin)
            txtTimeTableDay.text = day
            txtTimeTableDay.setTextColor(Color.parseColor("#24c2a1"))

        }
        val gridView = dialog.findViewById<GridView>(R.id.gridTimeTable)

        timeTableModelArrayList?.let {it
            val arrayList = getList(it[i].day?:"")
            arrayList?.let {list->
                val myAdapter = TimeTableGridAdapter(context, list)
                gridView.adapter = myAdapter
                imgClose.setOnClickListener { dialog.dismiss() }
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.show()
            }
        }
    }

    fun setData( list: ArrayList<TimeTableModel>,timeTableList: TimeTableList)
    {
        this.timeTableModelArrayList=list
        this.timeTableList = timeTableList
        notifyDataSetChanged()
    }

    fun getList(day:String):ArrayList<TimeTableModel>?
    {
      return when(day)
        {
            "MONDAY"-> timeTableList?.mondayList
            "SUNDAY"-> timeTableList?.sundayList
            "TUESDAY"-> timeTableList?.tuesdayList
            "WEDNESDAY"-> timeTableList?.wednesdayList
            "THURSDAY"-> timeTableList?.thursdayList
            "FRIDAY"-> timeTableList?.fridayList
            "SATURDAY"-> timeTableList?.saturdayList

            else-> null

      }
    }
}