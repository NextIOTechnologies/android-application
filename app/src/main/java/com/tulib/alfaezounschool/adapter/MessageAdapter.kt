package com.tulib.alfaezounschool.adapter


import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.ui.message.conversation_view.MessageConverstionActivity
import com.tulib.alfaezounschool.adapter.MessageAdapter.MessageViewHolder
import com.tulib.alfaezounschool.data.network.response.response_models.MessagesModel
import java.util.*

class MessageAdapter(var context: Context, var messageModelArrayList: ArrayList<MessagesModel>? = null) : RecyclerView.Adapter<MessageViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_message, parent, false)
        return MessageViewHolder(v)
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        messageModelArrayList?.let { list->
            holder.txtName.text = list[position].name
            holder.txtDate.text = list[position].createDate
            holder.txtSubject.text = list[position].subject
        }

        holder.txtView.setOnClickListener { view ->
            startActivity(view.context, MessageConverstionActivity::class.java,messageModelArrayList!![position].conversationId)
          // Common.startActivity(view.context, MessageConverstionActivity::class.java)
        }
    }

    override fun getItemCount(): Int {
        return messageModelArrayList?.size?:0
    }

    inner class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtName: TextView
        var txtDate: TextView
        var txtSubject: TextView
        var txtView: TextView

        init {
            txtName = itemView.findViewById(R.id.txtMessageName)
            txtDate = itemView.findViewById(R.id.txtMessageDate)
            txtSubject = itemView.findViewById(R.id.txtMessageSubject)
            txtView = itemView.findViewById(R.id.txtMessageView)
        }
    }

    fun setData(list: ArrayList<MessagesModel>? = null)
    {
        this.messageModelArrayList = list
        notifyDataSetChanged()
    }

    fun startActivity(context: Context, secondActivity: Class<*>?,id:String)
    {
        val intent = Intent(context, secondActivity)
        intent.putExtra("id",id)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        context.startActivity(intent)
    }
}