package com.tulib.alfaezounschool.adapter

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.adapter.StudentActivityAdapter.StudentActivityViewHolder
import com.tulib.alfaezounschool.data.network.response.response_models.StudentActivityModel
import java.util.*

class StudentActivityAdapter(var context: Context, var studentActivityModelArrayList: ArrayList<StudentActivityModel>?=null) : RecyclerView.Adapter<StudentActivityViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentActivityViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_studentactivity, parent, false)
        return StudentActivityViewHolder(v)
    }

    override fun onBindViewHolder(holder: StudentActivityViewHolder, position: Int) {
        studentActivityModelArrayList?.let {list->
            holder.txtStudentName.setText(list[position].studentName)
            holder.txtDate.setText(list[position].date)
            holder.txtSubject.setText(list[position].subject)
            holder.txtTeacherName.setText(list[position].teacherName)
            holder.txtView.setOnClickListener { view -> StudentActivityDialog(view.context, position)
            }
        }

    }

    override fun getItemCount(): Int {
        return studentActivityModelArrayList?.size?:0
    }

    inner class StudentActivityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtStudentName: TextView
        var txtDate: TextView
        var txtSubject: TextView
        var txtTeacherName: TextView
        var txtView: TextView
        var relativeLayoutTimeTable:RelativeLayout;

        init {
            txtStudentName = itemView.findViewById(R.id.txtStudentActivityName);
            txtDate = itemView.findViewById(R.id.txtStudentActivityDate);
            txtSubject = itemView.findViewById(R.id.txtDialogAccountDate);
            txtTeacherName = itemView.findViewById(R.id.txtStudentActivityTeacher);
            txtView = itemView.findViewById(R.id.txtStudentActivityView);
            relativeLayoutTimeTable = itemView.findViewById(R.id.relativeStudentActivity);
        }
    }

    private fun StudentActivityDialog(context: Context, position: Int) {
        studentActivityModelArrayList?.let {list->
            val dialog = Dialog(context)
            dialog.setContentView(R.layout.dialog_studentactivity)
            dialog.findViewById<TextView>(R.id.txtDialogStudentActivitysStudentName).text = list[position].studentName
            dialog.findViewById<TextView>(R.id.txtDialogAccountDate).text = list[position].date

            dialog.findViewById<TextView>(R.id.txtDialogStudentActivitySubject).text = list[position].studentName
            dialog.findViewById<TextView>(R.id.txtDialogStudentActivityTeacher).text = list[position].teacherName
            dialog.findViewById<TextView>(R.id.txtDialogStudentActivityOral).text = list[position].oral
            dialog.findViewById<TextView>(R.id.txtDialogStudentActivityWriting).text = list[position].writing
            dialog.findViewById<TextView>(R.id.txtDialogStudentActivityBehaviour).text = list[position].behaviour
            dialog.findViewById<TextView>(R.id.txtDialogStudentActivityPresent).text = list[position].present
            dialog.findViewById<TextView>(R.id.txtDialogStudentActivityParticipation).text = list[position].participation
            dialog.findViewById<TextView>(R.id.txtDialogStudentActivityNote).text = list[position].note


            val imgClose: ImageView
            val btnOk: Button
            val linearDownload: LinearLayout
            imgClose = dialog.findViewById(R.id.imgDialogStudentActivityClose)
            imgClose.setOnClickListener { dialog.dismiss() }

            btnOk = dialog.findViewById(R.id.btnDialogStudentActivityOk)
            btnOk.setOnClickListener { dialog.dismiss() }


            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        }


    }

    fun setData( list: ArrayList<StudentActivityModel>)
    {
        this.studentActivityModelArrayList=list
        notifyDataSetChanged()
    }
}