package com.tulib.alfaezounschool.adapter

import android.content.Context
import com.tulib.alfaezounschool.data.network.response.response_models.ExamScheduleModel
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.adapter.ExamScheduleAdapter.ExamScheduleViewHolder
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.tulib.alfaezounschool.R
import android.widget.TextView
import java.util.ArrayList

class ExamScheduleAdapter(var context: Context, var examScheduleModelArrayList: ArrayList<ExamScheduleModel>?=null) : RecyclerView.Adapter<ExamScheduleViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExamScheduleViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_exam_schedule, parent, false)
        return ExamScheduleViewHolder(v)
    }

    override fun onBindViewHolder(holder: ExamScheduleViewHolder, position: Int) {
        examScheduleModelArrayList?.let {list->
            holder.txtExamSubject.text = list[position].subject
            holder.txtExamDate.text = list[position].date
            holder.txtExamTime.text = list[position].examto
            holder.txtExamClass.text = list[position].room
            holder.txtExamName.text = list[position].exam
        }
    }

    override fun getItemCount(): Int {
        return examScheduleModelArrayList?.size?:0
    }

    inner class ExamScheduleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtExamSubject: TextView
        var txtExamDate: TextView
        var txtExamTime: TextView
        var txtExamClass: TextView
        var txtExamName: TextView

        init {
            txtExamSubject = itemView.findViewById(R.id.txtExamSubjectName)
            txtExamDate = itemView.findViewById(R.id.txtExamDate)
            txtExamTime = itemView.findViewById(R.id.txtExamTime)
            txtExamClass = itemView.findViewById(R.id.txtExamRoom)
            txtExamName = itemView.findViewById(R.id.txtExamName)
        }
    }

    fun setData(list: ArrayList<ExamScheduleModel>)
    {
        this.examScheduleModelArrayList=list
        notifyDataSetChanged()
    }
}