package com.tulib.alfaezounschool.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.data.network.response.response_models.ConversationModel
import java.text.SimpleDateFormat
import java.util.*

class ConversationAdapter(var context: Context, var messageModelArrayList: ArrayList<ConversationModel>? = null) : RecyclerView.Adapter<ConversationAdapter.ConversationViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConversationViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_conversation, parent, false)
        return ConversationViewHolder(v)
    }

    override fun onBindViewHolder(holder: ConversationViewHolder, position: Int) {
        messageModelArrayList?.let { list->
            holder.msg.text = list[position].msg
            holder.tvSender.text = list[position].sender
            val simpleDateFormat = SimpleDateFormat("yyyy-mm-dd hh:MM:ss")
            val simpleDateFormat2 = SimpleDateFormat("dd mm,yyyy")
            val date = simpleDateFormat.parse(list[position].createDate)
            holder.tvDate.text = simpleDateFormat2.format(date)
        }


    }

    override fun getItemCount(): Int {
        return messageModelArrayList?.size?:0
    }

    inner class ConversationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var msg: TextView
        var tvSender: TextView
        var tvDate: TextView


        init {
            msg = itemView.findViewById(R.id.msg)
            tvSender = itemView.findViewById(R.id.tvSender)
            tvDate = itemView.findViewById(R.id.tvDate)
        }
    }

    fun setData(list: ArrayList<ConversationModel>? = null)
    {
        this.messageModelArrayList = list
        notifyDataSetChanged()
    }


}