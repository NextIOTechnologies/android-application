package com.tulib.alfaezounschool.adapter

import android.content.Context
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.data.network.response.response_models.HolidayModel
import java.util.*

class HolidayAdapter(var context: Context, var holidayArrayList: ArrayList<HolidayModel>?=null)
    : RecyclerView.Adapter<HolidayAdapter.HolidayViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolidayViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_notification, parent, false)
        return HolidayViewHolder(v)
    }

    inner class HolidayViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtTitle: TextView
        var txtDescription: TextView
        var txtDate: TextView

        init {
            txtTitle = itemView.findViewById(R.id.txtNotificationTitle)
            txtDescription = itemView.findViewById(R.id.txtNotificationDescription)
            txtDate = itemView.findViewById(R.id.txtNotificationDate)
        }
    }
    fun setData( list: ArrayList<HolidayModel>)
    {
        this.holidayArrayList=list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: HolidayAdapter.HolidayViewHolder, position: Int) {
        holidayArrayList?.let { list->
            holder.txtTitle.text = list[position].title
            val mode = HtmlCompat.FROM_HTML_MODE_LEGACY
            holder.txtDescription.text = if (Build.VERSION.SDK_INT >= 24) Html.fromHtml( list[position].details, mode)
            else HtmlCompat.fromHtml( list[position].details, mode)
            holder.txtDate.text = list[position].fromDate
        }
    }

    override fun getItemCount(): Int {
        return holidayArrayList?.size?:0
    }
}