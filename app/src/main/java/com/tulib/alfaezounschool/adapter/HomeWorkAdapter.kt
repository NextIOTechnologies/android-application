package com.tulib.alfaezounschool.adapter

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.adapter.HomeWorkAdapter.HomeWorkViewHolder
import com.tulib.alfaezounschool.data.network.response.response_models.HomeWorkModel
import java.util.*

class HomeWorkAdapter(var context: Context, var homeWorkModelArrayList: ArrayList<HomeWorkModel>?=null) : RecyclerView.Adapter<HomeWorkViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeWorkViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_homework, parent, false)
        return HomeWorkViewHolder(v)
    }

    override fun onBindViewHolder(holder: HomeWorkViewHolder, position: Int) {
        homeWorkModelArrayList?.let {list->
            holder.txtSubjectName.setText(list[position].subject)
            holder.txtDate.setText(list[position].createDate)
            holder.txtDeadlineDate.setText(list[position].deadlinedate)
            holder.imgDocument.setImageResource(R.drawable.download_icon)
            holder.txtView.setOnClickListener { view -> HomeWorkDialog(view.context, position) }
        }

    }

    override fun getItemCount(): Int {
        return homeWorkModelArrayList?.size?:0
    }

    inner class HomeWorkViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtSubjectName: TextView
        var txtDate: TextView
        var txtDeadlineDate: TextView
        var txtView: TextView
        var relativeLayoutHomeWork: RelativeLayout
        var imgDocument: ImageView

        init {
            txtSubjectName = itemView.findViewById(R.id.txtHomeWorkSubject)
            txtDate = itemView.findViewById(R.id.txtHomeWorkDate)
            txtDeadlineDate = itemView.findViewById(R.id.txtHomeWorkDeadlineDate)
            imgDocument = itemView.findViewById(R.id.imgHomeWorkDocumentDownload)
            txtView = itemView.findViewById(R.id.txtHomeWorkView)
            relativeLayoutHomeWork = itemView.findViewById(R.id.relativeHomeWork)
        }
    }

    private fun HomeWorkDialog(context: Context, position: Int) {
        homeWorkModelArrayList?.let {list->
            val dialog = Dialog(context)
            dialog.setContentView(R.layout.dialog_homework)
            dialog.findViewById<TextView>(R.id.txtDialogHomeWorkSubject).text = list[position].subject
            dialog.findViewById<TextView>(R.id.txtDialogHomeWorkDate).text = list[position].createDate
            dialog.findViewById<TextView>(R.id.txtDialogHomeWorkDescription).text = list[position].description
            dialog.findViewById<TextView>(R.id.txtDialogHomeWorkDeadlinDate).text = list[position].deadlinedate


            val imgClose: ImageView
            val linearDownload: LinearLayout
            linearDownload = dialog.findViewById(R.id.linearHomeWorkDownload)
            imgClose = dialog.findViewById(R.id.imgDialogHomeWorkClose)
            imgClose.setOnClickListener { dialog.dismiss() }
            linearDownload.setOnClickListener { dialog.dismiss() }
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        }


    }

    fun setData( list: ArrayList<HomeWorkModel>)
    {
        this.homeWorkModelArrayList=list
        notifyDataSetChanged()
    }
}