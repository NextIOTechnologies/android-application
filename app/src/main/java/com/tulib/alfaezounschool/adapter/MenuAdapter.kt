package com.tulib.alfaezounschool.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.activity.AccountActivity
import com.tulib.alfaezounschool.activity.MainActivity
import com.tulib.alfaezounschool.activity.ui.accounts.Accounts
import com.tulib.alfaezounschool.activity.ui.announcements.AnnouncementActivity
import com.tulib.alfaezounschool.activity.ui.attendance.AttendanceActivity
import com.tulib.alfaezounschool.activity.ui.base_activity.BaseActivity
import com.tulib.alfaezounschool.activity.ui.complain.ComplainActivity
import com.tulib.alfaezounschool.activity.ui.homework.HomeWorkActivity
import com.tulib.alfaezounschool.activity.ui.marks.MarksActivity
import com.tulib.alfaezounschool.activity.ui.media.Media
import com.tulib.alfaezounschool.activity.ui.studentactivity.StudentActivity
import com.tulib.alfaezounschool.activity.ui.timetable.TimeTableActivity
import com.tulib.alfaezounschool.utils.Common

class MenuAdapter(context: BaseActivity, image: IntArray, name: Array<String>) : RecyclerView.Adapter<MenuAdapter.MenuViewHolder?>() {
    var context: BaseActivity
    var image: IntArray
    var name: Array<String>
    var listener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        val v: View = LayoutInflater.from(context).inflate(R.layout.row_menu_item, parent, false)
        return MenuViewHolder(v)
    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        holder.bind(name, image, position, listener)
        if (position == 11  ||  position==10) {
            holder.linearMenu.setVisibility(View.GONE)
        }
        holder.linearMenu.setOnClickListener(View.OnClickListener { view ->
            if (position == 0) {
                Common.startActivity(view.context, MainActivity::class.java)
            }
            if (position == 1) {
                Common.startActivity(view.context, AttendanceActivity::class.java)
            }
            if (position == 2) {
                Common.startActivity(view.context, StudentActivity::class.java)
            }
            if (position == 3) {
                Common.startActivity(view.context, HomeWorkActivity::class.java)
            }
            if (position == 4) {
                Common.startActivity(view.context, TimeTableActivity::class.java)
            }
            if (position == 5) {
                Common.startActivity(view.context, Media::class.java)
            }
            if (position == 6) {
                Common.startActivity(view.context, MarksActivity::class.java)
            }
            if (position == 7) {
                Common.startActivity(view.context, Accounts::class.java)
            }
            if (position == 8) {
                Common.startActivity(view.context, AnnouncementActivity::class.java)
            }
            if (position == 9) {
                Common.startActivity(view.context, ComplainActivity::class.java)
            }
            if (position == 10) {
                // DashboardFragment.count = 0;
                Common.startActivity(view.context, AccountActivity::class.java)
            }
            context.drawerLayout?.closeDrawers()
        })
    }

    override fun getItemCount(): Int {
        return image.size
    }

    inner class MenuViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgMenu: ImageView
        var txtMenuName: TextView
        var linearMenu: LinearLayout
        fun bind(name: Array<String>, image: IntArray, pos: Int, listener: OnItemClickListener?) {
            txtMenuName.setText(name[pos])
            imgMenu.setImageResource(image[pos])
            linearMenu.setOnClickListener(View.OnClickListener { listener!!.onItemClick(name, pos) })
        }

        init {
            imgMenu = itemView.findViewById(R.id.imgMenu)
            txtMenuName = itemView.findViewById<TextView>(R.id.txtMenuName)
            linearMenu = itemView.findViewById<LinearLayout>(R.id.linearMenu)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(item: Array<String>?, pos: Int)
    }

    init {
        this.context = context
        this.image = image
        this.name = name
    }
}