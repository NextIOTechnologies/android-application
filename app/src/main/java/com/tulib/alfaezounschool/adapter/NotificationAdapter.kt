package com.tulib.alfaezounschool.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.adapter.NotificationAdapter.NotificationViewHolder
import com.tulib.alfaezounschool.data.network.response.response_models.NotificationModel
import java.util.*

class NotificationAdapter(var context: Context, var notificationModelArrayList: ArrayList<NotificationModel>?=null)
    : RecyclerView.Adapter<NotificationViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_notification, parent, false)
        return NotificationViewHolder(v)
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        notificationModelArrayList?.let { list->
            holder.txtTitle.text = list[position].title
            holder.txtDescription.text = list[position].description
            holder.txtDate.text = list[position].date
        }
    }

    override fun getItemCount(): Int {
        return notificationModelArrayList?.size?:0
    }

    inner class NotificationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtTitle: TextView
        var txtDescription: TextView
        var txtDate: TextView

        init {
            txtTitle = itemView.findViewById(R.id.txtNotificationTitle)
            txtDescription = itemView.findViewById(R.id.txtNotificationDescription)
            txtDate = itemView.findViewById(R.id.txtNotificationDate)
        }
    }
    fun setData( list: ArrayList<NotificationModel>)
    {
        this.notificationModelArrayList=list
        notifyDataSetChanged()
    }
}