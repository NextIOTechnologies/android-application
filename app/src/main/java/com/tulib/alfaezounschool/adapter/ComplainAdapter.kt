package com.tulib.alfaezounschool.adapter

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.adapter.ComplainAdapter.ComplainViewHolder
import com.tulib.alfaezounschool.app.AppController
import com.tulib.alfaezounschool.data.network.response.response_models.ComplainModel
import com.tulib.alfaezounschool.model.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import nye.health.data.network.api_call.session.SessionManager
import org.kodein.di.generic.instance
import java.util.*


class ComplainAdapter(var context: Context, var complainModelArrayList: ArrayList<ComplainModel>? = null) : RecyclerView.Adapter<ComplainViewHolder>() {
    private val sessionManager : SessionManager by AppController.kodein().instance()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComplainViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_complain, parent, false)


        return ComplainViewHolder(v)
    }




    override fun onBindViewHolder(holder: ComplainViewHolder, position: Int) {
        complainModelArrayList?.let { list->

            var user:User?
            CoroutineScope (Dispatchers.Main ).launch {
                 user =sessionManager.databaseInterface.getUser()
                holder.txtComplain.setText(list[position].name)
                holder.txtName.setText(list[position].title)

                if(list[position].type=="received"){
                    holder.txtComplainTolabel.setText(R.string.complain_from)
                }else{
                    holder.txtComplainTolabel.setText(R.string.complain_to)
                }
            }




            holder.txtDate.setText(list[position].createDate)

            holder.txtView.setOnClickListener { view -> ComplainDialog(view.context, position)
            }
        }

    }

    override fun getItemCount(): Int {
        return complainModelArrayList?.size?:0
    }

    inner class ComplainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtName: TextView
        var txtDate: TextView
        var txtComplain: TextView
        var txtView: TextView
        var txtComplainTolabel: TextView
        init {
            txtName = itemView.findViewById(R.id.txtComplainName);
            txtDate = itemView.findViewById(R.id.txtComplainDate);
            txtComplainTolabel = itemView.findViewById(R.id.txtComplainTolabel);
            txtComplain = itemView.findViewById(R.id.txtComplainTo);
            txtView = itemView.findViewById(R.id.txtComplainView);

        }
    }

    private fun ComplainDialog(context: Context, position: Int) {
        complainModelArrayList?.let { list->
            val dialog = Dialog(context)
            dialog.setContentView(R.layout.dialog_complain)
            dialog.findViewById<TextView>(R.id.txtDialogComplainTitle).text = list[position].title
            dialog.findViewById<TextView>(R.id.txtDialogComplainDate).text = list[position].createDate
            dialog.findViewById<TextView>(R.id.txtDialogComplainDescription).text = list[position].description




            val imgClose: ImageView
            val linearDownload: LinearLayout
            imgClose = dialog.findViewById(R.id.imgDialogComplainClose)
            imgClose.setOnClickListener { dialog.dismiss() }
//            URL_ATTACH_FILES+"/"+list[position].attachment





            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        }


    }


// ...


    fun setData(list: ArrayList<ComplainModel>)
    {
        this.complainModelArrayList=list
        notifyDataSetChanged()
    }
}