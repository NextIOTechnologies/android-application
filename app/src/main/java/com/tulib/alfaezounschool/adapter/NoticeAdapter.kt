package com.tulib.alfaezounschool.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.data.network.response.response_models.NoticeModel
import kotlinx.android.synthetic.main.fragment_dashboard.*
import java.text.SimpleDateFormat
import java.util.*

class NoticeAdapter(var context: Context, var noticeArrayList: ArrayList<NoticeModel>?=null)
    : RecyclerView.Adapter<NoticeAdapter.NoticeViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoticeViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_notification, parent, false)
        return NoticeViewHolder(v)
    }

    inner class NoticeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtTitle: TextView
        var txtDescription: TextView
        var txtDate: TextView

        init {
            txtTitle = itemView.findViewById(R.id.txtNotificationTitle)
            txtDescription = itemView.findViewById(R.id.txtNotificationDescription)
            txtDate = itemView.findViewById(R.id.txtNotificationDate)
        }
    }
    fun setData( list: ArrayList<NoticeModel>)
    {
        this.noticeArrayList=list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: NoticeAdapter.NoticeViewHolder, position: Int) {
        noticeArrayList?.let { list->
            val simpleDateFormat = SimpleDateFormat("yyyy-mm-dd")
            val simpleDateFormat2 = SimpleDateFormat("dd mm,yyyy")
            val date = simpleDateFormat.parse(list[position].date)

            holder.txtTitle.text = list[position].title
            holder.txtDescription.text = list[position].notice
            holder.txtDate.text = simpleDateFormat2.format(date)
        }
    }

    override fun getItemCount(): Int {
        return noticeArrayList?.size?:0
    }
}