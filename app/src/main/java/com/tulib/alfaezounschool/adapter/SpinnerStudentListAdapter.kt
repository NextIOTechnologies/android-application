package com.tulib.alfaezounschool.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.data.network.response.response_models.StudentModel
import kotlin.collections.ArrayList

class SpinnerStudentListAdapter(context: Context, var studentListArrayList: ArrayList<StudentModel>)
    : ArrayAdapter<StudentModel?>(context!!, 0, studentListArrayList!! as List<StudentModel?>) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent)
    }

    @SuppressLint("ResourceAsColor")
    private fun initView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.row_spinner_studentlist, parent, false
            )
        }
        val imageView = convertView?.findViewById<ImageView>(R.id.imgSpinnerStudentImage)
        textViewName = convertView?.findViewById<TextView>(R.id.txtSpinnerStudent)

        val textViewClass: TextView? = convertView?.findViewById(R.id.txtSpinnerClass)

        val currentItem: StudentModel? = getItem(position)
        if (currentItem != null) {
//            imageView?.setImageResource(currentItem.photo)
            textViewName?.setText(currentItem.name)
            textViewClass?.setText(currentItem.className)
        }
        return convertView!!
    }

    companion object {
        var textViewName: TextView? = null
    }


    fun setData(list: ArrayList<StudentModel>) {
        this.studentListArrayList = list
        notifyDataSetChanged()
    }
}