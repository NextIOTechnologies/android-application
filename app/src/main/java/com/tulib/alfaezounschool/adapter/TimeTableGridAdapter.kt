package com.tulib.alfaezounschool.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.data.network.response.response_models.TimeTableModel
import java.util.*
import android.widget.*
class TimeTableGridAdapter     // this.inflter = inflter;
(var context: Context, var timeTableModelArrayList: ArrayList<TimeTableModel>) : BaseAdapter() {
    override fun getCount(): Int {
        return timeTableModelArrayList.size
    }

    override fun getItem(p0: Int): Any? {
        return null
    }


    override fun getItemId(i: Int): Long {
        return 0
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
        var view = view
        view = LayoutInflater.from(viewGroup.context).inflate(R.layout.row_gridtimetable, null)
        val txtTime: TextView
        val txtSubject: TextView
        val txtClass: TextView
        val txtName: TextView // inflate the layout
        txtTime = view.findViewById(R.id.txtGridTimeTableTime)
        txtSubject = view.findViewById(R.id.txtGridTimeTableSubject)
        txtClass = view.findViewById(R.id.txtGridTimeTableClass)
        txtName = view.findViewById(R.id.txtGridTimeTableName) // get the reference of ImageView
        txtTime.text = timeTableModelArrayList[i].time
        txtSubject.text = timeTableModelArrayList[i].subject
        txtClass.text = timeTableModelArrayList[i].classes
        txtName.text = timeTableModelArrayList[i].teacher // set logo images
        return view
    }
}