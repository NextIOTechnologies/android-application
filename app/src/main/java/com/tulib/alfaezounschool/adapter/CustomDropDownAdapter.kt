package com.tulib.alfaezounschool.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.data.network.response.response_models.StudentModel
import com.tulib.alfaezounschool.data.network.response.response_models.UserTypeModel
import java.util.ArrayList

class CustomDropDownAdapter(val context: Context, var dataSource: ArrayList<UserTypeModel>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view: View
        val vh: ItemHolder
        if (convertView == null) {
            view = inflater.inflate(R.layout.custom_spinner_item, parent, false)
            vh = ItemHolder(view)
            view?.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemHolder
        }
        vh.text.text = dataSource.get(position).usertype
        return view
    }

    override fun getItem(position: Int): Any? {
        return dataSource[position];
    }



    override fun getItemId(position: Int): Long {
        return position.toLong();
    }

    private class ItemHolder(row: View?) {
        val text: TextView


        init {
            text = row?.findViewById(R.id.text) as TextView
        }
    }

    override fun getCount(): Int {
        // don't display last item. It is used as hint.
        val count = dataSource.size
        return if (count > 0) count - 1 else count
    }

}