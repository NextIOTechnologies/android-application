package com.tulib.alfaezounschool.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.tulib.alfaezounschool.data.network.response.response_models.LibraryModel
import androidx.recyclerview.widget.RecyclerView
import com.tulib.alfaezounschool.adapter.LibraryAdapter.LibraryViewHolder

import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.tulib.alfaezounschool.R
import android.widget.TextView
import com.tulib.alfaezounschool.utils.AppConstants.URL_ATTACH_FILES
import com.squareup.picasso.Picasso
import java.util.ArrayList

class LibraryAdapter(var context: Context, var libraryModelArrayList: ArrayList<LibraryModel>?=null) : RecyclerView.Adapter<LibraryViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LibraryViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_library, parent, false)
        return LibraryViewHolder(v)
    }

    override fun onBindViewHolder(holder: LibraryViewHolder, position: Int) {
        libraryModelArrayList?.let {list->
            holder.txtBookName.text = list[position].name
            holder.txtBookAuthorName.text = list[position].author

            holder.txtView.setOnClickListener {
                val  intent =  Intent(Intent.ACTION_VIEW)
                intent.setDataAndType(Uri.parse(URL_ATTACH_FILES+"/ebooks/"+list[position].file), "application/pdf");
                context.startActivity(intent);
            }

            Picasso.get().load(URL_ATTACH_FILES+"/ebooks/"+list[position].coverPhoto).into(holder.imgBookCover)

        }
    }

    override fun getItemCount(): Int {
        return libraryModelArrayList?.size?:0
    }

    inner class LibraryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtBookName: TextView
        var txtBookAuthorName: TextView
        var imgBookCover: ImageView
        var txtView: TextView
        var imgDownload: ImageView


        init {
            txtBookName = itemView.findViewById(R.id.txtLibraryBookName)
            txtBookAuthorName = itemView.findViewById(R.id.txtLibraryAuthorName)
            imgBookCover = itemView.findViewById(R.id.imgLibraryBookCover)
            txtView = itemView.findViewById(R.id.txtLibraryView)
            imgDownload = itemView.findViewById(R.id.imgLibraryBookDownload)
        }
    }

    fun setData(list: ArrayList<LibraryModel>)
    {
        this.libraryModelArrayList=list
        notifyDataSetChanged()
    }
}