package com.tulib.alfaezounschool.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.tulib.alfaezounschool.R
import com.tulib.alfaezounschool.data.network.response.response_models.StudentModel
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class StudentsAdapter(var context: Context, var studentModelArrayList: ArrayList<StudentModel>? = null) : RecyclerView.Adapter<StudentsAdapter.StudentsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentsViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.row_spinner_studentlist, parent, false)
        return StudentsViewHolder(v)
    }

    override fun onBindViewHolder(holder: StudentsViewHolder, position: Int) {
        studentModelArrayList?.let { list ->
            holder.textViewName.setText(list[position].name)
            holder.textViewClass.setText(list[position].className)
            Picasso.get()
                    .load(list[position].photo)
                    .placeholder(R.drawable.pic)
                    .into(holder.imageView)
        }
    }


    override fun getItemCount(): Int {
        return studentModelArrayList?.size ?: 0
    }

    inner class StudentsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: CircleImageView
        var textViewName: TextView
        var textViewClass: TextView
        var txtView: RelativeLayout

        init {
            imageView = itemView.findViewById(R.id.imgSpinnerStudentImage);
            textViewName = itemView.findViewById(R.id.txtSpinnerStudent);
            textViewClass = itemView.findViewById(R.id.txtSpinnerClass);
            txtView = itemView.findViewById(R.id.relative_studentlist);
        }
    }


    fun setData(list: ArrayList<StudentModel>) {
        this.studentModelArrayList = list
        notifyDataSetChanged()
    }
}