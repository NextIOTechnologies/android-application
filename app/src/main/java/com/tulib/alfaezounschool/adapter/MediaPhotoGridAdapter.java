package com.tulib.alfaezounschool.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.tulib.alfaezounschool.R;

public class MediaPhotoGridAdapter extends BaseAdapter {
    Context context;
    int[] photo;
    LayoutInflater inflter;

    public MediaPhotoGridAdapter(Context context, int[] photo) {
        this.context = context;
        this.photo = photo;
    }

    @Override
    public int getCount() {
        return photo.length;
    }
    @Override
    public Object getItem(int i) {
        return null;
    }
    @Override
    public long getItemId(int i) {
        return 0;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_gridmediaphoto, null);
        ImageView imgPhoto;// inflate the layout
        imgPhoto = view.findViewById(R.id.imggridMediaPhoto);
        imgPhoto.setImageResource(photo[i]);
 // set logo images
        return view;
    }
}
